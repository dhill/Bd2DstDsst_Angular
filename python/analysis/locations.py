################## Location in repository or on EOS

import os
repo = os.getenv('ANAROOT')

class loc : pass
loc.ROOT       = repo+'/'
loc.OUT        = loc.ROOT+'output/'
loc.PYTHON     = loc.ROOT+'python/'
loc.PLOTS      = loc.OUT+'plots/'
loc.LHCB       = loc.ROOT+'LHCb/'
loc.TMPS       = loc.ROOT+'templates/'
loc.TABS       = loc.OUT+'tables/'
loc.LOGS       = loc.OUT+'/logs/'
#loc.WGEOS      = "/eos/lhcb/wg/b2oc/AmAn_Bd2DstDsst_Run12" # Check that you have writing permission here!
#loc.OXDATA     = "/data/lhcb/users/hill/DoubleCharm_Angular"
loc.WGEOS      = "/data/lhcb/users/hill/DoubleCharm_Angular"
loc.OXDATA      = "/data/lhcb/users/hill/DoubleCharm_Angular"
