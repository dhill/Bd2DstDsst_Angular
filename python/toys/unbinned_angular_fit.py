import analysis as an
import sys,os
sys.path.append(an.loc.ROOT+"python/fitting")
#Import initial values used in toys
from init_vals_unbinned_angular_fit import init_vals
from listsfromhist import listsfromhist
from ROOT import TH1F, RooDataHist, RooArgList, RooArgSet, RooRealVar, RooFitResult, RooGaussian, RooPlot, RooFormulaVar, RooAddPdf
import json
import matplotlib.pyplot as plt
import numpy as np
import argparse

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

    toy_path = an.loc.WGEOS+"/Toys/Unbinned_Fit/"

    #Fit par titles
    fit_pars = {"Hm_amp": "$|H_-|$",
    			"Hp_phi": "$\\phi_+$",
    			"Hm_phi": "$\\phi_-$"
    			}

    #Pull value for each fit param
    pulls = {}
    for p in fit_pars:
    	pulls[p] = []

    #Get all toy results
    all_files = [f for f in os.listdir(toy_path)]

    for boost in ["N","Y"]:
        #Keep only files for given stats boost
        files = [k for k in all_files if f"Boost_{boost}" in k]

        for f in files:
            with open(toy_path+f, 'r') as t_file:
                t_result = json.load(t_file)

            for p in fit_pars:
                #Average of MINOS errors as they are very similar
                pull = (t_result[p][0] - init_vals[p]) / (0.5*(-t_result[p][1] + t_result[p][2]))

                pulls[p].append(pull)

        #Plot pulls
        pull_dict = {}

        for p in fit_pars:

            n_bins = 100
            low = -3.5
            high = 3.5

            #Fit pull distribution with Gaussian
            hist = TH1F("hist","",n_bins,low,high)
            for val in pulls[p]:
                hist.Fill(val)

            x = RooRealVar("x","",low,high)
            data_hist = RooDataHist("data_hist","",RooArgList(x),hist)

            mu = RooRealVar("mu","",0,low,high)
            sigma = RooRealVar("sigma","",1,0,10)
            sigma_ratio = RooRealVar("sigma_ratio","",1.0,0.,10.)
            sigma2 = RooFormulaVar("sigma2","@0*@1",RooArgList(sigma,sigma_ratio))
            f = RooRealVar("f","",0.5,0.,1.)

            g = RooGaussian("g","",x,mu,sigma)
            r = g.fitTo(data_hist)

            frame = x.frame()

            #Plot the results distribution
            fig, ax = plt.subplots(figsize=(8,8))

            data_hist.plotOn(frame)
            g.plotOn(frame)

            D = frame.getObject(0)

            data_arr = listsfromhist(D, overunder = True)

            #Plot data
            plt.errorbar(data_arr[0],data_arr[1],yerr=data_arr[2],ls=None,color='k',fmt='o',markersize=4,label="Toy results",alpha=0.6)

            #Plot Gaussian fit
            x_vals = np.linspace(low,high,1000)
            P = frame.getObject(1)
            pdf_arr = []
            for v in x_vals:
                pdf_arr.append(P.interpolate(v))
            plt.plot(x_vals,pdf_arr,color="crimson",linewidth=1.5,label="Gaussian fit")

            mean = [mu.getVal(),mu.getError()]
            width = [sigma.getVal(),sigma.getError()]

            pull_dict[p] = {"mu": mean,
                            "sigma": width
                            }

            plt.title("$\mu = %.3f \pm %.3f, \sigma = %.3f \pm %.3f$" % (mean[0],mean[1],width[0],width[1]),fontsize=20)

            ymin, ymax = plt.ylim()
            plt.ylim(0.0,1.05*ymax)
            plt.xlim(low,high)
            plt.xlabel(fit_pars[p]+" pull ($\sigma$)",fontsize=25)
            ax.tick_params(axis='both', which='major', labelsize=25)
            plt.legend(fontsize=20,loc='upper left')
            plt.tight_layout()
            #plt.show()
            fig.savefig(an.loc.ROOT+f"output/plots/pull_{p}_boost_{boost}.pdf")

        #Write pull params to dict, use to correct final results
        with open(an.loc.ROOT+f"output/json/unbinned_angular_fit_pulls_boost_{boost}.json", 'w') as fp:
            json.dump(pull_dict, fp, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()
