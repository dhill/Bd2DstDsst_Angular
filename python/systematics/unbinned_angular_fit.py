import analysis as an
import sys,os
import json
import matplotlib.pyplot as plt
import numpy as np

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

    syst_path = an.loc.WGEOS+"/Systematics/Unbinned_Fit/"

    #Fit par titles
    fit_pars = {"Hm_amp": "$|H_-|$",
    			"Hp_phi": "$\\phi_+$",
    			"Hm_phi": "$\\phi_-$"
    			}

    #Systematics categories
    syst_cats = {"fL": "Fixed $f_L$ in angular fit and $\\cos(\\theta_{X/D})$ acceptance",
                 "data_sw": "Use of $_s$Weighted data",
                 "acc_funcs": "Statistical uncertainty of accetpance functions"
                 }

    #Distribution for each fit param
    dists = {}
    for p in fit_pars:
        for s in syst_cats:
    	       dists["%s_%s" % (p,s)] = []

    #Get all results
    all_files = [f for f in os.listdir(syst_path)]

    for s in syst_cats:

        #Keep files belonging to systematics category
        files = [k for k in all_files if s in k]

        for f in files:
            with open(syst_path+f, 'r') as s_file:
                s_result = json.load(s_file)

                for p in fit_pars:
                    dists["%s_%s" % (p,s)].append(s_result[p][0])

    #Summarise systematics and store to json
    systs_dict = {}
    for p in fit_pars:
        systs_dict["%s_tot" % p] = 0.
        for s in syst_cats:
            systs_dict["%s_%s" % (p,s)] = np.std(dists["%s_%s" % (p,s)])
            systs_dict["%s_tot" % p] += systs_dict["%s_%s" % (p,s)]**2
        systs_dict["%s_tot" % p] = np.sqrt(systs_dict["%s_tot" % p])

    #Write systematics to dict
    with open(an.loc.ROOT+"output/json/unbinned_angular_fit_syst.json", 'w') as fp:
    	json.dump(systs_dict, fp, sort_keys=True, indent=4)

    #Make systematics summary table
    f = open(an.loc.ROOT+"output/tex/unbinned_angular_fit_syst.tex",'w')
    f.write("\\begin{table}[!h] \n")
    f.write("\\begin{center} \n")
    f.write("\\begin{tabular}{c|c|c|c} \n")
    f.write("Systematic & ")
    for p in fit_pars:
        f.write(fit_pars[p])
        if(p!="Hm_phi"):
            f.write(" & ")
    f.write("\\\\ \\hline \n")
    for s in syst_cats:
        f.write(syst_cats[s]+" & ")
        for p in fit_pars:
            f.write("%.4f" % systs_dict["%s_%s" % (p,s)])
            if(p!="Hm_phi"):
                f.write(" & ")
        f.write("\\\\ \\hline \n")
    f.write("Total & ")
    for p in fit_pars:
        f.write("%.4f" % systs_dict["%s_tot" % p])
        if(p!="Hm_phi"):
            f.write(" & ")
    f.write("\\\\ \n")
    f.write("\\end{tabular} \n")
    f.write("\\caption{Systematic uncertainties for the helicity parameters measured in the unbinned angular fit. \\label{tab:angular_systs}} \n")
    f.write("\\end{center} \n")
    f.write("\\end{table} \n")

if __name__ == '__main__':
    main()
