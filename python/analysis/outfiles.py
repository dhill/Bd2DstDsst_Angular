################# Values database and outputfiles

import pickle, os, csv
from analysis.locations import loc

## A pickle file to hold your numbers
dbloc = loc.OUT+"db.csv"

def initDB() :
    if not os.path.exists(dbloc) :
        os.system('touch '+dbloc)

def loadDB(name = dbloc) :

    dbfile = open(name)
    db = {}
    for row in csv.reader(dbfile,delimiter=':') :
        if len(row) != 2 : continue
        try :
            db[row[0]] = eval(row[1])
        except :
            db[row[0]] = row[1]
    dbfile.close()
    return db

def dumpDB(db,name=dbloc) :

    dbfile = open(name,'w')
    writer = csv.writer(dbfile,delimiter=":")

    l = []
    for k,v in sorted(db.items()) :
        print(k,v)
        writer.writerow([k,v])

    dbfile.close()


## Support for templares filling and output files bookkeping

from pyutils.editing.formatter import PartialFormatter as Formatter

listloc = loc.OUT+"outfiles_list.txt"

class Outfiles :

    def __init__(self) :

        if not os.path.exists(listloc) :
            f = open(listloc,"w")
            f.close()

        lines = open(listloc).readlines()
        self.files = {}
        for l in lines :
            toks = l.split()
            self.files[toks[0]] = toks[1]

    def writeline(self,name,text,clear=False) :

        self.write(name,text+"\n",clear)

    def write(self,name,text,clear=False) :

        if clear : f = open(loc.ROOT+self.files[name],"w")
        else : f = open(loc.ROOT+self.files[name],"a")
        f.write(text)
        f.close()

    def fill_template(self,name,template,db) :

        name = os.path.basename(name)
        
        if name not in self.files :
            self.create(name)

        tmp = open(loc.TMPS+template)

        fmt = Formatter()
        out = fmt.format(tmp.read(),**db)

        self.write(name,out,clear=True)

    def create(self,name,filename=None, extension=".txt") :

        if filename == None : filename = name
        if "." not in filename : filename += extension

        path = (loc.TABS+filename).replace(loc.ROOT,"")
        self.files[name] = path

        f = open(listloc,"w")
        for n,p in self.files.items() :
            f.write(n+" "+p)

outfiles = Outfiles()
