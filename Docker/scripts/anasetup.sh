source /root/run_kinit.sh

if [ "$1" = "snake" ]; 
    then
        echo "Setup with snakemake"
        source activate snake
        export PYTHONPATH=$ANAROOT/python:$ANAROOT/pyutils:$PYTHONPATH
    else
        source $ANAROOT/setup_env.sh
fi
