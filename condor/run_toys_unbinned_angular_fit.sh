#!/bin/sh

export ANAROOT=/home/dhill/Bd2DstDsst_Angular
source $ANAROOT/setup/setup.sh

for i in {1..10}
do
  python $ANAROOT/python/fitting/unbinned_angular_fit.py --Syst none --Toy Y --Boost $1
done
