import glob, os, sys
import numpy as np
import os.path
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy import stats
from scipy.stats import norm
import json
from collections import OrderedDict
from root_pandas import read_root
import pandas as pd
from scipy.stats import pearsonr
import pickle
from uncertainties import ufloat
import analysis as an

def binomial_err(N,k):
	return (1.0/N)*np.sqrt(k*(1-float(k)/N))

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)


def main():

	#Generator efficiencies
	gen_eff = {}
	gen_eff["Bd2DstDs"] = ufloat(0.25*(0.16656 + 0.16801 + 0.16778 + 0.16797), 0.001)
	gen_eff["Bd2DstDsst"] = ufloat(0.5*(0.1685 + 0.1636), 0.001)

	years = ["2015","2016"] #Sim09c for D* Ds and Sim09h for D* Ds* (2017 and 2018 have multiplicity bugs)
	mags = ["Up","Down"]

	#B0 -> D* Ds* part reco
	path = an.loc.WGEOS+"/tuples/MC/Bd2DstDsst"

	file_list = []
	file_list_gen = []

	for y in years:
		for m in mags:
			file_list.append("%s/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))
			file_list.append("%s_ReDecay/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))
			#Add ReDecay samples too
			file_list_gen.append("%s/%s_%s/Total.root" % (path,y,m))
			file_list_gen.append("%s_ReDecay/%s_%s/Total.root" % (path,y,m))

	df_dstdsst = read_root(file_list,"DecayTree")
	df_dstdsst["Ds_FD_ZSIG"] = (df_dstdsst["Ds_ENDVERTEX_Z"] - df_dstdsst["B_ENDVERTEX_Z"]) / np.sqrt( df_dstdsst["Ds_ENDVERTEX_ZERR"]**2 + df_dstdsst["B_ENDVERTEX_ZERR"]**2)

	df_dstdsst_gen = read_root(file_list_gen,"EventTuple/EventTuple")

	#B0 -> D* Ds full reco
	path = an.loc.WGEOS+"/tuples/MC/Bd2DstDs"

	file_list = []
	file_list_gen = []

	for y in years:
		for m in mags:
			file_list.append("%s/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))
			file_list_gen.append("%s/%s_%s/Total.root" % (path,y,m))

	df_dstds = read_root(file_list,"DecayTree")
	df_dstds["Ds_FD_ZSIG"] = (df_dstds["Ds_ENDVERTEX_Z"] - df_dstds["B_ENDVERTEX_Z"]) / np.sqrt( df_dstds["Ds_ENDVERTEX_ZERR"]**2 + df_dstds["B_ENDVERTEX_ZERR"]**2)

	df_dstds_gen = read_root(file_list_gen,"EventTuple/EventTuple")

	#Truth matching cuts
	df_dstdsst = df_dstdsst.query("B_BKGCAT==50 or B_BKGCAT==60")
	df_dstdsst = df_dstdsst.query("Ds_K1_PIDK>1 and Ds_K2_PIDK>1 and Ds_Pi_PIDK<10")
	df_dstds = df_dstds.query("B_BKGCAT==10 or B_BKGCAT==50 or B_BKGCAT==60")
	df_dstds = df_dstds.query("Ds_K1_PIDK>1 and Ds_K2_PIDK>1 and Ds_Pi_PIDK<10")

	eff_dstds = float(len(df_dstds)) / len(df_dstds_gen)
	eff_dstds_err = binomial_err(len(df_dstds_gen), len(df_dstds))

	eff_dstdsst = float(len(df_dstdsst)) / len(df_dstdsst_gen)
	eff_dstdsst_err = binomial_err(len(df_dstdsst_gen), len(df_dstdsst))

	#Additional stripping filtering efficiency for B0 -> D* Ds
	eff_strip_dstds = 0.0162

	e_dstds = ufloat(eff_strip_dstds*eff_dstds, eff_strip_dstds*eff_dstds_err)
	e_dstdsst = ufloat(eff_dstdsst, eff_dstdsst_err)

	#Multiply by generator eff
	e_dstds = e_dstds * gen_eff["Bd2DstDs"]
	e_dstdsst = e_dstdsst * gen_eff["Bd2DstDsst"]

	print("D* Ds eff = %s" % e_dstds)
	print("D* Ds* eff = %s" % e_dstdsst)

	r = e_dstds / e_dstdsst

	print("Eff ratio = %s" % r)

	eff_dict = {}
	eff_dict["Bd2DstDs"] = [e_dstds.n,e_dstds.s]
	eff_dict["Bd2DstDsst"] = [e_dstdsst.n,e_dstdsst.s]
	eff_dict["r"] = [r.n,r.s]

	#Also add B0 -> D* Ds* full reco for comparison
	path = an.loc.WGEOS+"/tuples/MC/Bd2DstDsst"

	file_list = []
	file_list_gen = []

	for y in years:
		for m in mags:
			file_list.append("%s/%s_%s/Total_Bd2DstDsst_Vars.root" % (path,y,m))
			file_list.append("%s_ReDecay/%s_%s/Total_Bd2DstDsst_Vars.root" % (path,y,m))

	df_dstdsst_fullreco = read_root(file_list,"DecayTree")
	df_dstdsst_fullreco["Ds_FD_ZSIG"] = (df_dstdsst_fullreco["Ds_ENDVERTEX_Z"] - df_dstdsst_fullreco["B_ENDVERTEX_Z"]) / np.sqrt( df_dstdsst_fullreco["Ds_ENDVERTEX_ZERR"]**2 + df_dstdsst_fullreco["B_ENDVERTEX_ZERR"]**2)

	#Truth matching cuts
	df_dstdsst_fullreco = df_dstdsst_fullreco.query("B_BKGCAT==10 or B_BKGCAT==50 or B_BKGCAT==60")

	eff_dstdsst_fullreco = float(len(df_dstdsst_fullreco)) / len(df_dstdsst_gen)
	eff_dstdsst_fullreco_err = binomial_err(len(df_dstdsst_gen), len(df_dstdsst_fullreco))

	e_dstdsst_fullreco = ufloat(eff_dstdsst_fullreco, eff_dstdsst_fullreco_err)

	#Multiply by generator eff
	e_dstdsst_fullreco = e_dstdsst_fullreco * gen_eff["Bd2DstDsst"]

	#Ratio of part reco to full reco B0 -> D* Ds* eff
	r_full_part = e_dstdsst_fullreco / e_dstdsst

	eff_dict["Bd2DstDsst_FullReco"] = [e_dstdsst_fullreco.n,e_dstdsst_fullreco.s]
	eff_dict["r_full_part"] = [r_full_part.n,r_full_part.s]

	with open(an.loc.ROOT+'output/json/effs.json', 'w') as fp:
		json.dump(eff_dict, fp, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()
