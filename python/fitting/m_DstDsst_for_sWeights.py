import numpy as np
import sys, os
import json
import os.path
from os import path
from root_pandas import to_root, read_root
import pandas as pd
import matplotlib
from collections import OrderedDict
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooAbsData, RooStats, gInterpreter, gPad, RooChebychev, RooSimultaneous, RooAddPdf, RooCategory, RooExponential, RooFit, TCanvas, TFile, TTree, TChain, RooArgList, RooArgSet, RooGaussian, RooCBShape, RooDataSet, RooDataHist, RooPlot, RooRealVar, RooFormulaVar
from listsfromhist import listsfromhist
import analysis as an
from num2tex import num2tex
import argparse

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def PlotPull(D,low,high,bin_width,title,plot_name):
	data_arr = listsfromhist(D, overunder = True)

	fig, ax = plt.subplots(figsize=(10,4))

	#Plot data
	plt.bar(data_arr[0],data_arr[1],color='gray',width=2.0)

	plt.ylabel("Residual ($\\sigma$)",fontsize=25)
	plt.xlabel(title,fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(low,high-1)
	plt.axhline(y=3.0,color='crimson',linestyle='--',alpha=0.3)
	plt.axhline(y=-3.0,color='crimson',linestyle='--',alpha=0.3)
	plt.ylim(-5.,5.)
	#plt.tight_layout()
	margins = {  #     vvv margin in inches
				"left"   : 0.1,
				"bottom" : 0.20,
				"right"  : 0.98,
				"top"    : 0.98
				}
	fig.subplots_adjust(**margins)
	#plt.show()
	fig.savefig(plot_name)

def Plot(frame,low,high,bin_width,title,plot_name):
	D = frame.getObject(0)
	data_arr = listsfromhist(D, overunder = True)

	fig, ax = plt.subplots(figsize=(10,6))

	#Plot data
	plt.errorbar(data_arr[0],data_arr[1],yerr=data_arr[2],ls=None,color='k',fmt='o',markersize=2,label="Data")

	plt.ylabel("Candidates / (%.2f MeV/$c^2$)" % bin_width,fontsize=25)
	plt.xlabel(title,fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(low,high-1)

	x_vals = np.linspace(low,high,1000)

	#Total
	P = frame.getObject(1)
	pdf_arr = []
	for v in x_vals:
		pdf_arr.append(P.interpolate(v))
	plt.plot(x_vals,pdf_arr,color="black",linewidth=1.0,label="Total fit")

	#Bkg
	P = frame.getObject(3)
	pdf_arr_bkg = []
	for v in x_vals:
		pdf_arr_bkg.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="royalblue")

	#Signal
	P = frame.getObject(2)
	pdf_arr_sig = []
	for v in x_vals:
		pdf_arr_sig.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="crimson")

	#Sig + Bkg
	plt.fill_between(x_vals,[sum(x) for x in zip(pdf_arr_sig, pdf_arr_bkg)],color="crimson",alpha=0.8,label="$B^0 \\to D^{*-} D_s^{*+}$")
	#Bkg only in front
	plt.fill_between(x_vals,pdf_arr_bkg,color="royalblue",label="Combinatorial")

	ymin, ymax = ax.get_ylim()
	plt.ylim(0.1,ymax*1.5)
	#plt.yscale("log")
	plt.legend(fontsize=20,loc='upper right')
	#plt.tight_layout()
	margins = {  #     vvv margin in inches
				"left"   : 0.1,
				"bottom" : 0.18,
				"right"  : 0.98,
				"top"    : 0.98
				}
	fig.subplots_adjust(**margins)

	plt.show()
	fig.savefig(plot_name)


def main():

	#Load data
	years = ["2015","2016","2017","2018"]
	mags = ["Up","Down"]

	path = f"{an.loc.WGEOS}/tuples/data/Bd2DstDs"

	tree = TChain("DecayTree")

	angle_type = "VV"

	vars = ["B_D0constDsconstPVconst_B_M",
			"B_M",
			"D0_M",
			"Ds_M",
			"Dst_M",
			"Dst_ID",
			"Dst_Delta_M",
			"costheta_D_reco",
			f"costheta_X_{angle_type}_reco",
			f"chi_{angle_type}_reco",
			"B_L0HadronDecision_TOS",
			"B_L0Global_TIS",
			"B_Hlt1TrackMVADecision_TOS",
			"B_Hlt1TwoTrackMVADecision_TOS",
			"B_Hlt2Topo2BodyDecision_TOS",
			"B_Hlt2Topo3BodyDecision_TOS",
			"B_Hlt2Topo4BodyDecision_TOS",
			"Dsst_Gamma_PT",
			"Dsst_Delta_M",
			"DstDs_M",
			"Dsst_M",
			"B_PX",
			"B_PY",
			"B_PZ",
			"B_PE",
			"Dsst_PX",
			"Dsst_PY",
			"Dsst_PZ",
			"Dsst_PE",
			"Ds_PX",
			"Ds_PY",
			"Ds_PZ",
			"Ds_PE",
			"Dsst_PT",
			"Dst_PT",
			"Dsst_Gamma_PX",
			"Dsst_Gamma_PY",
			"Dsst_Gamma_PZ",
			"Dsst_Gamma_PE",
			"costheta_D_Breco_reco",
			"costheta_X_VV_Breco_reco",
			"chi_VV_Breco_reco",
			"Ds_ENDVERTEX_Z",
			"B_ENDVERTEX_Z",
			"Ds_ENDVERTEX_ZERR",
			"B_ENDVERTEX_ZERR",
			"Ds_K1_PIDK",
			"Ds_K2_PIDK",
			"Ds_Pi_PIDK"
			]

	for y in years:
		for m in mags:
			tree.Add(f"{path}/{y}_{m}/Total_Bd2DstDsst_Vars.root")

	tree.SetBranchStatus("*",0)

	for v in vars:
		tree.SetBranchStatus(v,1)

	#Cuts as defined in analysis/cuts/__init__.py
	cuts = an.cuts.Bd2DstDsst

	tree = tree.CopyTree(cuts)
	print(f"Events in tree: {tree.GetEntries()}")

	#Make RooDataSet containing fit variable (B mass) and the angles we want to look at sWeighted
	b_low = 5175.
	b_high = 5500.
	B_M = RooRealVar("B_D0constDsconstPVconst_B_M","",b_low,b_high)
	bin_width = 2.0
	num_bins = int((b_high-b_low)/bin_width)
	B_M.setBins(num_bins)

	d_low = 100
	d_high = 200
	Dsst_Delta_M = RooRealVar("Dsst_Delta_M","",d_low,d_high)

	costheta_D = RooRealVar("costheta_D_reco","",-1,1)
	costheta_X = RooRealVar(f"costheta_X_{angle_type}_reco","",-1,1)
	chi = RooRealVar(f"chi_{angle_type}_reco","",-np.pi,np.pi)
	Dst_Delta_M = RooRealVar("Dst_Delta_M","",100,400)
	Dst_ID = RooRealVar("Dst_ID","",-1e6,1e6)
	Dsst_M = RooRealVar("Dsst_M","",-1e10,1e10)
	L0_TOS = RooRealVar("B_L0HadronDecision_TOS","",-10000000,100000000)
	L0_TIS = RooRealVar("B_L0Global_TIS","",-10000000,100000000)
	B_PX = RooRealVar("B_PX","",-1e10,1e10)
	B_PY = RooRealVar("B_PY","",-1e10,1e10)
	B_PZ = RooRealVar("B_PZ","",-1e10,1e10)
	B_PE = RooRealVar("B_PE","",-1e10,1e10)
	Dsst_PX = RooRealVar("Dsst_PX","",-1e10,1e10)
	Dsst_PY = RooRealVar("Dsst_PY","",-1e10,1e10)
	Dsst_PZ = RooRealVar("Dsst_PZ","",-1e10,1e10)
	Dsst_PE = RooRealVar("Dsst_PE","",-1e10,1e10)
	Dsst_PT = RooRealVar("Dsst_PT","",-1e10,1e10)
	Ds_PX = RooRealVar("Ds_PX","",-1e10,1e10)
	Ds_PY = RooRealVar("Ds_PY","",-1e10,1e10)
	Ds_PZ = RooRealVar("Ds_PZ","",-1e10,1e10)
	Ds_PE = RooRealVar("Ds_PE","",-1e10,1e10)
	Ds_M = RooRealVar("Ds_M","",-1e10,1e10)
	D0_M = RooRealVar("D0_M","",-1e10,1e10)
	Dst_PT = RooRealVar("Dst_PT","",-1e10,1e10)
	Dsst_Gamma_PX = RooRealVar("Dsst_Gamma_PX","",-1e10,1e10)
	Dsst_Gamma_PY = RooRealVar("Dsst_Gamma_PY","",-1e10,1e10)
	Dsst_Gamma_PZ = RooRealVar("Dsst_Gamma_PZ","",-1e10,1e10)
	Dsst_Gamma_PE = RooRealVar("Dsst_Gamma_PE","",-1e10,1e10)
	Dsst_Gamma_PT = RooRealVar("Dsst_Gamma_PT","",-1e10,1e10)
	costheta_D_Breco = RooRealVar("costheta_D_Breco_reco","",-1e10,1e10)
	costheta_X_VV_Breco = RooRealVar("costheta_X_VV_Breco_reco","",-1e10,1e10)
	chi_VV_Breco = RooRealVar("chi_VV_Breco_reco","",-1e10,1e10)
	DstDs_M = RooRealVar("DstDs_M","",-1e10,1e10)

	args = RooArgSet(B_M,costheta_D,costheta_X,chi,Dst_Delta_M,Dsst_Delta_M,Dsst_M,L0_TOS,L0_TIS)
	args.add(B_PX)
	args.add(B_PY)
	args.add(B_PZ)
	args.add(B_PE)
	args.add(Dsst_PX)
	args.add(Dsst_PY)
	args.add(Dsst_PZ)
	args.add(Dsst_PE)
	args.add(Dsst_PT)
	args.add(Ds_PX)
	args.add(Ds_PY)
	args.add(Ds_PZ)
	args.add(Ds_PE)
	args.add(Dst_PT)
	args.add(Dst_ID)
	args.add(Dsst_Gamma_PX)
	args.add(Dsst_Gamma_PY)
	args.add(Dsst_Gamma_PZ)
	args.add(Dsst_Gamma_PE)
	args.add(Dsst_Gamma_PT)
	args.add(Ds_M)
	args.add(costheta_D_Breco)
	args.add(costheta_X_VV_Breco)
	args.add(chi_VV_Breco)
	args.add(DstDs_M)

	data = RooDataSet("data","data",args,RooFit.Import(tree))
	data.Print()

	#B mass signal PDF with tails from MC fit
	with open(an.loc.ROOT+"output/json/MC_Bd2DstDsst_fullreco.json","r") as f:
		shape_pars = json.load(f)

	B_mu = RooRealVar("B_mu","Signal $\\mu$",shape_pars["mu"][0],5200,5400)
	B_sigma = RooRealVar("B_sigma","Signal $\\sigma$",shape_pars["sigma"][0],0,100)

	B_alpha_R = RooRealVar("B_alpha_R","Signal $\\alpha_R$ (MC constrained)",shape_pars["alpha_R"][0],-10.0,-1e-10)
	B_alpha_R_const = RooGaussian("B_alpha_R_const","", B_alpha_R, RooFit.RooConst(shape_pars["alpha_R"][0]), RooFit.RooConst(shape_pars["alpha_R"][1]))

	B_n_R = RooRealVar("B_n_R","Signal $n_R$ (MC constrained)",shape_pars["n_R"][0],0,1000)
	B_n_R_const = RooGaussian("B_n_R_const","", B_n_R, RooFit.RooConst(shape_pars["n_R"][0]), RooFit.RooConst(shape_pars["n_R"][1]))

	B_alpha_L = RooRealVar("B_alpha_L","Signal $\\alpha_L$ (MC constrained)",shape_pars["alpha_L"][0],0.0,10.0)
	B_alpha_L_const = RooGaussian("B_alpha_L_const","", B_alpha_L, RooFit.RooConst(shape_pars["alpha_L"][0]), RooFit.RooConst(shape_pars["alpha_L"][1]))

	B_n_L = RooRealVar("B_n_L","Signal $n_L$ (MC constrained)",shape_pars["n_L"][0],0,1000)
	B_n_L_const = RooGaussian("B_n_L_const","", B_n_L, RooFit.RooConst(shape_pars["n_L"][0]), RooFit.RooConst(shape_pars["n_L"][1]))

	B_frac = RooRealVar("B_frac","Signal CB fraction (MC constrained)",shape_pars["frac"][0],0.,1.)
	B_frac_const = RooGaussian("B_frac_const","", B_frac, RooFit.RooConst(shape_pars["frac"][0]), RooFit.RooConst(shape_pars["frac"][1]))

	B_sigL_pdf = RooCBShape("B_sigL_pdf","",B_M,B_mu,B_sigma,B_alpha_L,B_n_L)
	B_sigR_pdf = RooCBShape("B_sigR_pdf","",B_M,B_mu,B_sigma,B_alpha_R,B_n_R)
	B_sig_pdf = RooAddPdf("B_sig_pdf","",RooArgList(B_sigL_pdf,B_sigR_pdf),RooArgList(B_frac))

	#B mass bkg pdf
	B_lam0 = RooRealVar("B_lam0","Chebyshev background $p_0$",0.,-1,1)
	B_lam1 = RooRealVar("B_lam1","Chebyshev background $p_1$",0.,-1,1)
	B_lam2 = RooRealVar("B_lam2","",0,-1,1)
	B_bkg_pdf = RooChebychev("B_bkg_pdf","B_bkg_pdf",B_M,RooArgList(B_lam0,B_lam1))#,B_lam2))

	#Yields
	n_sig = RooRealVar("n_sig","$B^0 \\to D^{*-}D_s^{*+}$ yield",0.6*data.sumEntries(),0,10e6)
	n_bkg = RooRealVar("n_bkg","Background yield",0.4*data.sumEntries(),0,1e6)

	#Total PDF
	B_tot_pdf = RooAddPdf("B_tot_pdf","",RooArgList(B_sig_pdf,B_bkg_pdf),RooArgList(n_sig,n_bkg))

	#Gaussian constraints on the signal shape
	constraints = RooArgSet()
	constraints.add(B_alpha_R_const)
	constraints.add(B_n_R_const)
	constraints.add(B_alpha_L_const)
	constraints.add(B_n_L_const)
	constraints.add(B_frac_const)

	result = B_tot_pdf.fitTo(data,RooFit.Save(),RooFit.NumCPU(8,0),RooFit.Extended(True),RooFit.Optimize(False),RooFit.Offset(False),RooFit.Minimizer("Minuit2","migrad"),RooFit.Strategy(2),RooFit.ExternalConstraints(constraints))
	result.Print("V")

	#Save RooFitResult to file
	r_file = TFile(f"{an.loc.ROOT}output/roofitresults/m_DstDsst_for_sWeights.root", "RECREATE")
	r_file.cd()
	result.Write()
	r_file.Close()

	#Write tex table of results
	params = result.floatParsFinal()
	tex_file = open(an.loc.ROOT+"output/tex/m_DstDsst_for_sWeights.tex","w")
	tex_file.write("\\begin{table}[!h] \n")
	tex_file.write("\\begin{center} \n")
	tex_file.write("\\begin{tabular}{l|c} \n")
	tex_file.write("Parameter & Fit result \\\\ \\hline \n")
	for p in params:
		title = p.GetTitle()
		val = p.getVal()
		err = p.getError()
		tex_file.write("%s & " % title)
		if(val<0.1):
			tex_file.write("${:.2e} \\pm {:.2e}$ \\\\ \n".format(num2tex(val),num2tex(err)))
		else:
			tex_file.write("$%.2f \\pm %.2f$ \\\\ \n" % (val,err))
	tex_file.write("\\end{tabular} \n")
	tex_file.write("\\caption{ Results of the $m(D^{*-}D_s^{*+})$ fit. \label{tab:m_DstDsst_for_sWeights_results}} \n")
	tex_file.write("\\end{center} \n")
	tex_file.write("\\end{table} \n")

	#Save signal yield to json
	yield_dict = {}
	yield_dict["n_Bd2DstDsst_FullReco"] = [n_sig.getVal(),n_sig.getError()]

	with open(an.loc.ROOT+"output/json/m_DstDsst_for_sWeights.json", 'w') as fp:
		json.dump(yield_dict, fp, sort_keys=True, indent=4)

	B_frame = B_M.frame()
	data.plotOn(B_frame)
	B_tot_pdf.plotOn(B_frame)
	pull_hist = B_frame.pullHist()
	B_tot_pdf.plotOn(B_frame,RooFit.Components("B_sig_pdf"),RooFit.LineColor(2))
	B_tot_pdf.plotOn(B_frame,RooFit.Components("B_bkg_pdf"),RooFit.LineColor(2))

	#Output location for figs
	plot_loc = f"{an.loc.ROOT}output/plots/"
	x_title = "$m(D^{*-}D_s^{*+})$ [MeV/$c^2$]"
	Plot(B_frame,b_low,b_high,bin_width,x_title,plot_loc+"m_DstDsst_for_sWeights_fit.pdf")
	#Residuals
	PlotPull(pull_hist,b_low,b_high,bin_width,x_title,plot_loc+"m_DstDsst_for_sWeights_pull.pdf")

	#############
	### sPlot ###
	#############

	#Create the sPlot data from the fit to B_M.  The sweights computed here are valid for plotting angles
	sDataX = RooStats.SPlot("sData","sData", data, B_tot_pdf, RooArgList(n_sig, n_bkg))

	print("Signal yield is %s. From sWeights it is %s" % (n_sig.getVal(),sDataX.GetYieldFromSWeight("n_sig")))
	print("Bkg yield is %s. From sWeights it is %s" % (n_bkg.getVal(),sDataX.GetYieldFromSWeight("n_bkg")))

	#Create weighted data set
	dataw_bkg = RooDataSet(data.GetName(),data.GetTitle(),data,data.get(),"","n_bkg_sw")

	out_tree = dataw_bkg.GetClonedTree()

	out_file = TFile(f"{an.loc.ROOT}output/rootfiles/Bd2DstDsst_sWeights.root","RECREATE")
	out_file.cd()
	out_tree.Write()
	out_file.Write()
	out_file.Close()

if __name__ == '__main__':
    main()
