import sys,os
import json
import analysis as an
from ROOT import TFile, RooFitResult

def main():

    #Load input feed-down fractions
    with open(an.loc.ROOT+'output/json/feed_down.json','r') as file:
        feed_dict = json.load(file)

    #Load RooFitResult with the final values of the feed-down fractions
    r_file = TFile.Open(an.loc.ROOT+"output/roofitresults/m_DstDs_for_fL.root")
    result = r_file.Get("fitresult_tot_pdf_data_binned")

    result_dict = {}

    name_dict = {"Bd2D2420Ds": ["frac_d1ds","$B^0 \\to (D_1(2420)^- \\to D^{*-} \piz) D_s^+$"],
                 "Bd2D2420Dsst": ["frac_d1dsst","$B^0 \\to (D_1(2420)^- \\to D^{*-} \piz) (D_s^{*+} \\to D_s^+ \gamma)$"],
                 "Bd2DstDs2460_Dsgamma": ["frac_dstds2460_dsgamma","$B^0 \\to D^{*-} (D_{s1}(2460)^+ \\to D_s^+ \gamma)$"],
                 "Bd2DstDs2460_Dsstpi0": ["frac_dstds2460_dsstpi0","$B^0 \\to D^{*-} (D_{s1}(2460)^+ \\to D_s^+ \piz)$"]
                }

    pars = result.floatParsFinal()

    for i in range(0,len(pars)):

        par = pars[i]
        name = par.GetName()

        for p in name_dict:
            if(name==name_dict[p][0]):
                result_dict[p] = [par.getVal(),par.getError()]


    f = open(an.loc.ROOT+"output/tex/feed_down.tex","w")
    f.write("\\begin{table}[!h] \n")
    f.write("\\begin{center} \n")
    f.write("\\begin{tabular}{c|c|c} \n")
    f.write("Component & Constraint & Result \\\\ \\hline \n")
    for p in name_dict:
        f.write("%s & $%.3f \\pm %.3f$ & $%.3f \\pm %.3f$ \\\\ \n" % (name_dict[p][1], feed_dict[p][0], feed_dict[p][1], result_dict[p][0], result_dict[p][1]))
    f.write("\\end{tabular} \n")
    f.write("\\caption{Results for the Gaussian constrained feed-down components in the fit, which are related via ratios to the freely varying $B^0 \\to D^{*-}D_s^+$ yield. \\label{tab:feed_down_results}} \n")
    f.write("\\end{center} \n")
    f.write("\\end{table} \n")


if __name__ == '__main__':
    main()
