import numpy as np
import sys, os
import json
import os.path
from os import path
from root_pandas import to_root, read_root
import pandas as pd
import matplotlib
from collections import OrderedDict
import matplotlib.pyplot as plt
from uncertainties import ufloat
import analysis as an

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

	#BFs
	BF_Bd2DstDs = ufloat(8.0e-3,1.1e-3)
	D2420_Dst_ratio = ufloat(0.2,0.04) #Assume 20% uncertainty
	BF_Bd2D2420Ds = 2 * BF_Bd2DstDs * D2420_Dst_ratio #factor 2 for B0 and B+
	BF_Bd2DstDs2460 = ufloat(9.3e-3,2.2e-3)
	BF_Ds24602Dsgamma = ufloat(0.18,0.04)
	BF_Ds24602Dsstpi0 = ufloat(0.48,0.11)
	BF_Bd2DstDs2460_Dsgamma = BF_Bd2DstDs2460 * BF_Ds24602Dsgamma
	BF_Bd2DstDs2460_Dsstpi0 = BF_Bd2DstDs2460 * BF_Ds24602Dsstpi0
	BF_Bd2DstDsst = ufloat(0.0177,0.0014)
	BF_Bd2D2420Dsst = 2 * D2420_Dst_ratio * BF_Bd2DstDsst #factor 2 for B0 and B+

	#Relative efficiency with 10% uncertainty
	rel_eff = ufloat(1.0,0.1)

	modes = {}

	modes["Bd2DstDs"] = ["$B^0 \\to D^{*-} D_s^+", BF_Bd2DstDs]
	modes["Bd2D2420Ds"] = ["$B^0 \\to D_1(2420)^- D_s^+", BF_Bd2D2420Ds]
	modes["Bd2DstDs2460_Dsgamma"] = ["$B^0 \\to D^{*-} (D_{s1}(2460)^+ \\to D_s^+ \\gamma$", BF_Bd2DstDs2460_Dsgamma]
	modes["Bd2DstDs2460_Dsstpi0"] = ["$B^0 \\to D^{*-} (D_{s1}(2460)^+ \\to D_s^{*+} \\pi^0$", BF_Bd2DstDs2460_Dsstpi0]
	modes["Bd2D2420Dsst"] = ["$B^0 \\to D_1(2420)^0 D_s^{*+}", BF_Bd2D2420Dsst]

	path = an.loc.WGEOS+"/RapidSim_tuples/"

	df = {}

	n = {}
	eff = {}

	for m in modes:
		df[m] = read_root(path+"%s/model_KKPi_LHCb_tree.root" % m,"DecayTree", columns=["B_M"])
		n[m] = float(len(df[m].query("B_M>4.9")))
		eff[m] = n[m]/len(df[m])

	#Fraction relative to B0 -> D* Ds
	f = {}

	#Dictionary of effciiencies and errors for use in fL fit
	eff_dict = {}

	for m in modes:
		f[m] = (modes[m][1]/modes["Bd2DstDs"][1]) * (n[m]/n["Bd2DstDs"]) * rel_eff
		if(m!="Bd2DstDs"):
			print("Mass cut efficiency for %s = %s" % (m,eff[m]))
			print("f for %s = %s" % (m,f[m]))
			eff_dict[m] = [f[m].n, f[m].s]

	#Save eff values to file
	with open(an.loc.ROOT+'output/json/feed_down.json', 'w') as f:
		json.dump(eff_dict, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()
