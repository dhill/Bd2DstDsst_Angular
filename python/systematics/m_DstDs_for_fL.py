import analysis as an
import sys,os
import json
import matplotlib.pyplot as plt
import numpy as np
from collections import OrderedDict
from ROOT import TFile, TH1F, RooDataHist, RooArgList, RooArgSet, RooRealVar, RooFitResult, RooGaussian, RooPlot, RooFormulaVar, RooAddPdf

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

    syst_path = an.loc.WGEOS+"/Systematics/m_DstDs_for_fL/"

    #Fit par tex titles and RooRealVar names
    fit_pars = OrderedDict()
    fit_pars["n_Bd2DstDsst_Dsst2Dsgamma"] = ["n_sig","$\\mathcal{N}(B^0 \\to D^{*-} (D_s^{*+} \\to D_s^+ \\gamma))$"]
    fit_pars["n_Bd2DstDs"] = ["n_dstds","$\\mathcal{N}(B^0 \\to D^{*-} D_s^+)$"]
    fit_pars["fL"] = ["f_L","$f_L$"]

    #Systematics categories
    syst_cats = {"PDFs": "Fixed PDF shape parameters",
                 "BFs": "Fixed branching fractions"
                 }

    #Distributions for each fit param
    dists = {}
    for p in fit_pars:
        for s in syst_cats:
    	       dists["%s_%s" % (p,s)] = []

    #Get all results
    all_files = [f for f in os.listdir(syst_path)]

    for s in syst_cats:

        #Keep files belonging to systematics category
        files = [k for k in all_files if s in k]

        for f in files:

            s_file = TFile.Open(syst_path+f)
            s_result = s_file.Get("fitresult_tot_pdf_data_binned")

            pars = s_result.floatParsFinal()

            for i in range(0,len(pars)):

                par = pars[i]
                name = par.GetName()

                for p in fit_pars:

                    if(name==fit_pars[p][0]):
                        #Remove very small number of outliers
                        if(name=="f_L" and par.getVal()<0.65 and par.getVal()>0.5):
                            dists["%s_%s" % (p,s)].append(par.getVal())
                        elif(name=="n_sig" and par.getVal()>35000 and par.getVal()<38000):
                            dists["%s_%s" % (p,s)].append(par.getVal())
                        elif(name=="n_dstds" and par.getVal()>20000 and par.getVal()<21000):
                            dists["%s_%s" % (p,s)].append(par.getVal())

    #Summarise systematics and store to json
    systs_dict = {}
    for p in fit_pars:
        systs_dict["%s_tot" % p] = 0.
        for s in syst_cats:
            systs_dict["%s_%s" % (p,s)] = np.std(dists["%s_%s" % (p,s)])
            systs_dict["%s_tot" % p] += systs_dict["%s_%s" % (p,s)]**2
        systs_dict["%s_tot" % p] = np.sqrt(systs_dict["%s_tot" % p])

    #Write systematics to dict
    with open(an.loc.ROOT+"output/json/m_DstDs_for_fL_syst.json", 'w') as fp:
    	json.dump(systs_dict, fp, sort_keys=True, indent=4)

    #Make systematics summary table
    f = open(an.loc.ROOT+"output/tex/m_DstDs_for_fL_syst.tex",'w')
    f.write("\\begin{table}[!h] \n")
    f.write("\\begin{center} \n")
    f.write("\\begin{tabular}{c|c|c|c} \n")
    f.write("Systematic & ")
    for p in fit_pars:
        f.write(fit_pars[p][1])
        if(p!="fL"):
            f.write(" & ")
    f.write("\\\\ \\hline \n")
    for s in syst_cats:
        f.write(syst_cats[s]+" & ")
        for p in fit_pars:
            if(p=="fL"):
                f.write("%.4f" % systs_dict["%s_%s" % (p,s)])
            else:
                f.write("%.0f & " % systs_dict["%s_%s" % (p,s)])
        f.write("\\\\ \\hline \n")
    f.write("Total & ")
    for p in fit_pars:
        if(p=="fL"):
            f.write("%.4f" % systs_dict["%s_tot" % p])
        else:
            f.write("%.0f & " % systs_dict["%s_tot" % p])
    f.write("\\\\ \n")
    f.write("\\end{tabular} \n")
    f.write("\\caption{Systematic uncertainties on $f_L$ as measured in the $m(D^{*-} D_s^+)$ fit. The systematic uncertainties on the yields which enter the branching fraction ratio are also listed.\\label{tab:fL_systs}} \n")
    f.write("\\end{center} \n")
    f.write("\\end{table} \n")

if __name__ == '__main__':
    main()
