import sys,os
import json
import analysis as an
from uncertainties import ufloat, correlated_values

def main():

    #File containing various results defined as parameters for use in document
    f = open(an.loc.ROOT+"output/tex/results_def.tex",'w')

    #fL
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL.json','r') as file:
        fL_result = json.load(file)
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL_syst.json','r') as file:
        fL_syst = json.load(file)
    f.write("\\def\\fLstat{%.3f \\pm %.3f} \n" % (fL_result["fL"][0], fL_result["fL"][1]))
    f.write("\\def\\fLsyst{%.3f \\pm %.3f \\pm %.3f} \n" % (fL_result["fL"][0], fL_result["fL"][1], fL_syst["fL_tot"]))

    #Values for BF measurement

    #Yields
    f.write("\\def\\ndstdsstat{%.0f \\pm %.0f} \n" % (fL_result["n_Bd2DstDs"][0], fL_result["n_Bd2DstDs"][1]))
    f.write("\\def\\ndstdssyst{%.0f \\pm %.0f \\pm %.0f} \n" % (fL_result["n_Bd2DstDs"][0], fL_result["n_Bd2DstDs"][1], fL_syst["n_Bd2DstDs_tot"]))
    f.write("\\def\\ndstdsststat{%.0f \\pm %.0f} \n" % (fL_result["n_Bd2DstDsst_Dsst2Dsgamma"][0], fL_result["n_Bd2DstDsst_Dsst2Dsgamma"][1]))
    f.write("\\def\\ndstdsstsyst{%.0f \\pm %.0f \\pm %.0f} \n" % (fL_result["n_Bd2DstDsst_Dsst2Dsgamma"][0], fL_result["n_Bd2DstDsst_Dsst2Dsgamma"][1], fL_syst["n_Bd2DstDsst_Dsst2Dsgamma_tot"]))

    #Effs
    with open(an.loc.ROOT+'/output/json/effs.json','r') as file:
        effs = json.load(file)
    f.write("\\def\\effdstds{%.4f \\pm %.4f} \n" % (100*effs["Bd2DstDs"][0], 100*effs["Bd2DstDs"][1]))
    f.write("\\def\\effdstdsst{%.4f \\pm %.4f} \n" % (100*effs["Bd2DstDsst"][0], 100*effs["Bd2DstDsst"][1]))
    f.write("\\def\\effdstdsstfullreco{%.4f \\pm %.4f} \n" % (100*effs["Bd2DstDsst_FullReco"][0], 100*effs["Bd2DstDsst_FullReco"][1]))
    f.write("\\def\\effratio{%.3f \\pm %.3f} \n" % (effs["r"][0], effs["r"][1]))
    f.write("\\def\\effratiofullpart{%.3f \\pm %.3f} \n" % (effs["r_full_part"][0], effs["r_full_part"][1]))
    #BF ratio
    with open(an.loc.ROOT+'/output/json/BF_ratio.json','r') as file:
        BF_ratio = json.load(file)
    f.write("\\def\\nratio{%.3f \\pm %.3f \\pm %.3f} \n" % (BF_ratio["yield_ratio"][0], BF_ratio["yield_ratio"][1], BF_ratio["yield_ratio"][2]))
    f.write("\\def\\BFratio{%.3f \\pm %.3f \\pm %.3f} \n" % (BF_ratio["BF_ratio"][0], BF_ratio["BF_ratio"][1], BF_ratio["BF_ratio"][2]))

    #Yield of B0 -> D* Ds* fully reco
    with open(an.loc.ROOT+'/output/json/m_DstDsst_for_sWeights.json','r') as file:
        yield_dict = json.load(file)
    f.write("\\def\\ndstdsstfullreco{%.0f \\pm %.0f} \n" % (yield_dict["n_Bd2DstDsst_FullReco"][0], yield_dict["n_Bd2DstDsst_FullReco"][1]))

    #Helicity parameters

    #H0 derived from fL
    fL_stat_u = ufloat(fL_result["fL"][0], fL_result["fL"][1])
    fL_syst_u = ufloat(fL_result["fL"][0], fL_syst["fL_tot"])
    H0_stat_u = fL_stat_u**(0.5)
    H0_syst_u = fL_syst_u**(0.5)
    f.write("\\def\\Hzamp{%.3f \\pm %.3f \\pm %.3f} \n" % (H0_stat_u.n, H0_stat_u.s, H0_syst_u.s))

    #Unbnned fit results
    with open(an.loc.ROOT+'/output/json/unbinned_angular_fit_results.json','r') as file:
        unbinned_fit_results_dict = json.load(file)
    with open(an.loc.ROOT+'/output/json/unbinned_angular_fit_syst.json','r') as file:
        unbinned_fit_syst_dict = json.load(file)
    with open(an.loc.ROOT+'/output/json/unbinned_angular_fit_pulls_boost_N.json','r') as file:
        unbinned_fit_pulls_dict = json.load(file)

    #Pull for H-
    Hm_pull = unbinned_fit_pulls_dict["Hm_amp"]["mu"][0]
    f.write("\\def\\Hmpull{%.2f\\sigma} \n" % Hm_pull)
    #Correct Hm for pull
    Hm_err = 0.5*(-unbinned_fit_results_dict["Hm_amp"][1] + unbinned_fit_results_dict["Hm_amp"][2])
    Hm_corr = unbinned_fit_results_dict["Hm_amp"][0] - Hm_pull*Hm_err
    f.write("\\def\\Hmamp{%.3f \\pm %.3f \\pm %.3f} \n" % (Hm_corr,Hm_err,unbinned_fit_syst_dict["Hm_amp_tot"]))
    Hm_phi = unbinned_fit_results_dict["Hm_phi"][0]
    Hm_phi_err = 0.5*(-unbinned_fit_results_dict["Hm_phi"][1] + unbinned_fit_results_dict["Hm_phi"][2])
    f.write("\\def\\Hmphi{%.3f \\pm %.3f \\pm %.3f} \n" % (Hm_phi,Hm_phi_err,unbinned_fit_syst_dict["Hm_phi_tot"]))
    Hm_phi = unbinned_fit_results_dict["Hm_phi"][0]
    Hm_phi_err = 0.5*(-unbinned_fit_results_dict["Hm_phi"][1] + unbinned_fit_results_dict["Hm_phi"][2])
    f.write("\\def\\Hmphi{%.3f \\pm %.3f \\pm %.3f} \n" % (Hm_phi,Hm_phi_err,unbinned_fit_syst_dict["Hm_phi_tot"]))
    Hp_phi = unbinned_fit_results_dict["Hp_phi"][0]
    Hp_phi_err = 0.5*(-unbinned_fit_results_dict["Hp_phi"][1] + unbinned_fit_results_dict["Hp_phi"][2])
    f.write("\\def\\Hpphi{%.3f \\pm %.3f \\pm %.3f} \n" % (Hp_phi,Hp_phi_err,unbinned_fit_syst_dict["Hp_phi_tot"]))
    #H+ derived from fL and H-
    Hm_stat_u = ufloat(Hm_corr,Hm_err)
    Hm_syst_u = ufloat(Hm_corr,unbinned_fit_syst_dict["Hm_amp_tot"])
    Hp_stat_u = (1.0 - fL_stat_u - Hm_stat_u**2)**(0.5)
    Hp_syst_u = (1.0 - fL_syst_u - Hm_syst_u**2)**(0.5)
    f.write("\\def\\Hpamp{%.3f \\pm %.3f \\pm %.3f} \n" % (Hp_stat_u.n, Hp_stat_u.s, Hp_syst_u.s))

    #MC unbinned fit results
    with open(an.loc.ROOT+'/output/json/unbinned_angular_fit_MC_results.json','r') as file:
        unbinned_fit_results_dict = json.load(file)
    Hm_err = 0.5*(-unbinned_fit_results_dict["Hm_amp"][1] + unbinned_fit_results_dict["Hm_amp"][2])
    Hm_amp = unbinned_fit_results_dict["Hm_amp"][0]
    f.write("\\def\\HmampMC{%.3f \\pm %.3f} \n" % (Hm_amp,Hm_err))
    Hm_phi = unbinned_fit_results_dict["Hm_phi"][0]
    Hm_phi_err = 0.5*(-unbinned_fit_results_dict["Hm_phi"][1] + unbinned_fit_results_dict["Hm_phi"][2])
    f.write("\\def\\HmphiMC{%.3f \\pm %.3f} \n" % (Hm_phi,Hm_phi_err))
    Hp_phi = unbinned_fit_results_dict["Hp_phi"][0]
    Hp_phi_err = 0.5*(-unbinned_fit_results_dict["Hp_phi"][1] + unbinned_fit_results_dict["Hp_phi"][2])
    f.write("\\def\\HpphiMC{%.3f \\pm %.3f} \n" % (Hp_phi,Hp_phi_err))


if __name__ == '__main__':
    main()
