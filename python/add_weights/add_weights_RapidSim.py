from ROOT import TAxis, TH1, TH1F, TH3F, TH2F, TFile, TChain
from array import array
import sys,os, argparse
import analysis as an

def add_weights(geom):

    angle_type = "VV"

    in_file = f"{an.loc.WGEOS}/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_{geom}_Vars"

    out_file = in_file+"_Weights"

    tree = TChain("DecayTree")
    tree.Add(in_file+".root")

    #Set addresses of true angles, used to find which weight bin the event falls in
    costheta_D_true = array( 'd', [0] )
    tree.SetBranchAddress( "costheta_D_true", costheta_D_true )

    costheta_X_true = array( 'd', [0] )
    tree.SetBranchAddress( f"costheta_X_{angle_type}_true", costheta_X_true )

    chi_true = array( 'd', [0] )
    tree.SetBranchAddress( f"chi_{angle_type}_true", chi_true )

    new_file = TFile(out_file+".root","RECREATE")
    new_tree = tree.CloneTree(0)

    #New weight branches for each angular term
    coeffs = ["1","2","3","4","5","6"]

    w = {}

    #Weight histograms
    h_file = {}
    h = {}
    hist_x = {}
    hist_y = {}
    hist_z = {}
    bin_x = {}
    bin_y = {}
    bin_z = {}

    for c in coeffs:
    	h_file[c] = TFile.Open(f"{an.loc.ROOT}output/hists/weight_hists_Bd2DstDsst.root")
    	h[c] = h_file[c].Get(f"hist_ratio_{c}")
    	h[c].SetName(f"hist_ratio_{c}")
    	h[c].SetTitle(f"hist_ratio_{c}")
    	w[c] = array( 'd', [0] )
    	new_tree.Branch(f"w_{c}", w[c], f"w_{c}/D")

    #Loop over tree, anf fill weights based on which bin the events fall into
    for i in range(0,tree.GetEntries()):
    	tree.GetEntry(i)
    	if(i % 10000 == 0):
    		print(i)
    	#Loop over the coefficients
    	for c in coeffs:
    		hist_x[c] = h[c].GetXaxis()
    		bin_x[c] = hist_x[c].FindBin(costheta_D_true[0])

    		hist_y[c] = h[c].GetYaxis()
    		bin_y[c] = hist_y[c].FindBin(costheta_X_true[0])

    		hist_z[c] = h[c].GetZaxis()
    		bin_z[c] = hist_z[c].FindBin(chi_true[0])

    		#Assign weight to event as the content of the histogram bin
    		w[c][0] = h[c].GetBinContent(bin_x[c],bin_y[c],bin_z[c])


    	new_tree.Fill()

    new_file.cd()
    new_tree.AutoSave()
    new_file.Write()
    new_file.Close()


def main():
    parser = argparse.ArgumentParser(description='Add angular coefficient weights to RapidSim MC')
    parser.add_argument("--Geom", choices=["All","LHCb"],
                        required=True, help="Geometry")
    args = parser.parse_args()

    add_weights(args.Geom)

if __name__ == '__main__':
    main()
