import ROOT
import analysis as an
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHILLdini.cpp")
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHORNSdini.cpp")
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHILLdini_misID.cpp")
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHORNSdini_misID.cpp")
from ROOT import gSystem, gStyle, gInterpreter
from ROOT import RooCategory, RooAbsReal, RooFormulaVar, RooDataSet, RooDataHist, RooRealVar, RooGaussian, RooChebychev, RooExponential, RooAddPdf, RooArgList, RooArgSet, RooFit, RooRandom, gApplication, RooUnblindUniform, RooMCStudy
from ROOT import RooStats, RooAbsData, RooCBShape, RooSimultaneous, RooHILLdini, RooHILLdini_misID, RooHORNSdini, RooHORNSdini_misID
from ROOT import TFile, TChain, TNtuple, TCanvas, TH1F, TH2F, TH3F, TRandom3, TList

from root_pandas import read_root, to_root
import pandas as pd
import numpy as np
import json
import random
import sys, os, argparse
from listsfromhist import listsfromhist
from init_vals_m_DstDs_for_fL import init_vals
from num2tex import num2tex

import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

#Get part reco shape parameters from fits to RapidSim samples
def get_pars(mode,r_name,syst):

	pdf_pars = {}

	#Load MC fit result
	mc_fit = TFile.Open(an.loc.ROOT+"output/roofitresults/MC_%s.root" % mode)

	mc_result = mc_fit.Get(r_name)

	pars = mc_result.floatParsFinal()
	par_vals = []

	#Get default values
	for i in range(0,len(pars)):

		p = pars[i]
		name = p.GetName()

		pdf_pars[name] = RooRealVar("%s_%s" % (mode,name), "", p.getVal())

		par_vals.append(p.getVal())

	#cov matrix for MC fit
	cov_mat = mc_result.covarianceMatrix()
	cov_mat_np = np.ones((len(pars),len(pars)))

	#Numpy version
	for i in range(0,len(pars)):
		for j in range(0,len(pars)):
			cov_mat_np[i,j] = cov_mat[i][j]

	par_arr = np.array(par_vals)

	#If running systematics, make multivariate Gaussian using MC covariance matrix and set PDF shapes to the randomly sampled values
	if(syst=="PDFs"):
		rand_int = random.randint(1,1e6)
		np.random.seed(rand_int)
		rand_pars = np.random.multivariate_normal(par_arr, cov_mat_np, size=1)[0]

		for i in range(0,len(pars)):
			p = pars[i]
			name = p.GetName()
			pdf_pars[name].setVal(rand_pars[i])

	for p in pdf_pars:
		if(p=="a" or "b"==p or "sigma" in p):
			pdf_pars[p].setVal(1000.*pdf_pars[p].getVal())

	return pdf_pars


def PlotPull(D,low,high,bin_width):
	data_arr = listsfromhist(D, overunder = True)

	fig, ax = plt.subplots(figsize=(10,4))

	#Plot data
	plt.bar(data_arr[0],data_arr[1],color='gray',width=2.0)

	plt.ylabel("Residual ($\\sigma$)",fontsize=25)
	plt.xlabel("$m(D^{*-}D_s^{+})$ [MeV/$c^2$]",fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(low,high-1)
	plt.axhline(y=3.0,color='crimson',linestyle='--',alpha=0.3)
	plt.axhline(y=-3.0,color='crimson',linestyle='--',alpha=0.3)
	plt.ylim(-5.,5.)
	#plt.tight_layout()
	margins = {  #     vvv margin in inches
				"left"   : 0.12,
				"bottom" : 0.2,
				"right"  : 0.98,
				"top"    : 0.98
				}
	fig.subplots_adjust(**margins)
	plt.show()
	fig.savefig(an.loc.ROOT+"output/plots/m_DstDs_for_fL_pull.pdf")


def Plot(frame,low,high,bin_width,isLog):
	D = frame.getObject(0)
	data_arr = listsfromhist(D, overunder = True)

	fig, ax = plt.subplots(figsize=(10,6))

	#Plot data
	plt.errorbar(data_arr[0],data_arr[1],yerr=data_arr[2],ls=None,color='k',fmt='o',markersize=2,label="Data",alpha=0.4)

	plt.ylabel("Candidates / (%.2f MeV/$c^2$)" % bin_width,fontsize=25)
	plt.xlabel("$m(D^{*-}D_s^{+})$ [MeV/$c^2$]",fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(low,high-1)

	x_vals = np.linspace(low,high,1000)

	#Total
	P = frame.getObject(1)
	pdf_arr = []
	for v in x_vals:
		pdf_arr.append(P.interpolate(v))
	plt.plot(x_vals,pdf_arr,color="crimson",linewidth=1.5,label="Total fit")

	#Combinatorial
	P = frame.getObject(2)
	pdf_arr_comb = []
	for v in x_vals:
		pdf_arr_comb.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="royalblue")

	#pi0_L
	P = frame.getObject(3)
	pdf_arr_pi0_L = []
	for v in x_vals:
		pdf_arr_pi0_L.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="crimson")

	#pi0_T
	P = frame.getObject(4)
	pdf_arr_pi0_T = []
	for v in x_vals:
		pdf_arr_pi0_T.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="crimson")

	#gamma_L
	P = frame.getObject(5)
	pdf_arr_gamma_L = []
	for v in x_vals:
		pdf_arr_gamma_L.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="crimson")

	#gamma_T
	P = frame.getObject(6)
	pdf_arr_gamma_T = []
	for v in x_vals:
		pdf_arr_gamma_T.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="crimson")

	#B -> D* Ds
	P = frame.getObject(7)
	pdf_arr_dstds = []
	for v in x_vals:
		pdf_arr_dstds.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="crimson")

	#Feed down
	P = frame.getObject(8)
	pdf_arr_feed = []
	for v in x_vals:
		pdf_arr_feed.append(P.interpolate(v))
	#plt.plot(x_vals,pdf_arr,color="crimson")

	#Comb + Feed + Pi0_L + Pi0_T + Gamma_L + Gamma_T
	plt.fill_between(x_vals,[sum(x) for x in zip(pdf_arr_pi0_L, pdf_arr_pi0_T, pdf_arr_gamma_L, pdf_arr_gamma_T, pdf_arr_comb, pdf_arr_feed)],color="orange",label="$B^0 \\to D^{*-} (D_s^{*+} \\to D_s^+ \\pi^0), L$")
	#Comb + Feed + Pi0_L + Pi0_T + Gamma_L
	plt.fill_between(x_vals,[sum(x) for x in zip(pdf_arr_pi0_T, pdf_arr_gamma_L, pdf_arr_gamma_T, pdf_arr_comb, pdf_arr_feed)],color="green",label="$B^0 \\to D^{*-} (D_s^{*+} \\to D_s^+ \\pi^0), T$")
	#Comb + Feed + Pi0_L + Pi0_T
	plt.fill_between(x_vals,[sum(x) for x in zip(pdf_arr_gamma_L, pdf_arr_gamma_T, pdf_arr_comb, pdf_arr_feed)],color="hotpink",label="$B^0 \\to D^{*-} (D_s^{*+} \\to D_s^+ \\gamma), L$")
	#Comb + Feed + Pi0_L
	plt.fill_between(x_vals,[sum(x) for x in zip(pdf_arr_gamma_T, pdf_arr_comb, pdf_arr_feed)],color="purple",label="$B^0 \\to D^{*-} (D_s^{*+} \\to D_s^+ \\gamma), T$")
	#Comb + Feed
	plt.fill_between(x_vals,[sum(x) for x in zip(pdf_arr_comb, pdf_arr_feed)],color="dodgerblue",label="$B \\to D^{*(*)} D_s^{(*\\{*\\})+}$")
	#Comb
	plt.fill_between(x_vals,pdf_arr_comb,color="gray",label="Combinatorial")
	#D* Ds
	plt.plot(x_vals,pdf_arr_dstds,color='navy',label="$B^0 \\to D^{*-} D_s^+$")

	ymin, ymax = ax.get_ylim()
	if(isLog=="N"):
		plt.ylim(0.1,ymax*1.2)
		plt.legend(fontsize=15,ncol=1,loc='upper left')
	else:
		plt.yscale("log")
		plt.ylim(0.1,ymax*3)

	#plt.tight_layout()
	margins = {  #     vvv margin in inches
				"left"   : 0.12,
				"bottom" : 0.18,
				"right"  : 0.98,
				"top"    : 0.98
				}
	fig.subplots_adjust(**margins)
	plt.show()
	fig.savefig(an.loc.ROOT+"output/plots/m_DstDs_for_fL_fit_log_%s.pdf" % isLog)


def run_fit(syst, toy):

	r3 = TRandom3(0)

	b_low = 4900.
	b_high = 5350.
	B_M = RooRealVar("B_D0constDsconstPVconst_B_M","",b_low,b_high)
	bin_width = 2.0
	num_bins = int((b_high-b_low)/bin_width)
	B_M.setBins(num_bins)

	args = RooArgSet(B_M)

	#Check if RooDataSet exists and load it - if not, make it
	data_file = an.loc.ROOT+"output/rootfiles/m_DstDs_for_fL_roodataset.root"
	if(os.path.isfile(data_file)):
		print("Loading RooDataSet")
		file = TFile.Open(data_file)
		data = file.Get("data")

	else:
		print("Making RooDataSet from trees")
		#Load data
		years = ["2015","2016","2017","2018"]
		mags = ["Up","Down"]

		path = an.loc.WGEOS+"/tuples/data/Bd2DstDs"

		tree = TChain("DecayTree")

		angle_type = "VV"

		vars = ["B_D0constDsconstPVconst_B_M"]

		for y in years:
			for m in mags:
				tree.Add("%s/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))

		tree.SetBranchStatus("*",0)

		for v in vars:
			tree.SetBranchStatus(v,1)

		data = RooDataSet("data","data",args,RooFit.Import(tree))
		data.Print()

		file = TFile(data_file,"RECREATE")
		file.cd()
		data.Write()
		file.Write()
		file.Close()

		file = TFile.Open(data_file)
		data = file.Get("data")

	data_h = data.binnedClone()
	#Models

	#Combinatorial
	lam = RooRealVar("lam","Exponential $\\lambda$",-0.003,-1e-2,-1e-10)
	pdf_comb = RooExponential("pdf_comb","",B_M,lam)

	#Pi0 background
	pdf_pars_pi0_T = get_pars("Bd2DstDsst_Dsst2DsPi0_HELAMP100", "fitresult_sig_pdf_data", syst)

	pi0_csi = RooRealVar("pi0_csi","",1.0)
	pi0_shift = RooRealVar("pi0_shift","",0.0)

	pdf_pi0_T = RooHILLdini("pdf_pi0_T","",B_M,pdf_pars_pi0_T["a"],pdf_pars_pi0_T["b"],pi0_csi,pi0_shift,pdf_pars_pi0_T["sigma"],pdf_pars_pi0_T["r"],pdf_pars_pi0_T["f"])

	#010 shape (longitudinal)
	pdf_pars_pi0_L = get_pars("Bd2DstDsst_Dsst2DsPi0_HELAMP010", "fitresult_sig_pdf_data", syst)

	pdf_pi0_L = RooHORNSdini("pdf_pi0_L","",B_M,pdf_pars_pi0_L["a"],pdf_pars_pi0_L["b"],pdf_pars_pi0_L["csi"],pi0_shift,pdf_pars_pi0_L["sigma"],pdf_pars_pi0_L["r"],pdf_pars_pi0_L["f"])

	#Longitudinal polarisation fraction (share between gamma and pi0 as both as D* Ds* decay)
	f_L = RooRealVar("f_L","$f_L$",init_vals["fL"],0.,1.)

	pdf_pi0 = RooAddPdf("pdf_pi0","",RooArgList(pdf_pi0_L,pdf_pi0_T),RooArgList(f_L))

	#gamma signal

	#100 and 001 (transverse)
	pdf_pars_gamma_T = get_pars("Bd2DstDsst_HELAMP100", "fitresult_sig_pdf_data", syst)

	gamma_csi = RooRealVar("gamma_csi","Relative peak height for $B^0 \\to D^{*-}D_s^{*+}$",1.,0.,10.)
	gamma_shift = RooRealVar("gamma_shift","Uniform PDF shift for $B^0 \\to D^{*-}D_s^{*+}$",0.0,-10.,10.)

	pdf_horns_gamma_T = RooHORNSdini("pdf_horns_gamma_T","",B_M,pdf_pars_gamma_T["a"],pdf_pars_gamma_T["b"],gamma_csi,gamma_shift,pdf_pars_gamma_T["sigma_horns"],pdf_pars_gamma_T["r_horns"],pdf_pars_gamma_T["f_horns"])
	pdf_hill_gamma_T = RooHILLdini("pdf_hill_gamma_T","",B_M,pdf_pars_gamma_T["a"],pdf_pars_gamma_T["b"],gamma_csi,gamma_shift,pdf_pars_gamma_T["sigma_hill"],pdf_pars_gamma_T["r_hill"],pdf_pars_gamma_T["f_hill"])
	pdf_gamma_T = RooAddPdf("pdf_gamma_T","",RooArgList(pdf_horns_gamma_T,pdf_hill_gamma_T),RooArgList(pdf_pars_gamma_T["frac_horns"]))

	#010 shape (longitudinal)
	pdf_pars_gamma_L = get_pars("Bd2DstDsst_HELAMP010", "fitresult_sig_pdf_data", syst)

	pdf_gamma_L = RooHILLdini("pdf_gamma_L","",B_M,pdf_pars_gamma_L["a"],pdf_pars_gamma_L["b"],gamma_csi,gamma_shift,pdf_pars_gamma_L["sigma"],pdf_pars_gamma_L["r"],pdf_pars_gamma_L["f"])

	#Total gamma (L + T)
	pdf_gamma = RooAddPdf("pdf_gamma","",RooArgList(pdf_gamma_L,pdf_gamma_T),RooArgList(f_L))

	#gamma signal
	n_sig = RooRealVar("n_sig","$B^0 \\to D^{*-} (D_s^{*+} \\to D_s^+ \\gamma)$ yield",50000,0,100000)

	#Comb
	n_comb = RooRealVar("n_comb","Combinatorial yield",40000,0,100000)

	#Ds* BFs (assume they sum to 1, so pi0 BF is 1 - others)
	rand = 0.0
	if(syst=="BFs"):
		rand = r3.Gaus(0.,1.6e-3)
	BF_Dsst2Dsee = RooRealVar("BF_Dsst2Dsee","",6.7e-3+rand)
	rand = 0.0
	if(syst=="BFs"):
		rand = r3.Gaus(0.,0.007)
	BF_Dsst2Dsgamma = RooRealVar("BF_Dsst2Dsgamma","",0.935+rand)

	BF_Dsst2Dspi0 = RooFormulaVar("BF_Dsst2Dspi0","1-@0-@1",RooArgList(BF_Dsst2Dsgamma,BF_Dsst2Dsee))

	n_pi0 = RooFormulaVar("n_pi0","@0*(@1/@2)",RooArgList(n_sig,BF_Dsst2Dspi0,BF_Dsst2Dsgamma))

	#B -> D* Ds
	'''
	mu = RooRealVar("mu","$\\mu$ for $B^0 \\to D^{*-}D_s^+$",5279,5200,5400)
	sigma_L = RooRealVar("sigma_L","$\\sigma_L$ for $B^0 \\to D^{*-}D_s^+$",10,0,100)
	sigma_R = RooRealVar("sigma_R","$\\sigma_R$ for $B^0 \\to D^{*-}D_s^+$",8,0,100)
	alpha_L = RooRealVar("alpha_L","$\\alpha_L$ for $B^0 \\to D^{*-} D_s^+$",2.0,0.0,5.0)
	alpha_R = RooRealVar("alpha_R","$\\alpha_R$ for $B^0 \\to D^{*-} D_s^+$",-2.0,-5.0,-0.00001)
	n_L = RooRealVar("n_L","$n_L$ for $B^0 \\to D^{*-} D_s^+$",1.0,0,100)
	n_R = RooRealVar("n_R","$n_R$ for $B^0 \\to D^{*-} D_s^+$",1.0)#,0,100)
	dstds_frac = RooRealVar("dstds_frac","Crystal Ball relative fraction for $B^0 \\to D^{*-} D_s^+$",0.5,0,1)

	pdf_dstds_L = RooCBShape("pdf_dstds_L","",B_M,mu,sigma_L,alpha_L,n_L)
	pdf_dstds_R = RooCBShape("pdf_dstds_R","",B_M,mu,sigma_R,alpha_R,n_R)

	pdf_dstds = RooAddPdf("pdf_dstds","",RooArgList(pdf_dstds_L,pdf_dstds_R),RooArgList(dstds_frac))
	'''

	#B mass signal PDF with tails from MC fit
	with open(an.loc.ROOT+"output/json/MC_Bd2DstDs.json","r") as f:
		shape_pars = json.load(f)

	mu = RooRealVar("mu","$B^0 \\to D^{*-}D_s^+$ $\\mu$",shape_pars["mu"][0],5200,5400)
	sigma = RooRealVar("sigma","$B^0 \\to D^{*-}D_s^+$ $\\sigma$",shape_pars["sigma"][0],0,100)

	alpha_R = RooRealVar("alpha_R","$B^0 \\to D^{*-}D_s^+$ $\\alpha_R$ (MC constrained)",shape_pars["alpha_R"][0],-10.0,-1e-10)
	alpha_R_const = RooGaussian("alpha_R_const","", alpha_R, RooFit.RooConst(shape_pars["alpha_R"][0]), RooFit.RooConst(shape_pars["alpha_R"][1]))

	n_R = RooRealVar("n_R","$B^0 \\to D^{*-}D_s^+$ $n_R$ (MC constrained)",shape_pars["n_R"][0],0,1000)
	n_R_const = RooGaussian("n_R_const","", n_R, RooFit.RooConst(shape_pars["n_R"][0]), RooFit.RooConst(shape_pars["n_R"][1]))

	alpha_L = RooRealVar("alpha_L","$B^0 \\to D^{*-}D_s^+$ $\\alpha_L$ (MC constrained)",shape_pars["alpha_L"][0],0.0,10.0)
	alpha_L_const = RooGaussian("alpha_L_const","", alpha_L, RooFit.RooConst(shape_pars["alpha_L"][0]), RooFit.RooConst(shape_pars["alpha_L"][1]))

	n_L = RooRealVar("n_L","$B^0 \\to D^{*-}D_s^+$ $n_L$ (MC constrained)",shape_pars["n_L"][0],0,1000)
	n_L_const = RooGaussian("n_L_const","", n_L, RooFit.RooConst(shape_pars["n_L"][0]), RooFit.RooConst(shape_pars["n_L"][1]))

	dstds_frac = RooRealVar("dstds_frac","$B^0 \\to D^{*-}D_s^+$ CB fraction (MC constrained)",shape_pars["frac"][0],0.,1.)
	dstds_frac_const = RooGaussian("dstds_frac_const","", dstds_frac, RooFit.RooConst(shape_pars["frac"][0]), RooFit.RooConst(shape_pars["frac"][1]))

	pdf_dstds_L = RooCBShape("pdf_dstds_L","",B_M,mu,sigma,alpha_L,n_L)
	pdf_dstds_R = RooCBShape("pdf_dstds_R","",B_M,mu,sigma,alpha_R,n_R)
	pdf_dstds = RooAddPdf("pdf_dstds","",RooArgList(pdf_dstds_L,pdf_dstds_R),RooArgList(dstds_frac))

	n_dstds = RooRealVar("n_dstds","$B^0 \\to D^{*-} D_s^+$ yield",40000,0,100000)

	#Load feed-down fractions
	with open(an.loc.ROOT+'/output/json/feed_down.json','r') as f:
		feed_down_dict = json.load(f)

	#B -> D1(2420) Ds
	pdf_pars_d1ds = get_pars("Bd2D2420Ds", "fitresult_sig_pdf_data", syst)

	d1ds_shift = RooRealVar("d1ds_shift","",1.0)

	pdf_d1ds = RooHORNSdini("pdf_d1ds","",B_M,pdf_pars_d1ds["a"],pdf_pars_d1ds["b"],pdf_pars_d1ds["csi"],d1ds_shift,pdf_pars_d1ds["sigma"],pdf_pars_d1ds["r"],pdf_pars_d1ds["f"])

	#Rate relative to B0 -> D* Ds, accounting for mass cut efficiency, BFs (20% error) and relative efficiency (10% error)
	frac_d1ds_val = [feed_down_dict["Bd2D2420Ds"][0],feed_down_dict["Bd2D2420Ds"][1]]
	frac_d1ds = RooRealVar("frac_d1ds","Fraction of $B^0 \\to D_1(2420)^- D_s^+$",frac_d1ds_val[0],0,1)
	frac_d1ds_const = RooGaussian("frac_d1ds_const","",frac_d1ds,RooFit.RooConst(frac_d1ds_val[0]),RooFit.RooConst(frac_d1ds_val[1]))
	n_d1ds = RooFormulaVar("n_d1ds","@0*@1",RooArgList(frac_d1ds,n_dstds))

	#B -> D* (Ds(2460) -> Ds gamma)
	pdf_pars_dstds2460_dsgamma = get_pars("Bd2DstDs2460_Dsgamma", "fitresult_sig_pdf_data", syst)

	dstds2460_dsgamma_shift = RooRealVar("dstds2460_dsgamma_shift","",1.0)

	pdf_dstds2460_dsgamma = RooHORNSdini("pdf_dstds2460_dsgamma","",B_M,pdf_pars_dstds2460_dsgamma["a"],pdf_pars_dstds2460_dsgamma["b"],pdf_pars_dstds2460_dsgamma["csi"],dstds2460_dsgamma_shift,pdf_pars_dstds2460_dsgamma["sigma"],pdf_pars_dstds2460_dsgamma["r"],pdf_pars_dstds2460_dsgamma["f"])

	#Rate relative to B0 -> D* Ds, accounting for mass cut efficiency, BFs (20% error) and relative efficiency (10% error)
	frac_dstds2460_dsgamma_val = [feed_down_dict["Bd2DstDs2460_Dsgamma"][0],feed_down_dict["Bd2DstDs2460_Dsgamma"][1]]
	frac_dstds2460_dsgamma = RooRealVar("frac_dstds2460_dsgamma","Fraction of $B^0 \\to D^{*-} (D_{s1}(2460)^+ \\to D_s^+ \\gamma$",frac_dstds2460_dsgamma_val[0],0,1)
	frac_dstds2460_dsgamma_const = RooGaussian("frac_dstds2460_dsgamma_const","",frac_dstds2460_dsgamma,RooFit.RooConst(frac_dstds2460_dsgamma_val[0]),RooFit.RooConst(frac_dstds2460_dsgamma_val[1]))
	n_dstds2460_dsgamma = RooFormulaVar("n_dstds2460_dsgamma","@0*@1",RooArgList(frac_dstds2460_dsgamma,n_dstds))

	#B -> D* (Ds(2460) -> Ds* pi0)
	pdf_pars_dstds2460_dsstpi0 = get_pars("Bd2DstDs2460_Dsstpi0", "fitresult_sig_pdf_data", syst)

	dstds2460_dsstpi0_r = RooRealVar("dstds2460_dsstpi0_r","",1.0)
	dstds2460_dsstpi0_f = RooRealVar("f_dstds2460_dsstpi0_f","",1.0)
	dstds2460_dsstpi0_shift = RooRealVar("shift_dstds2460_dsstpi0_shift","",0)

	pdf_dstds2460_dsstpi0 = RooHILLdini("pdf_dstds2460_dsstpi0","",B_M,pdf_pars_dstds2460_dsstpi0["a"],pdf_pars_dstds2460_dsstpi0["b"],pdf_pars_dstds2460_dsstpi0["csi"],dstds2460_dsstpi0_shift,pdf_pars_dstds2460_dsstpi0["sigma"],dstds2460_dsstpi0_r,dstds2460_dsstpi0_f)

	#Rate relative to B0 -> D* Ds, accounting for mass cut efficiency, BFs (20% error) and relative efficiency (10% error)
	frac_dstds2460_dsstpi0_val = [feed_down_dict["Bd2DstDs2460_Dsstpi0"][0],feed_down_dict["Bd2DstDs2460_Dsstpi0"][1]]
	frac_dstds2460_dsstpi0 = RooRealVar("frac_dstds2460_dsstpi0","Fraction of $B^0 \\to D^{*-} (D_{s1}(2460)^+ \\to D_s^{*+} \\pi^0$",frac_dstds2460_dsstpi0_val[0],-0.05,1)
	frac_dstds2460_dsstpi0_const = RooGaussian("frac_dstds2460_dsstpi0_const","",frac_dstds2460_dsstpi0,RooFit.RooConst(frac_dstds2460_dsstpi0_val[0]),RooFit.RooConst(frac_dstds2460_dsstpi0_val[1]))
	n_dstds2460_dsstpi0 = RooFormulaVar("n_dstds2460_dsstpi0","@0*@1",RooArgList(frac_dstds2460_dsstpi0,n_dstds))

	#B -> D1(2420) Ds*
	pdf_pars_d1dsst = get_pars("Bd2D2420Dsst", "fitresult_sig_pdf_data", syst)

	d1dsst_r = RooRealVar("d1dsst_r","",1.0)
	d1dsst_f = RooRealVar("d1dsst_f","",1.0)
	d1dsst_shift = RooRealVar("d1dsst_shift","",0)

	pdf_d1dsst = RooHILLdini("pdf_d1dsst","",B_M,pdf_pars_d1dsst["a"],pdf_pars_d1dsst["b"],pdf_pars_d1dsst["csi"],d1dsst_shift,pdf_pars_d1dsst["sigma"],d1dsst_r,d1dsst_f)

	#Rate relative to B0 -> D* Ds, accounting for mass cut efficiency, BFs (20% error) and relative efficiency (10% error)
	frac_d1dsst_val = [feed_down_dict["Bd2D2420Dsst"][0],feed_down_dict["Bd2D2420Dsst"][1]]
	frac_d1dsst = RooRealVar("frac_d1dsst","Fraction of $B^0 \\to D_1(2420)^- D_s^{*+}$",frac_d1dsst_val[0],0,1)
	frac_d1dsst_const = RooGaussian("frac_d1dsst_const","",frac_d1dsst,RooFit.RooConst(frac_d1dsst_val[0]),RooFit.RooConst(frac_d1dsst_val[1]))
	n_d1dsst = RooFormulaVar("n_d1dsst","@0*@1",RooArgList(frac_d1dsst,n_dstds))

	#Total PDF
	tot_pdf = RooAddPdf("tot_pdf","",RooArgList(pdf_gamma,pdf_pi0,pdf_comb,pdf_dstds,pdf_d1ds,pdf_dstds2460_dsgamma,pdf_dstds2460_dsstpi0,pdf_d1dsst),RooArgList(n_sig,n_pi0,n_comb,n_dstds,n_d1ds,n_dstds2460_dsgamma,n_dstds2460_dsstpi0,n_d1dsst))

	constraints = RooArgSet()
	#Gaussian constraints on the feed down contributions
	constraints.add(frac_d1ds_const)
	constraints.add(frac_dstds2460_dsgamma_const)
	constraints.add(frac_dstds2460_dsstpi0_const)
	constraints.add(frac_d1dsst_const)
	#Constraints on the B0 -> D* Ds tails
	constraints.add(alpha_R_const)
	constraints.add(n_R_const)
	constraints.add(alpha_L_const)
	constraints.add(n_L_const)
	constraints.add(dstds_frac_const)

	if(toy=="N"):
		result = tot_pdf.fitTo(data_h,RooFit.Save(),RooFit.NumCPU(8,0),RooFit.Extended(True),RooFit.Optimize(False),RooFit.Offset(True),RooFit.Minimizer("Minuit2","migrad"),RooFit.Strategy(1),RooFit.ExternalConstraints(constraints))
		result.Print("V")
		#Save to file
		if(syst=="none"):
			#Write list of pars to latex table
			params = result.floatParsFinal()
			tex_file = open(an.loc.ROOT+"output/tex/m_DstDs_for_fL.tex","w")
			tex_file.write("\\begin{table}[!h] \n")
			tex_file.write("\\begin{center} \n")
			tex_file.write("\\begin{tabular}{l|c} \n")
			tex_file.write("Parameter & Fit result \\\\ \\hline \n")
			for p in params:
				title = p.GetTitle()
				val = p.getVal()
				err = p.getError()
				tex_file.write("%s & " % title)
				if(val<0.1):
					tex_file.write("${:.2e} \\pm {:.2e}$ \\\\ \n".format(num2tex(val),num2tex(err)))
				else:
					tex_file.write("$%.2f \\pm %.2f$ \\\\ \n" % (val,err))
			tex_file.write("\\end{tabular} \n")
			tex_file.write("\\caption{ Results of the $m(D^{*-}D_s^{+})$ fit. \label{tab:m_DstDs_for_fL_results}} \n")
			tex_file.write("\\end{center} \n")
			tex_file.write("\\end{table} \n")


			result_file = TFile.Open(an.loc.ROOT+"output/roofitresults/m_DstDs_for_fL.root","RECREATE")

			#json file containing results for use elsewhere in analysis
			results_dict = {}
			results_dict["fL"] = [f_L.getVal(),f_L.getError()]
			results_dict["n_Bd2DstDsst_Dsst2Dsgamma"] = [n_sig.getVal(),n_sig.getError()]
			results_dict["n_Bd2DstDs"] = [n_dstds.getVal(),n_dstds.getError()]

			with open(an.loc.ROOT+'output/json/m_DstDs_for_fL.json', 'w') as f:
				json.dump(results_dict, f, sort_keys=True, indent=4)
		else:
			rand_str = random.randint(1, 1e10)
			result_file = TFile.Open(an.loc.WGEOS+"/Systematics/m_DstDs_for_fL/%s_%s.root" % (syst,rand_str),"RECREATE")
		result_file.cd()
		result.Write()
		result_file.Close()
	else:
		rand_str = random.randint(1, 1e10)
		RooRandom.randomGenerator().SetSeed(rand_str)
		n_gen = tot_pdf.expectedEvents(RooArgSet(B_M))
		toy_data = tot_pdf.generate(RooArgSet(B_M),n_gen)
		toy_data_h = toy_data.binnedClone()
		toy_result = tot_pdf.fitTo(toy_data_h,RooFit.Save(),RooFit.NumCPU(8,0),RooFit.Extended(True),RooFit.Optimize(False),RooFit.Offset(True),RooFit.Minimizer("Minuit2","migrad"),RooFit.Strategy(1),RooFit.ExternalConstraints(constraints))
		toy_result.Print("V")
		toy_result_file = TFile(an.loc.WGEOS+"/Toys/m_DstDs_for_fL/%s.root" % rand_str,"RECREATE")
		toy_result_file.cd()
		toy_result.Write()
		toy_result_file.Close()

	#Plot if running default fit
	if(syst=="none" and toy=="N"):
		m = result.correlationMatrix()
		m.Print()

		frame = B_M.frame()
		data_h.plotOn(frame)
		tot_pdf.plotOn(frame)
		pull_hist = frame.pullHist()
		tot_pdf.plotOn(frame,RooFit.Components("pdf_comb"),RooFit.LineColor(2))
		tot_pdf.plotOn(frame,RooFit.Components("pdf_pi0_L"),RooFit.LineColor(2))
		tot_pdf.plotOn(frame,RooFit.Components("pdf_pi0_T"),RooFit.LineColor(2))
		tot_pdf.plotOn(frame,RooFit.Components("pdf_gamma_L"),RooFit.LineColor(2))
		tot_pdf.plotOn(frame,RooFit.Components("pdf_gamma_T"),RooFit.LineColor(2))
		tot_pdf.plotOn(frame,RooFit.Components("pdf_dstds"),RooFit.LineColor(2))
		tot_pdf.plotOn(frame,RooFit.Components("pdf_d1ds,pdf_dstds2460_dsgamma,pdf_dstds2460_dsstpi0,pdf_d1dsst"),RooFit.LineColor(2))

		Plot(frame,b_low,b_high,bin_width,"Y") #last argument is isLog
		Plot(frame,b_low,b_high,bin_width,"N") #last argument is isLog

		#Residuals
		PlotPull(pull_hist,b_low,b_high,bin_width)

def main():
    parser = argparse.ArgumentParser(description='Fit to m(D* Ds) distribution to measure fL')
    parser.add_argument("--Syst", choices=["PDFs","BFs","none"],
                    required=True, help="Systematic category")
    parser.add_argument("--Toy", choices=["Y","N"],
                    required=True, help="Whether to run a toy fit or not")
    args = parser.parse_args()

    run_fit(args.Syst, args.Toy)

if __name__ == '__main__':
    main()
