import tensorflow as tf
import zfit
import numpy as np
from root_pandas import read_root, to_root
import json
import matplotlib.pyplot as plt
import analysis as an
import argparse
import random
from zfit.minimize import DefaultToyStrategy
import progressbar
from init_vals_unbinned_angular_fit import init_vals

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

#3D decay rate PDF with acceptance corrections
class CustomPDF(zfit.pdf.ZPDF):
    _PARAMS = ['H0_amp',
    		   'Hm_amp',
    		   'Hp_amp',
    		   'H0_phi',
    		   'Hm_phi',
    		   'Hp_phi',
    		   'costheta_D_p0',
			   'costheta_D_p1',
			   'costheta_D_p2',
			   'costheta_D_p3',
			   'costheta_D_p4',
			   'costheta_D_p5',
			   'costheta_D_p6',
			   'costheta_X_p0',
			   'costheta_X_p1',
			   'costheta_X_p2',
			   'costheta_X_p3',
			   'costheta_X_p4',
			   'costheta_X_p5',
			   'costheta_X_p6',
			   'chi_p0',
			   'chi_p1',
			   'chi_p2',
			   ]
    _N_OBS = 3

    def _unnormalized_pdf(self, x):
        costheta_D, costheta_X, chi = x.unstack_x()

        H0_amp = self.params['H0_amp']
        H0_phi = self.params['H0_phi']
        Hm_amp = self.params['Hm_amp']
        Hp_amp = self.params['Hp_amp']
        Hm_phi = self.params['Hm_phi']
        Hp_phi = self.params['Hp_phi']
        costheta_D_p0 = self.params['costheta_D_p0']
        costheta_D_p1 = self.params['costheta_D_p1']
        costheta_D_p2 = self.params['costheta_D_p2']
        costheta_D_p3 = self.params['costheta_D_p3']
        costheta_D_p4 = self.params['costheta_D_p4']
        costheta_D_p5 = self.params['costheta_D_p5']
        costheta_D_p6 = self.params['costheta_D_p6']
        costheta_X_p0 = self.params['costheta_X_p0']
        costheta_X_p1 = self.params['costheta_X_p1']
        costheta_X_p2 = self.params['costheta_X_p2']
        costheta_X_p3 = self.params['costheta_X_p3']
        costheta_X_p4 = self.params['costheta_X_p4']
        costheta_X_p5 = self.params['costheta_X_p5']
        costheta_X_p6 = self.params['costheta_X_p6']
        chi_p0 = self.params['chi_p0']
        chi_p1 = self.params['chi_p1']
        chi_p2 = self.params['chi_p2']

        sintheta_D = tf.math.sqrt( 1.0 - costheta_D * costheta_D )
        sintheta_X = tf.math.sqrt( 1.0 - costheta_X * costheta_X )
        sintheta_D2 = (2.0 * sintheta_D * costheta_D)
        sintheta_X2 = (2.0 * sintheta_X * costheta_X)
        coschi = tf.math.cos(chi)
        sinchi = tf.math.sin(chi)
        cos2chi = 2*coschi*coschi-1
        sin2chi = 2*sinchi*coschi

        h_0 = tf.complex(H0_amp*tf.cos(H0_phi),H0_amp*tf.sin(H0_phi))
        h_p = tf.complex(Hp_amp*tf.cos(Hp_phi),Hp_amp*tf.sin(Hp_phi))
        h_m = tf.complex(Hm_amp*tf.cos(Hm_phi),Hm_amp*tf.sin(Hm_phi))

        h_0st = tf.math.conj(h_0)
        h_pst = tf.math.conj(h_p)
        h_mst = tf.math.conj(h_m)

        HpHmst = h_p*h_mst
        HpH0st = h_p*h_0st
        HmH0st = h_m*h_0st

        #True decay rate
        pdf = H0_amp**2 * costheta_D**2 * sintheta_X**2
        pdf += 0.25 * (Hp_amp**2 + Hm_amp**2) * sintheta_D**2 * (1.0 + costheta_X**2)
        pdf += -0.5 * tf.math.real(HpHmst) * sintheta_D**2 * sintheta_X**2 * cos2chi
        pdf += 0.5 * tf.math.imag(HpHmst) * sintheta_D**2 * sintheta_X**2 * sin2chi
        pdf += -0.25 * tf.math.real(HpH0st + HmH0st) * sintheta_D2 * sintheta_X2 * coschi
        pdf += 0.25 * tf.math.imag(HpH0st - HmH0st) * sintheta_D2 * sintheta_X2 * sinchi
        pdf = pdf * 9./8.

        #Normalise to 1
        #pdf = pdf * (1./tf.reduce_sum(pdf))

        #Acceptance corrections
        acc_pdf_costheta_D = costheta_D_p0 + costheta_D_p1 * costheta_D + costheta_D_p2 * costheta_D**2 + costheta_D_p3 * costheta_D**3 + costheta_D_p4 * costheta_D**4 + costheta_D_p5 * costheta_D**5 + costheta_D_p6 * costheta_D**6
        acc_pdf_costheta_X = costheta_X_p0 + costheta_X_p1 * costheta_X + costheta_X_p2 * costheta_X**2 + costheta_X_p3 * costheta_X**3 + costheta_X_p4 * costheta_X**4 + costheta_X_p5 * costheta_X**5 + costheta_X_p6 * costheta_X**6
        acc_pdf_chi = chi_p0 + chi_p1 * chi + chi_p2 * chi**2 #chi_p0 + chi_p1 * chi + chi_p2 * chi**2 + chi_p3 * chi**3 + chi_p4 * chi**4
        acc_pdf_chi = tf.abs(acc_pdf_chi)

        acc_pdf = tf.multiply(acc_pdf_costheta_D,acc_pdf_costheta_X)
        acc_pdf = tf.multiply(acc_pdf,acc_pdf_chi)

        pdf = tf.multiply(pdf,acc_pdf)

        return pdf

def main():

    #Angular phase space
    xobs = zfit.Space('costheta_D_reco', (-1, 1))
    yobs = zfit.Space('costheta_X_VV_reco', (-0.9, 1))
    zobs = zfit.Space('chi_VV_reco', (-np.pi, np.pi))
    obs = xobs * yobs * zobs

    obs_dict = {"costheta_D_reco": [0,"$\\cos(\\theta_D)$",-1,1,xobs],
                "costheta_X_VV_reco": [1,"$\\cos(\\theta_X)$",-0.9,1,yobs],
                "chi_VV_reco": [2,"$\\chi$ [rad]",-np.pi,np.pi,zobs]
                }

    #Helicity amplitude parameters - H0 fixed to value generated in full sim MC
    #All other pars initialised to values used in MC generation
    H0_amp = zfit.Parameter("H0_amp", 0.7204, floating=False)
    Hm_amp = zfit.Parameter("Hm_amp", 0.4904, 0., 1.)
    def Hp_amp_func(H0_amp, Hm_amp):
	       return tf.sqrt(1. - H0_amp**2 - Hm_amp**2)
    Hp_amp = zfit.ComposedParameter("Hp_amp", Hp_amp_func, dependents=[H0_amp, Hm_amp])
    H0_phi =  zfit.Parameter("H0_phi", 0., floating=False)
    Hp_phi =  zfit.Parameter("Hp_phi", 0., -2*np.pi, 2*np.pi)
    Hm_phi =  zfit.Parameter("Hm_phi", 0.,-2*np.pi, 2*np.pi)

    #Acceptance corrections

    #Full sim MC based costheta_D and costheta_X corrections
    with open(an.loc.ROOT+'/output/json/MC_acc_pars_costheta_X_D.json','r') as f:
        acc_dict = json.load(f)

    costheta_D_p0 = zfit.Parameter("costheta_D_p0" , acc_dict["costheta_D_a0"][0], floating=False)
    costheta_D_p1 = zfit.Parameter("costheta_D_p1" , acc_dict["costheta_D_a1"][0], floating=False)
    costheta_D_p2 = zfit.Parameter("costheta_D_p2" , acc_dict["costheta_D_a2"][0], floating=False)
    costheta_D_p3 = zfit.Parameter("costheta_D_p3" , acc_dict["costheta_D_a3"][0], floating=False)
    costheta_D_p4 = zfit.Parameter("costheta_D_p4" , acc_dict["costheta_D_a4"][0], floating=False)
    costheta_D_p5 = zfit.Parameter("costheta_D_p5" , acc_dict["costheta_D_a5"][0], floating=False)
    costheta_D_p6 = zfit.Parameter("costheta_D_p6" , acc_dict["costheta_D_a6"][0], floating=False)

    costheta_X_p0 = zfit.Parameter("costheta_X_p0" , acc_dict["costheta_X_VV_a0"][0], floating=False)
    costheta_X_p1 = zfit.Parameter("costheta_X_p1" , acc_dict["costheta_X_VV_a1"][0], floating=False)
    costheta_X_p2 = zfit.Parameter("costheta_X_p2" , acc_dict["costheta_X_VV_a2"][0], floating=False)
    costheta_X_p3 = zfit.Parameter("costheta_X_p3" , acc_dict["costheta_X_VV_a3"][0], floating=False)
    costheta_X_p4 = zfit.Parameter("costheta_X_p4" , acc_dict["costheta_X_VV_a4"][0], floating=False)
    costheta_X_p5 = zfit.Parameter("costheta_X_p5" , acc_dict["costheta_X_VV_a5"][0], floating=False)
    costheta_X_p6 = zfit.Parameter("costheta_X_p6" , acc_dict["costheta_X_VV_a6"][0], floating=False)

    with open(an.loc.ROOT+'/output/json/acc_pars_chi.json','r') as f:
        acc_dict_chi = json.load(f)

    chi_p0 = zfit.Parameter("chi_p0" , acc_dict_chi["chi_VV_a0"][0], floating=False)
    chi_p1 = zfit.Parameter("chi_p1" , acc_dict_chi["chi_VV_a1"][0], floating=False)
    chi_p2 = zfit.Parameter("chi_p2" , acc_dict_chi["chi_VV_a2"][0], floating=False)


    custom_pdf = CustomPDF(obs=obs,
                           H0_amp=H0_amp,
                           Hm_amp=Hm_amp,
                           Hp_amp=Hp_amp,
                           H0_phi=H0_phi,
                           Hm_phi=Hm_phi,
                           Hp_phi=Hp_phi,
                           costheta_D_p0 = costheta_D_p0,
                           costheta_D_p1 = costheta_D_p1,
                           costheta_D_p2 = costheta_D_p2,
                           costheta_D_p3 = costheta_D_p3,
                           costheta_D_p4 = costheta_D_p4,
                           costheta_D_p5 = costheta_D_p5,
                           costheta_D_p6 = costheta_D_p6,
                           costheta_X_p0 = costheta_X_p0,
                           costheta_X_p1 = costheta_X_p1,
                           costheta_X_p2 = costheta_X_p2,
                           costheta_X_p3 = costheta_X_p3,
                           costheta_X_p4 = costheta_X_p4,
                           costheta_X_p5 = costheta_X_p5,
                           costheta_X_p6 = costheta_X_p6,
                           chi_p0 = chi_p0,
                           chi_p1 = chi_p1,
                           chi_p2 = chi_p2)


    integral = custom_pdf.integrate(limits=obs)  # = 1 since normalized

    #Load full sim MC to represent data
    #Full sim MC, to represent the data
    path_data = an.loc.WGEOS+"/tuples/MC/Bd2DstDsst"
    file_list = []
    for y in ["2015","2016","2017","2018"]:
        for m in ["Up","Down"]:
            #file_list.append("%s/%s_%s/Total_Bd2DstDsst_Vars.root" % (path_data,y,m))
            file_list.append("%s_ReDecay/%s_%s/Total_Bd2DstDsst_Vars.root" % (path_data,y,m))
    df = read_root(file_list,"DecayTree")
    df = df.query("B_BKGCAT==10")

    df_data = df[["costheta_D_reco","costheta_X_VV_reco","chi_VV_reco"]]

    data = zfit.Data.from_pandas(df_data, obs=obs)

    # Stage 1: create an unbinned likelihood with the given PDF and dataset
    nll = zfit.loss.UnbinnedNLL(model=custom_pdf, data=data)

    # Stage 2: instantiate a minimiser (in this case a basic minuit
    minimizer = zfit.minimize.Minuit(tolerance=1e-5)

    #Stage 3: minimise the given negative likelihood
    result = minimizer.minimize(nll)

    param_errors, _ = result.errors(method="minuit_minos")

    print("Function minimum:", result.fmin)
    print("Converged:", result.converged)
    print("Full minimizer information:", result.info)

    params = result.params
    print(params)

    #Write fit results to JSON file
    results_dict = {}
    for p in params:
        results_dict["%s" % p.name] = [params[p]['value'], param_errors[p]['lower'], param_errors[p]['upper']]

    with open(an.loc.ROOT+'output/json/unbinned_angular_fit_MC_results.json', 'w') as f:
        json.dump(results_dict, f, sort_keys=True, indent=4)


    #Plot projections
    bins = 30
    data_np = zfit.run(data)
    print(data_np)

    for v in obs_dict:

        fig, ax = plt.subplots(figsize=(7,7))
        lower = obs_dict[v][2]
        upper = obs_dict[v][3]
        counts, bin_edges = np.histogram(df_data[v], bins=bins, range=(lower, upper))
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
        err = np.sqrt(counts)
        #Normalise
        err = err / np.sum(counts)
        counts = counts / np.sum(counts)

        #Plot the data
        plt.errorbar(bin_centres, counts, yerr=err, fmt='o', color='xkcd:black')

        #Plot the PDF
        n_p = 1000
        x_plot = np.linspace(lower, upper, num=n_p)
        x_plot_data = zfit.Data.from_numpy(array=x_plot, obs=obs_dict[v][4])

        #Other dims
        other_obs = []
        for k in obs_dict:
            if(k!=v):
                other_obs.append(obs_dict[k][4])

        #Observables to integrate over
        obs_int = other_obs[0] * other_obs[1]

        #Integrating over the other dimensions
        pdf_x = custom_pdf.partial_integrate(x=x_plot_data, limits=obs_int)
        y_plot = zfit.run(pdf_x)
        #Normalise
        y_plot = y_plot / np.sum(y_plot) * (n_p/bins)

        plt.plot(x_plot, y_plot, color='crimson')

        #Labels and axis ranges
        plt.xlabel(obs_dict[v][1],fontsize=25)
        bin_width = float(upper - lower)/bins
        y_min, y_max = ax.get_ylim()
        plt.ylim(0.0,y_max*1.4)
        plt.xlim(lower, upper)
        ax.tick_params(axis='both', which='major', labelsize=25)
        plt.tight_layout()
        fig.savefig(an.loc.ROOT+f"output/plots/{v}_unbinned_fit_MC.pdf")

    #Plot 2D data, results, and residuals
    bins_2d = 15
    for v1 in obs_dict:
        for v2 in obs_dict:
            if(v1!=v2):

                #2D data
                fig, ax = plt.subplots(figsize=(8,7))
                h_data, x_data, y_data, i_data = ax.hist2d(df_data[v1],df_data[v2],bins=bins_2d,range=[[obs_dict[v1][2],obs_dict[v1][3]], [obs_dict[v2][2],obs_dict[v2][3]]],cmap='magma')
                plt.xlabel(obs_dict[v1][1],fontsize=25)
                plt.ylabel(obs_dict[v2][1],fontsize=25)
                plt.title("Data",fontsize=20)
                cbar = plt.colorbar(i_data, ax=ax)
                cbar.ax.tick_params(labelsize=20)
                ax.tick_params(axis='both', which='major', labelsize=25)
                plt.tight_layout()
                fig.savefig(an.loc.ROOT+f'output/plots/unbinned_fit_MC_data_{v1}_vs_{v2}.pdf')

                #2D PDF
                n_samp = 1e6
                sample = custom_pdf.sample(n_samp)
                sample_np = zfit.run(sample)
                if(v1=="costheta_D_reco"):
                    i = 0
                elif(v1=="costheta_X_VV_reco"):
                    i = 1
                else:
                    i = 2
                if(v2=="costheta_D_reco"):
                    j = 0
                elif(v2=="costheta_X_VV_reco"):
                    j = 1
                else:
                    j = 2
                fig, ax = plt.subplots(figsize=(8,7))
                h_pdf, x_pdf, y_pdf, i_pdf = ax.hist2d(sample_np[:,i],sample_np[:,j],bins=bins_2d,range=[[obs_dict[v1][2],obs_dict[v1][3]], [obs_dict[v2][2],obs_dict[v2][3]]],cmap='magma')
                #Scale values to match data yield
                h_pdf = h_pdf * float(len(df))/n_samp
                plt.xlabel(obs_dict[v1][1],fontsize=25)
                plt.ylabel(obs_dict[v2][1],fontsize=25)
                plt.title("Fit",fontsize=20)
                cbar = plt.colorbar(i_data, ax=ax)
                cbar.ax.tick_params(labelsize=20)
                ax.tick_params(axis='both', which='major', labelsize=25)
                plt.tight_layout()
                fig.savefig(an.loc.ROOT+f'output/plots/unbinned_fit_MC_pdf_{v1}_vs_{v2}.pdf')

                #Residuals
                resid = np.subtract(list(h_data),list(h_pdf))
                err = np.sqrt(list(h_data))
                resid = resid / err
                #Some bins with zero content end up with NaN when dividing by error above - set them to zero
                resid[np.isnan(resid)] = 0.
                resid[np.isinf(np.abs(resid))] = 0.

                fig, ax = plt.subplots(figsize=(8,7))

                im = ax.imshow(resid,origin='lower',extent=[obs_dict[v1][2], obs_dict[v1][3], obs_dict[v2][2], obs_dict[v2][3]],aspect='auto',clim=(-5.,5.),cmap='RdBu')
                v = np.linspace(-5., 5., 11, endpoint=True)
                cbar = plt.colorbar(im, ax=ax, ticks=v)
                cbar.ax.tick_params(labelsize=20)
                plt.xlabel(obs_dict[v1][1],fontsize=25)
                plt.ylabel(obs_dict[v2][1],fontsize=25)
                plt.title("Residuals",fontsize=20)
                ax.tick_params(axis='both', which='major', labelsize=25)
                plt.tight_layout()
                fig.savefig(an.loc.ROOT+f'output/plots/unbinned_fit_MC_resid_{v1}_vs_{v2}.pdf')

if __name__ == '__main__':
    main()
