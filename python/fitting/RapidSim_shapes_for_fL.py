import ROOT
import analysis as an
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHILLdini.cpp")
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHORNSdini.cpp")
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHILLdini_misID.cpp")
ROOT.gROOT.LoadMacro(an.loc.ROOT+"B2DHPartRecoPDFs/src/RooHORNSdini_misID.cpp")
from ROOT import gSystem, gStyle, gInterpreter
from ROOT import RooCategory, RooAbsReal, RooFormulaVar, RooDataSet, RooDataHist, RooRealVar, RooGaussian, RooChebychev, RooExponential, RooAddPdf, RooArgList, RooArgSet, RooFit, RooRandom, gApplication, RooUnblindUniform, RooMCStudy
from ROOT import RooStats, RooAbsData, RooCBShape, RooSimultaneous, RooHILLdini, RooHILLdini_misID, RooHORNSdini, RooHORNSdini_misID
from ROOT import TFile, TChain, TNtuple, TCanvas, TH1F, TH2F, TH3F, TRandom3, TList

from root_pandas import read_root, to_root
import pandas as pd
import numpy as np
import json
import random
import sys, os, argparse
from listsfromhist import listsfromhist

import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def Plot(frame,low,high,bin_width,mode):
	D = frame.getObject(0)
	data_arr = listsfromhist(D, overunder = True)

	fig, ax = plt.subplots(figsize=(8,6))

	#Plot data
	plt.errorbar(data_arr[0],data_arr[1],yerr=data_arr[2],ls=None,color='k',fmt='o',markersize=2,label="Data")

	plt.ylabel("Candidates / (%.3f GeV/$c^2$)" % bin_width,fontsize=25)
	plt.xlabel("$m(D^{*-}D_s^{+})$ [GeV/$c^2$]",fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(low,high)

	x_vals = np.linspace(low,high,1000)

	#Total
	P = frame.getObject(1)
	pdf_arr = []
	for v in x_vals:
		pdf_arr.append(P.interpolate(v))
	plt.plot(x_vals,pdf_arr,color="crimson",linewidth=2.0,label="Total fit")

	ymin, ymax = ax.get_ylim()
	plt.ylim(0.1,ymax*1.5)
	#plt.yscale("log")
	plt.legend(fontsize=20,loc='upper left')
	plt.tight_layout()
	plt.show()
	fig.savefig(an.loc.ROOT+f"output/plots/MC_{mode}.pdf")

def run_fit(mode):

    DstDsst_modes = {"Bd2DstDsst_Dsst2DsPi0_HELAMP100": [5.0,5.2],
                     "Bd2DstDsst_Dsst2DsPi0_HELAMP010": [5.0,5.2],
                     "Bd2DstDsst_HELAMP100": [4.9,5.3],
                     "Bd2DstDsst_HELAMP010": [4.95,5.25]
                     }

    feed_down_modes = {"Bd2D2420Ds": [4.9,5.15],
                       "Bd2DstDs2460_Dsgamma": [4.9,5.05],
                       "Bd2DstDs2460_Dsstpi0": [4.9,5.01],
                       "Bd2D2420Dsst": [4.8,5.02]
                       }

    all_modes = {}
    all_modes.update(DstDsst_modes)
    all_modes.update(feed_down_modes)

    if mode in DstDsst_modes:
        path = an.loc.WGEOS+f"/RapidSim_tuples/{mode}/KKPi_LHCb_Total/Total_Vars.root"
        var = "DstDs_M"
    else:
        path = an.loc.WGEOS+f"/RapidSim_tuples/{mode}/model_KKPi_LHCb_tree.root"
        var = "B_M"

    tree = TChain("DecayTree")
    tree.Add(path)

    tree.SetBranchStatus("*",0)
    #Fit variable
    tree.SetBranchStatus(var,1)

    print("Events in tree: %s" % tree.GetEntries())

    b_low = all_modes[mode][0]
    b_high = all_modes[mode][1]
    B_M = RooRealVar(var,"",b_low,b_high)
    bin_width = (b_high - b_low)/300
    num_bins = int((b_high-b_low)/bin_width)
    B_M.setBins(num_bins)

    args = RooArgSet(B_M)

    data = RooDataSet("data","data",args,RooFit.Import(tree))
    data.Print()

    #PDFs for each mode
    if(mode=="Bd2DstDsst_HELAMP010"):

        a = RooRealVar("a","",5.0,4.7,5.3)
        b = RooRealVar("b","",5.2,4.7,5.4)
        csi = RooRealVar("csi","",1.0,0.0,10.0)
        sigma = RooRealVar("sigma","",0.014,0,100)
        r = RooRealVar("r","",3.0,0,10)
        f = RooRealVar("f","",0.5,0,1)
        shift = RooRealVar("shift","",0)

        sig_pdf = RooHILLdini("sig_pdf","",B_M,a,b,csi,shift,sigma,r,f)

    elif(mode=="Bd2DstDsst_HELAMP100"):

        a = RooRealVar("a","",4.9,4.7,5.3)
        b = RooRealVar("b","",5.2,4.7,5.5)
        csi_horns = RooRealVar("csi_horns","",1.0)
        csi_hill = RooRealVar("csi_hill","",1.0)
        sigma_horns = RooRealVar("sigma_horns","",0.01,0,1)
        sigma_hill = RooRealVar("sigma_hill","",0.01,0,1)
        r_horns = RooRealVar("r_horns","",1.5,0,100)
        r_hill = RooRealVar("r_hill","",1.5,0,100)
        f_horns = RooRealVar("f_horns","",0.8,0,1)
        f_hill = RooRealVar("f_hill","",0.8,0,1)
        shift_horns = RooRealVar("shift_horns","",0)
        shift_hill = RooRealVar("shift_hill","",0)

        sig_pdf_horns = RooHORNSdini("sig_pdf_horns","",B_M,a,b,csi_horns,shift_horns,sigma_horns,r_horns,f_horns)
        sig_pdf_hill = RooHILLdini("sig_pdf_hill","",B_M,a,b,csi_hill,shift_hill,sigma_hill,r_hill,f_hill)

        frac_horns = RooRealVar("frac_horns","",0.5,0,1)

        sig_pdf = RooAddPdf("sig_pdf","",RooArgList(sig_pdf_horns,sig_pdf_hill),RooArgList(frac_horns))

    elif(mode=="Bd2DstDsst_Dsst2DsPi0_HELAMP010"):

        a = RooRealVar("a","",5.07,4.7,5.3)
        b = RooRealVar("b","",5.12,4.7,5.4)
        csi = RooRealVar("csi","",1.0,0.0,10.0)
        sigma = RooRealVar("sigma","",0.014,0,100)
        r = RooRealVar("r","",1.0,0,100)
        f = RooRealVar("f","",0.5,0,1)
        shift = RooRealVar("shift","",0)

        sig_pdf = RooHORNSdini("sig_pdf","",B_M,a,b,csi,shift,sigma,r,f)

    elif(mode=="Bd2DstDsst_Dsst2DsPi0_HELAMP100"):

        a = RooRealVar("a","",4.9,4.7,5.1)
        b = RooRealVar("b","",5.2,5.1,5.5)
        csi = RooRealVar("csi","",1.0)
        sigma = RooRealVar("sigma","",0.01,0,1)
        r = RooRealVar("r","",1.5,0,100)
        f = RooRealVar("f","",0.8,0,1)
        shift = RooRealVar("shift","",0)

        sig_pdf = RooHILLdini("sig_pdf","",B_M,a,b,csi,shift,sigma,r,f)

    elif(mode=="Bd2D2420Ds"):

        a = RooRealVar("a","",3.0,0.0,5.0)
        b = RooRealVar("b","",5.05,4.7,5.5)
        csi = RooRealVar("csi","",1.0,0.,10.)
        sigma = RooRealVar("sigma","",0.01,0,1)
        r = RooRealVar("r","",1.5,0,100)
        f = RooRealVar("f","",0.8,0,1)
        shift = RooRealVar("shift","",0)

        sig_pdf = RooHORNSdini("sig_pdf","",B_M,a,b,csi,shift,sigma,r,f)

    elif(mode=="Bd2DstDs2460_Dsgamma"):

        a = RooRealVar("a","",3.0,0.0,5.0)
        b = RooRealVar("b","",5.05,4.7,5.5)
        csi = RooRealVar("csi","",1.0,0.,100.)
        sigma = RooRealVar("sigma","",0.01,0,1)
        r = RooRealVar("r","",1.5,0,100)
        f = RooRealVar("f","",0.8,0,1)
        shift = RooRealVar("shift","",0)

        sig_pdf = RooHORNSdini("sig_pdf","",B_M,a,b,csi,shift,sigma,r,f)

    elif(mode=="Bd2DstDs2460_Dsstpi0"):

        a = RooRealVar("a","",3.0,0.0,4.9)
        b = RooRealVar("b","",5.0,4.9,5.5)
        csi = RooRealVar("csi","",20.0,0.,100.)
        sigma = RooRealVar("sigma","",0.01,0,1)
        r = RooRealVar("r","",1.0)#1.5,0,5)
        f = RooRealVar("f","",1.0)#0.8,0,1)
        shift = RooRealVar("shift","",0)

        sig_pdf = RooHILLdini("sig_pdf","",B_M,a,b,csi,shift,sigma,r,f)

    elif(mode=="Bd2D2420Dsst"):

        a = RooRealVar("a","",1.0,0.0,5.0)
        b = RooRealVar("b","",4.96,4.9,5.2)
        csi = RooRealVar("csi","",1.0,0.,10.)
        sigma = RooRealVar("sigma","",0.01,0,1)
        r = RooRealVar("r","",1.0)
        f = RooRealVar("f ","",1.0)
        shift = RooRealVar("shift","",0)

        sig_pdf = RooHILLdini("sig_pdf","",B_M,a,b,csi,shift,sigma,r,f)

    result = sig_pdf.fitTo(data,RooFit.Save(),RooFit.NumCPU(8,0),RooFit.Optimize(False),RooFit.Offset(True),RooFit.Minimizer("Minuit2","migrad"),RooFit.Strategy(2))
    result.Print("V")

    file = TFile(an.loc.ROOT+f"output/roofitresults/MC_{mode}.root","RECREATE")
    file.cd()
    result.Write()
    file.Write()
    file.Close()

    frame = B_M.frame()
    data.plotOn(frame)
    sig_pdf.plotOn(frame)

    Plot(frame,b_low,b_high,bin_width,mode)


def main():
    parser = argparse.ArgumentParser(description='Fit to RapidSim m(D* Ds) distributions to determine PDF params for fL fit')
    parser.add_argument("--Mode", choices=["Bd2DstDsst_Dsst2DsPi0_HELAMP100",
                                           "Bd2DstDsst_Dsst2DsPi0_HELAMP010",
                                           "Bd2DstDsst_HELAMP100",
                                           "Bd2DstDsst_HELAMP010",
                                           "Bd2D2420Ds",
                                           "Bd2DstDs2460_Dsgamma",
                                           "Bd2DstDs2460_Dsstpi0",
                                           "Bd2D2420Dsst"
                                           ],required=True, help="Decay mode")
    args = parser.parse_args()

    run_fit(args.Mode)

if __name__ == '__main__':
    main()
