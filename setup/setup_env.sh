export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

wget -nv http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
bash miniconda.sh -b -p $HOME/miniconda
source $HOME/miniconda/etc/profile.d/conda.sh
conda create -n ana_env python=3.7 root -c conda-forge
conda activate ana_env
conda config --env --add channels conda-forge
#Install required packages
conda install -y root_pandas iminuit tensorflow uncertainties matplotlib flake8
pip install rootpy # needed for TensorFlowAnalysis, not working with ROOT > 6.18
pip install snakemake
pip install zfit
pip install progressbar
pip install num2tex
#hepvecctor which is not released in conda or pip
cd $ANAROOT
git clone git://github.com/henryiii/hepvector
cd hepvector
python setup.py install
cd ..
#TensorFlowAnalysis
git clone https://gitlab.cern.ch/poluekt/TensorFlowAnalysis.git
#Meerkat for density histograms
cd $ANAROOT
git clone https://gitlab.cern.ch/poluekt/Meerkat.git
#Local edits to Meerkat to do 3D density and use of Double_t branches
cp local_Meerkat_src/*.cpp Meerkat/src/
cp local_Meerkat_src/*.hh Meerkat/inc/
cd Meerkat
make
cd ..
#RooHORNSdini and RooHILLdini shapes
git clone https://gitlab.cern.ch/lhcb-b2oc/CommonAnalysisTools/B2DHPartRecoPDFs.git
cp B2DHPartRecoPDFs/inc/* B2DHPartRecoPDFs/src/
cp B2DHPartRecoPDFs/obj/* B2DHPartRecoPDFs/src/
