#!/bin/sh

export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

cd $ANAROOT/condor

for boost in N Y
do
  condor_submit submit_scripts/toys_unbinned_angular_fit_boost_${boost}.submit
done
