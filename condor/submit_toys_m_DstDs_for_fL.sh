#!/bin/sh

export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

cd $ANAROOT/condor

condor_submit submit_scripts/toys_m_DstDs_for_fL.submit
