configfile : 'cfg.yml'
print('CONFIGURATION:',config)

if config['test'] == 'True' :
    config['test'] = " --test "
else :
    config['test'] = ""

import analysis as an
import glob

shell.executable("/bin/bash")
shell.prefix('cd $ANAROOT && source setup/setup.sh && ')

#Reconstruction modes for each decay
mode_reco = {"Bd2DstDs": ["Bd2DstDs"],
             "Bd2DstDsst": ["Bd2DstDs","Bd2DstDsst"],
             "Bd2DstDsst_ReDecay": ["Bd2DstDs","Bd2DstDsst"]
            }

################################################################################
### Define output files for steps, which are passed to the master rule "all" ###
################################################################################

###########################
### Calculate variables ###
###########################

#List of output files with variables added for full sim MC
out_list_vars = []
for d in config["DataType"]:
    for m in config["Mode"]:
        for y in config["Year"]:
            for mag in config["Mag"]:
                for m_reco in mode_reco[m]:
                    #All data stored under Bd2DstDs directory
                    if(d=="data"):
                        m = "Bd2DstDs"
                    out_list_vars.append(f"{an.loc.WGEOS}/tuples/{d}/{m}/{y}_{mag}/Total_{m_reco}_Vars.root")

#RapidSim MC
out_list_vars_RapidSim = []
for geom in ["All","LHCb"]:
    out_list_vars_RapidSim.append(f"{an.loc.WGEOS}/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_{geom}_Vars.root")

##############################################
### Create weight histograms for templates ###
##############################################

#Output file containing angular coeff weight hists
w_hist_out = f"{an.loc.ROOT}output/hists/weight_hists_Bd2DstDsst.root"

#########################
### Add weights to MC ###
#########################

#MC files with weights added
#Weights only added to Bd2DstDsst(_ReDecay) reco as Bd2DstDsst
out_list_weights = []
for m in ["Bd2DstDsst","Bd2DstDsst_ReDecay"]:
    for y in config["Year"]:
        for mag in config["Mag"]:
            for m_reco in ["Bd2DstDsst"]:
                out_list_weights.append(f"{an.loc.WGEOS}/tuples/MC/{m}/{y}_{mag}/Total_{m_reco}_Vars_Weights.root")

#RapidSim MC
out_list_weights_RapidSim = []
for geom in ["All","LHCb"]:
    out_list_weights_RapidSim.append(f"{an.loc.WGEOS}/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_{geom}_Vars_Weights.root")

###################################################
### sWeight fit of fully reco B0 -> D* Ds* data ###
###################################################

#Data with sWeights applied, RooFitResult, plots, and json file containing yield
out_list_sFit = [an.loc.ROOT+"output/rootfiles/Bd2DstDsst_sWeights.root",
                 an.loc.ROOT+"output/roofitresults/m_DstDsst_for_sWeights.root",
                 an.loc.ROOT+"output/plots/m_DstDsst_for_sWeights_fit.pdf",
                 an.loc.ROOT+"output/plots/m_DstDsst_for_sWeights_pull.pdf",
                 an.loc.ROOT+"output/json/m_DstDsst_for_sWeights.json"
                 ]

#################################################################
### Derive acceptance functions for costheta_D and costheta_X ###
#################################################################

out_list_acc_costheta_X_D = []

#Polynomial fit pars
out_list_acc_costheta_X_D.append(an.loc.ROOT+"output/json/acc_pars_costheta_X_D.json")
#Same but with systematically varied fL
out_list_acc_costheta_X_D.append(an.loc.ROOT+"output/json/acc_pars_costheta_X_D_syst.json")
#Plots, covariance matrices, and latex correlation matrices
for angle in ["costheta_D","costheta_X_VV"]:
    out_list_acc_costheta_X_D.append(an.loc.ROOT+f"output/plots/Data_RapidSim_True_MC_compare_{angle}.pdf")
    out_list_acc_costheta_X_D.append(an.loc.ROOT+f"output/plots/Data_RapidSim_True_MC_ratio_{angle}.pdf")
    out_list_acc_costheta_X_D.append(an.loc.ROOT+f"output/json/acc_pars_cov_{angle}.npy")
    out_list_acc_costheta_X_D.append(an.loc.ROOT+f"output/tex/acc_pars_corr_m_{angle}.tex")

##########################################################################
### Derive acceptance functions for costheta_D and costheta_X using MC ###
##########################################################################

out_list_acc_costheta_X_D_MC = []

#Polynomial fit pars
out_list_acc_costheta_X_D_MC.append(an.loc.ROOT+"output/json/MC_acc_pars_costheta_X_D.json")
#Plots, covariance matrices, and latex correlation matrices
for angle in ["costheta_D","costheta_X_VV"]:
    out_list_acc_costheta_X_D_MC.append(an.loc.ROOT+f"output/plots/FullSim_Reco_MC_RapidSim_True_MC_compare_{angle}.pdf")
    out_list_acc_costheta_X_D_MC.append(an.loc.ROOT+f"output/plots/FullSim_Reco_MC_RapidSim_True_MC_ratio_{angle}.pdf")
    out_list_acc_costheta_X_D_MC.append(an.loc.ROOT+f"output/json/MC_acc_pars_cov_{angle}.npy")
    out_list_acc_costheta_X_D_MC.append(an.loc.ROOT+f"output/tex/MC_acc_pars_corr_m_{angle}.tex")

##########################################
### Derive acceptance function for chi ###
##########################################

out_list_acc_chi = []

#Polynomial fit pars
out_list_acc_chi.append(an.loc.ROOT+"output/json/acc_pars_chi.json")
#Plots, covariance matrices, and latex correlation matrices
for angle in ["chi_VV"]:
    out_list_acc_chi.append(an.loc.ROOT+f"output/plots/FullSim_Reco_MC_RapidSim_True_MC_compare_{angle}.pdf")
    out_list_acc_chi.append(an.loc.ROOT+f"output/plots/FullSim_Reco_MC_RapidSim_True_MC_ratio_{angle}.pdf")
    out_list_acc_chi.append(an.loc.ROOT+f"output/json/acc_pars_cov_{angle}.npy")
    out_list_acc_chi.append(an.loc.ROOT+f"output/tex/acc_pars_corr_m_{angle}.tex")

#####################################
### Write tex tables for acc pars ###
#####################################

out_list_acc_table = [an.loc.ROOT+"output/tex/acc_pars.tex"]

####################################
### Unbinned angular fit to data ###
####################################

out_list_unbinned_fit = []
#Plots
for angle in ["costheta_D","costheta_X_VV","chi_VV"]:
    out_list_unbinned_fit.append(an.loc.ROOT+f"output/plots/{angle}_reco_unbinned_fit.pdf")
for angle1 in ["costheta_D","costheta_X_VV","chi_VV"]:
    for angle2 in ["costheta_D","costheta_X_VV","chi_VV"]:
        if(angle1!=angle2):
            out_list_unbinned_fit.append(an.loc.ROOT+f"output/plots/unbinned_fit_data_{angle1}_reco_vs_{angle2}_reco.pdf")
            out_list_unbinned_fit.append(an.loc.ROOT+f"output/plots/unbinned_fit_pdf_{angle1}_reco_vs_{angle2}_reco.pdf")
            out_list_unbinned_fit.append(an.loc.ROOT+f"output/plots/unbinned_fit_resid_{angle1}_reco_vs_{angle2}_reco.pdf")

#Results JSON
out_list_unbinned_fit.append(an.loc.ROOT+"output/json/unbinned_angular_fit_results.json")

##################################
### Unbinned angular fit to MC ###
##################################

out_list_unbinned_fit_MC = []
#Plots
for angle in ["costheta_D","costheta_X_VV","chi_VV"]:
    out_list_unbinned_fit_MC.append(an.loc.ROOT+f"output/plots/{angle}_reco_unbinned_fit_MC.pdf")
for angle1 in ["costheta_D","costheta_X_VV","chi_VV"]:
    for angle2 in ["costheta_D","costheta_X_VV","chi_VV"]:
        if(angle1!=angle2):
            out_list_unbinned_fit_MC.append(an.loc.ROOT+f"output/plots/unbinned_fit_MC_data_{angle1}_reco_vs_{angle2}_reco.pdf")
            out_list_unbinned_fit_MC.append(an.loc.ROOT+f"output/plots/unbinned_fit_MC_pdf_{angle1}_reco_vs_{angle2}_reco.pdf")
            out_list_unbinned_fit_MC.append(an.loc.ROOT+f"output/plots/unbinned_fit_MC_resid_{angle1}_reco_vs_{angle2}_reco.pdf")

#Results JSON
out_list_unbinned_fit_MC.append(an.loc.ROOT+"output/json/unbinned_angular_fit_MC_results.json")

#######################################################################
### Generate condor submission script for unbinned angular fit toys ###
#######################################################################

out_list_make_toy_unbinned_angular_fit_submit_script_condor = []

for boost in ["N","Y"]:
    out_list_make_toy_unbinned_angular_fit_submit_script_condor.append(an.loc.ROOT+f"condor/submit_scripts/toys_unbinned_angular_fit_boost_{boost}.submit")

#################################
### Analyse unbinned fit toys ###
#################################

out_list_unbinned_fit_toys = []
#Toy pull plots
for par in ["Hm_amp","Hp_phi","Hm_phi"]:
    #Stats boost
    for boost in ["N","Y"]:
        out_list_unbinned_fit_toys.append(an.loc.ROOT+f"output/plots/pull_{par}_boost_{boost}.pdf")
#Pulls summary json
for boost in ["N","Y"]:
    out_list_unbinned_fit_toys.append(an.loc.ROOT+f"output/json/unbinned_angular_fit_pulls_boost_{boost}.json")

#######################################################################
### Generate condor submission script for unbinned angular fit syst ###
#######################################################################

out_list_make_syst_unbinned_angular_fit_submit_script_condor = []

for s in ["fL","data_sw","acc_funcs"]:
    out_list_make_syst_unbinned_angular_fit_submit_script_condor.append(an.loc.ROOT+f"condor/submit_scripts/syst_unbinned_angular_fit_{s}.submit")

########################################
### Analyse unbinned fit systematics ###
########################################

out_list_unbinned_fit_syst = []
#json file containing values
out_list_unbinned_fit_syst.append(an.loc.ROOT+"output/json/unbinned_angular_fit_syst.json")
#tex table
out_list_unbinned_fit_syst.append(an.loc.ROOT+"output/tex/unbinned_angular_fit_syst.tex")

##########################################################
### Fits to RapidSim samples for shapes used in fL fit ###
##########################################################

part_reco_modes = ["Bd2DstDsst_Dsst2DsPi0_HELAMP100",
                   "Bd2DstDsst_Dsst2DsPi0_HELAMP010",
                   "Bd2DstDsst_HELAMP100",
                   "Bd2DstDsst_HELAMP010",
                   "Bd2D2420Ds",
                   "Bd2DstDs2460_Dsgamma",
                   "Bd2DstDs2460_Dsstpi0",
                   "Bd2D2420Dsst"
                   ]
out_list_RapidSim_shapes_for_fL = []

#RooFitResult for each part reco mode, used as input for shapes in m(D* Ds) fit to measure fL
for mode in part_reco_modes:
    out_list_RapidSim_shapes_for_fL.append(an.loc.ROOT+f"output/roofitresults/MC_{mode}.root")

##################################
### m(D* Ds) fit to measure fL ###
##################################

out_list_fL_fit = []
#RooFitResult
out_list_fL_fit.append(an.loc.ROOT+"output/roofitresults/m_DstDs_for_fL.root")
#Plots
for log in ["Y","N"]:
    out_list_fL_fit.append(an.loc.ROOT+f"output/plots/m_DstDs_for_fL_fit_log_{log}.pdf")
out_list_fL_fit.append(an.loc.ROOT+"output/plots/m_DstDs_for_fL_pull.pdf")
#json containing the result for fL used elsewhere, and the yields of B0 -> D* Ds* and B0 -> D* Ds used in BF measurement
out_list_fL_fit.append(an.loc.ROOT+"output/json/m_DstDs_for_fL.json")
#Fit result tex table
out_list_fL_fit.append(an.loc.ROOT+"output/tex/m_DstDs_for_fL.tex")


#######################################################################
### Generate condor submission script for unbinned angular fit toys ###
#######################################################################

out_list_make_toy_m_DstDs_for_fL_submit_script_condor = [an.loc.ROOT+"condor/submit_scripts/toys_m_DstDs_for_fL.submit"]

###########################
### Analyse fL fit toys ###
###########################

out_list_m_DstDs_for_fL_toys = []
#Toy pull plots
for par in ["fL"]:
    out_list_m_DstDs_for_fL_toys.append(an.loc.ROOT+f"output/plots/pull_{par}.pdf")
#Pulls summary json
out_list_m_DstDs_for_fL_toys.append(an.loc.ROOT+"output/json/m_DstDs_for_fL_pulls.json")

#######################################################################
### Generate condor submission script for unbinned angular fit syst ###
#######################################################################

out_list_make_syst_m_DstDs_for_fL_submit_script_condor = []

for s in ["PDFs","BFs"]:
    out_list_make_syst_m_DstDs_for_fL_submit_script_condor.append(an.loc.ROOT+f"condor/submit_scripts/syst_m_DstDs_for_fL_{s}.submit")


##################################
### Analyse fL fit systematics ###
##################################

out_list_m_DstDs_for_fL_syst = []
#json file containing values
out_list_m_DstDs_for_fL_syst.append(an.loc.ROOT+"output/json/m_DstDs_for_fL_syst.json")
#tex table
out_list_m_DstDs_for_fL_syst.append(an.loc.ROOT+"output/tex/m_DstDs_for_fL_syst.tex")

################################################
### Make table of feed-down fraction results ###
################################################

out_list_feed_down_table = [an.loc.ROOT+"output/tex/feed_down.tex"]

####################################################################
### Calculate effs for B0 -> D* Ds and B0 -> D* Ds* for BF ratio ###
####################################################################

out_list_effs = [an.loc.ROOT+"output/json/effs.json"]

######################
### Make eff table ###
######################

out_list_eff_table = [an.loc.ROOT+"output/tex/effs.tex"]

##########################
### Calculate BF ratio ###
##########################

out_list_BF_ratio = [an.loc.ROOT+"output/json/BF_ratio.json"]

############################################
### Make latex results for ANA and PAPER ###
############################################

out_list_final_results = []
out_list_final_results.append(an.loc.ROOT+"output/tex/results_def.tex")

#############################################
### Plot unbinned fit results as true PDF ###
#############################################

out_list_plot_model = []
for angle in ["costheta_D","costheta_X","chi"]:
    out_list_plot_model.append(an.loc.ROOT+f"output/plots/{angle}_results_vs_WA.pdf")


######################
### Table of pulls ###
######################

out_list_pull_table = [an.loc.ROOT+"output/tex/pulls.tex"]

##################################
### Fit B0 -> D* Ds* MC B peak ###
##################################

out_list_MC_Bd2DstDsst_fullreco_fit = []
out_list_MC_Bd2DstDsst_fullreco_fit.append(an.loc.ROOT+"output/roofitresults/MC_Bd2DstDsst_fullreco.root")
out_list_MC_Bd2DstDsst_fullreco_fit.append(an.loc.ROOT+"output/json/MC_Bd2DstDsst_fullreco.json")
out_list_MC_Bd2DstDsst_fullreco_fit.append(an.loc.ROOT+"output/plots/MC_Bd2DstDsst_fullreco_M_B.pdf")

#################################
### Fit B0 -> D* Ds MC B peak ###
#################################

out_list_MC_Bd2DstDs_fit = []
out_list_MC_Bd2DstDs_fit.append(an.loc.ROOT+"output/roofitresults/MC_Bd2DstDs.root")
out_list_MC_Bd2DstDs_fit.append(an.loc.ROOT+"output/json/MC_Bd2DstDs.json")
out_list_MC_Bd2DstDs_fit.append(an.loc.ROOT+"output/plots/MC_Bd2DstDs_M_B.pdf")

#########################
### Run full pipeline ###
#########################

rule all:
    input:
        vars_files = out_list_vars, #Data and full sim MC files with variables added
        vars_files_RapidSim = out_list_vars_RapidSim, #RapidSim MC with variables added
        w_hist = w_hist_out, #ROOT file with angular coeff weight hists
        weight_files = out_list_weights, #MC with angular coeff weights applied
        weight_files_RapidSim = out_list_weights_RapidSim, #RapidSim MC with angular coeff weights applied
        sFit_files = out_list_sFit, #B0 -> D* Ds* fully reco data with sWeights applied
        acc_costheta_X_D_files = out_list_acc_costheta_X_D, #Acceptance function fits for costheta_D and costheta_X
        acc_costheta_X_D_MC_files = out_list_acc_costheta_X_D_MC, #Acceptance function fits for costheta_D and costheta_X using MC
        acc_chi_files = out_list_acc_chi, #Acceptance function for chi
        acc_table_file = out_list_acc_table, #Table of acceptance parameters
        unbinned_fit_files = out_list_unbinned_fit, #Unbinned angular fit
        unbinned_fit_MC_files = out_list_unbinned_fit_MC, #Unbinned angular fit to MC
        make_toy_unbinned_angular_fit_submit_script_condor_files = out_list_make_toy_unbinned_angular_fit_submit_script_condor, #Make condor submission script for ubninned fit toys
        unbinned_fit_toys = out_list_unbinned_fit_toys, #Analyse unbinned fit toys
        make_syst_unbinned_angular_fit_submit_script_condor_files = out_list_make_syst_unbinned_angular_fit_submit_script_condor, #Make condor submission script for ubninned fit systematics
        unbinned_fit_syst_files = out_list_unbinned_fit_syst, #Analyse unbinned fit systematics
        RapidSim_shapes_for_fL_files = out_list_RapidSim_shapes_for_fL, #Fits to part reco RapidSim samples to determine shapes for fL fit
        fL_fit_files = out_list_fL_fit, #Run fit to m(D* Ds) to measure fL
        make_toy_m_DstDs_for_fL_submit_script_condor_files = out_list_make_toy_m_DstDs_for_fL_submit_script_condor, #Make condor submission script for m(D* Ds) fL fit
        m_DstDs_for_fL_toys = out_list_m_DstDs_for_fL_toys, #Analyse m(D* Ds) for fL fit toys
        make_syst_m_DstDs_for_fL_submit_script_condor_files = out_list_make_syst_m_DstDs_for_fL_submit_script_condor, #Make condor submission script for m(D* Ds) fL fit systematics
        m_DstDs_for_fL_syst_files = out_list_m_DstDs_for_fL_syst, #Analyse fL fit systematics
        feed_down_table_files = out_list_feed_down_table, #Make table of feed-down results
        effs_files = out_list_effs, #Calculate efficiencies for BF ratio calculation
        eff_table_files = out_list_eff_table, #tex table of MC effs
        BF_ratio_files = out_list_BF_ratio, #Calculate BF ratio
        final_results_files = out_list_final_results, #Final results latex for use in ANA and PAPER
        plot_model_files = out_list_plot_model, #Plot results of angular fit as true PDF
        pull_table_files = out_list_pull_table, #tex table of pulls
        MC_Bd2DstDsst_fullreco_fit_files = out_list_MC_Bd2DstDsst_fullreco_fit #Fit the B0 -> D* Ds* MC B peak


###########################
### Rules for each step ###
###########################

####################################################
### Calculate variables for data and full sim MC ###
####################################################

rule calc_vars:
    input:
        code="python/calc_vars/calc_vars.py",
        data=an.loc.WGEOS+"/tuples/{data_type}/{mode}/{year}_{mag}/Total.root" #Input is merged ganga output files
    output: an.loc.WGEOS+"/tuples/{data_type}/{mode}/{year}_{mag}/Total_{mode_r}_Vars.root"
    shell: "python {input.code} --DataType {wildcards.data_type} --Year {wildcards.year} --Mag {wildcards.mag} --Mode {wildcards.mode} --ModeReco {wildcards.mode_r}"

################################################
### Calculate variables for RapidSim samples ###
################################################

rule calc_vars_RapidSim:
    input:
        code="python/calc_vars/calc_vars_RapidSim.py",
        data=an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/model_KKPi_{geom}_tree.root" #RapidSim output tuple
    output: an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_{geom}_Vars.root"
    shell: "python {input.code} --Geom {wildcards.geom}"

################################################################
### Create weight histograms for templates using RapidSim MC ###
################################################################

rule make_weight_hists:
    input:
        code="python/make_weight_hists/make_weight_hists.py",
        data=an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_All_Vars.root" #4pi RapidSim with vars added
    output: w_hist_out
    shell: "python {input.code}"

################################################
### Add angular coeff weights to full sim MC ###
################################################

rule add_weights:
    input:
        code="python/add_weights/add_weights.py",
        data=an.loc.WGEOS+"/tuples/MC/{mode}/{year}_{mag}/Total_{mode_r}_Vars.root", #nTuples with variables added from calc_vars rule
        whist=w_hist_out #Weight histogrm from make_weight_hists rule
    output: an.loc.WGEOS+"/tuples/MC/{mode}/{year}_{mag}/Total_{mode_r}_Vars_Weights.root"
    shell: "python {input.code} --Year {wildcards.year} --Mag {wildcards.mag} --Mode {wildcards.mode} --ModeReco {wildcards.mode_r}"

################################################
### Add angular coeff weights to RapidSim MC ###
################################################

rule add_weights_RapidSim:
    input:
        code="python/add_weights/add_weights_RapidSim.py",
        data=an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_{geom}_Vars.root", #RapidSim with variables added
        whist=w_hist_out #Weight histogrm from make_weight_hists rule
    output: an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_{geom}_Vars_Weights.root"
    shell: "python {input.code} --Geom {wildcards.geom}"

###################################################
### sWeight fit of fully reco B0 -> D* Ds* data ###
###################################################

sFit_infiles = []
for y in config["Year"]:
    for m in config["Mag"]:
        sFit_infiles.append(an.loc.WGEOS+f"/tuples/data/Bd2DstDs/{y}_{m}/Total_Bd2DstDsst_Vars.root")
#Signal shape pars from MC fit
sFit_infiles.append(an.loc.ROOT+'output/json/MC_Bd2DstDsst_fullreco.json')

rule sFit_Bd2DstDsst:
    input:
        code="python/fitting/m_DstDsst_for_sWeights.py",
        data=sFit_infiles
    output: out_list_sFit
    shell: "python {input.code}"


#################################################################
### Derive acceptance functions for costheta_D and costheta_X ###
#################################################################

acc_infiles = []
#RapidSim 4pi geometry with no cuts
acc_infiles.append(an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_All_Vars_Weights.root")
#sWeighted data
acc_infiles.append(an.loc.ROOT+"output/rootfiles/Bd2DstDsst_sWeights.root")
#Results for fL from the m(D* Ds) fit
acc_infiles.append(an.loc.ROOT+"output/json/m_DstDs_for_fL.json")
acc_infiles.append(an.loc.ROOT+"output/json/m_DstDs_for_fL_syst.json")

rule acceptance_costheta_X_D:
    input:
        code="python/acceptance/acc_costheta_X_D.py",
        data=acc_infiles
    output: out_list_acc_costheta_X_D
    shell: "python {input.code}"

##########################################################################
### Derive acceptance functions for costheta_D and costheta_X using MC ###
##########################################################################

acc_MC_infiles = []
#RapidSim 4pi geometry with no cuts
acc_MC_infiles.append(an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_All_Vars_Weights.root")
#MC
for y in config["Year"]:
    for m in config["Mag"]:
        for mode in ["Bd2DstDsst","Bd2DstDsst_ReDecay"]:
            acc_MC_infiles.append(an.loc.WGEOS+f"/tuples/MC/{mode}/{y}_{m}/Total_Bd2DstDsst_Vars.root")

rule acceptance_costheta_X_D_MC:
    input:
        code="python/acceptance/acc_costheta_X_D_MC.py",
        data=acc_MC_infiles
    output: out_list_acc_costheta_X_D_MC
    shell: "python {input.code}"


##########################################
### Derive acceptance function for chi ###
##########################################

acc_chi_infiles = []
#RapidSim 4pi geometry with no cuts
acc_chi_infiles.append(an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_All_Vars_Weights.root")
#FullSim MC
for m in ["Bd2DstDsst","Bd2DstDsst_ReDecay"]:
    for y in config["Year"]:
        for mag in config["Mag"]:
            acc_chi_infiles.append(an.loc.WGEOS+f"/tuples/MC/{m}/{y}_{mag}/Total_Bd2DstDsst_Vars_Weights.root")

rule acceptance_chi:
    input:
        code="python/acceptance/acc_chi.py",
        data=acc_chi_infiles
    output: out_list_acc_chi
    shell: "python {input.code}"


#####################################
### Write tex tables for acc pars ###
#####################################

acc_table_infiles = []
acc_table_infiles.append(an.loc.ROOT+"output/json/acc_pars_costheta_X_D.json")
acc_table_infiles.append(an.loc.ROOT+"output/json/acc_pars_chi.json")

rule make_acc_table:
    input:
        code="python/acceptance/make_acc_tables.py",
        data=acc_table_infiles
    output: out_list_acc_table
    shell: "python {input.code}"

####################################
### Unbinned angular fit to data ###
####################################

unbinned_fit_infiles = []

#sWeighted data
unbinned_fit_infiles.append(an.loc.ROOT+"output/rootfiles/Bd2DstDsst_sWeights.root")
#Acceptance function parameters
unbinned_fit_infiles.append(an.loc.ROOT+"output/json/acc_pars_costheta_X_D.json")
unbinned_fit_infiles.append(an.loc.ROOT+"output/json/acc_pars_chi.json")
#Results for fL from the m(D* Ds) fit
unbinned_fit_infiles.append(an.loc.ROOT+"output/json/m_DstDs_for_fL.json")
unbinned_fit_infiles.append(an.loc.ROOT+"output/json/m_DstDs_for_fL_syst.json")

rule unbinned_angular_fit:
    input:
        code="python/fitting/unbinned_angular_fit.py",
        data=unbinned_fit_infiles
    output: out_list_unbinned_fit
    shell: "python {input.code} --Syst none --Toy N --Boost N"

####################################
### Unbinned angular fit to MC ###
####################################

unbinned_fit_MC_infiles = []
#MC
for y in config["Year"]:
    for m in config["Mag"]:
        for mode in ["Bd2DstDsst","Bd2DstDsst_ReDecay"]:
            unbinned_fit_MC_infiles.append(an.loc.WGEOS+f"/tuples/MC/{mode}/{y}_{m}/Total_Bd2DstDsst_Vars.root")
#Acceptance function parameters
unbinned_fit_MC_infiles.append(an.loc.ROOT+"output/json/MC_acc_pars_costheta_X_D.json")
unbinned_fit_MC_infiles.append(an.loc.ROOT+"output/json/acc_pars_chi.json")

rule unbinned_angular_fit_MC:
    input:
        code="python/fitting/unbinned_angular_fit_MC.py",
        data=unbinned_fit_MC_infiles
    output: out_list_unbinned_fit_MC
    shell: "python {input.code}"

#######################################################################
### Generate condor submission script for unbinned angular fit toys ###
#######################################################################

rule make_toy_unbinned_angular_fit_submit_script_condor:
    input:
        code="condor/make_submit_scripts_toys_unbinned_angular_fit.py"
    output: out_list_make_toy_unbinned_angular_fit_submit_script_condor
    shell: "python {input.code}"

#To run toys on condor batch, do:
#sh condor/submit_toys_unbinned_angular_fit.sh

#################################
### Analyse unbinned fit toys ###
#################################

#Get list of all toy files in output folder
unbinned_fit_toy_results = glob.glob(an.loc.WGEOS+"Toys/Unbinned_Fit/*.json")

rule analyse_toys_unbinned_angular_fit:
    input:
        code="python/toys/unbinned_angular_fit.py",
        data=unbinned_fit_toy_results
    output: out_list_unbinned_fit_toys
    shell: "python {input.code}"

#######################################################################
### Generate condor submission script for unbinned angular fit syst ###
#######################################################################

rule make_syst_unbinned_angular_fit_submit_script_condor:
    input:
        code="condor/make_submit_scripts_syst_unbinned_angular_fit.py"
    output: out_list_make_syst_unbinned_angular_fit_submit_script_condor
    shell: "python {input.code}"

#To run systematics on condor batch, do:
#sh condor/submit_syst_unbinned_angular_fit.sh

########################################
### Analyse unbinned fit systematics ###
########################################

#Get list of all syst files in output folder
unbinned_fit_syst_results = glob.glob(an.loc.WGEOS+"Systematics/Unbinned_Fit/*.json")

rule analyse_syst_unbinned_angular_fit:
    input:
        code="python/systematics/unbinned_angular_fit.py",
        data=unbinned_fit_syst_results
    output: out_list_unbinned_fit_syst
    shell: "python {input.code}"

##########################################################
### Fits to RapidSim samples for shapes used in fL fit ###
##########################################################

rule RapidSim_shapes_for_fL:
    input:
        code="python/fitting/RapidSim_shapes_for_fL.py"
    output: an.loc.ROOT+"output/roofitresults/MC_{mode}.root" #fit results
    shell: "python {input.code} --Mode {wildcards.mode}"

##################################
### m(D* Ds) fit to measure fL ###
##################################

fL_fit_infiles = []
#Data files
for y in config["Year"]:
    for m in config["Mag"]:
        fL_fit_infiles.append(an.loc.WGEOS+f"/tuples/data/Bd2DstDs/{y}_{m}/Total_Bd2DstDs_Vars.root")
#Shape params from fits to RapidSim samples
for mode in part_reco_modes:
    fL_fit_infiles.append(an.loc.ROOT+f"output/roofitresults/MC_{mode}.root")
#Feed-down fractions to Gaussian constrain background levels
fL_fit_infiles.append(an.loc.ROOT+"output/json/feed_down.json")
#Shape pars for B0 -> D* Ds PDF
fL_fit_infiles.append(an.loc.ROOT+"output/json/MC_Bd2DstDs.json")

rule fL_fit:
    input:
        code="python/fitting/m_DstDs_for_fL.py",
        data=fL_fit_infiles
    output: out_list_fL_fit
    shell: "python {input.code} --Syst none --Toy N"

#######################################################################
### Generate condor submission script for unbinned angular fit toys ###
#######################################################################

rule make_toy_m_DstDs_for_fL_submit_script_condor:
    input:
        code="condor/make_submit_scripts_toys_m_DstDs_for_fL.py"
    output: out_list_make_toy_m_DstDs_for_fL_submit_script_condor
    shell: "python {input.code}"

#To run toys on condor batch, do:
#sh condor/submit_toys_m_DstDs_for_fL.sh

###########################
### Analyse fL fit toys ###
###########################

#Get list of all toy files in output folder
m_DstDs_for_fL_toy_results = glob.glob(an.loc.WGEOS+"Toys/m_DstDs_for_fL/*.root")

rule analyse_toys_m_DstDs_for_fL:
    input:
        code="python/toys/m_DstDs_for_fL.py",
        data=m_DstDs_for_fL_toy_results
    output: out_list_m_DstDs_for_fL_toys
    shell: "python {input.code}"

#######################################################################
### Generate condor submission script for unbinned angular fit syst ###
#######################################################################

rule make_syst_m_DstDs_for_fL_submit_script_condor:
    input:
        code="condor/make_submit_scripts_syst_m_DstDs_for_fL.py"
    output: out_list_make_syst_m_DstDs_for_fL_submit_script_condor
    shell: "python {input.code}"

#To run systematics on condor batch, do:
#sh condor/submit_syst_m_DstDs_for_fL.sh

##################################
### Analyse fL fit systematics ###
##################################

#Get list of all syst files in output folder
m_DstDs_for_fL_syst_results = glob.glob(an.loc.WGEOS+"Systematics/m_DstDs_for_fL/*.root")

rule analyse_syst_m_DstDs_for_fL:
    input:
        code="python/systematics/m_DstDs_for_fL.py",
        data=m_DstDs_for_fL_syst_results
    output: out_list_m_DstDs_for_fL_syst
    shell: "python {input.code}"

################################################
### Make table of feed-down fraction results ###
################################################

feed_down_files = []
feed_down_files.append(an.loc.ROOT+"output/json/feed_down.json")
feed_down_files.append(an.loc.ROOT+"output/roofitresults/m_DstDs_for_fL.root")

rule make_feed_down_table:
    input:
        code="python/feed_down/make_feed_down_table.py",
        data=feed_down_files
    output: out_list_feed_down_table
    shell: "python {input.code}"

####################################################################
### Calculate effs for B0 -> D* Ds and B0 -> D* Ds* for BF ratio ###
####################################################################

eff_in_files = []
for y in ["2015","2016"]:
    for mag in ["Up","Down"]:
        for mode in ["Bd2DstDs","Bd2DstDsst","Bd2DstDsst_ReDecay"]:
            eff_in_files.append(an.loc.WGEOS+f"/tuples/MC/{mode}/{y}_{mag}/Total_Bd2DstDs_Vars.root")
            eff_in_files.append(an.loc.WGEOS+f"/tuples/MC/{mode}/{y}_{mag}/Total.root")

rule calc_effs:
    input:
        code="python/efficiencies/effs.py",
        data=eff_in_files
    output: out_list_effs
    shell: "python {input.code}"

##########################
### Make table of effs ###
##########################

rule eff_table:
    input:
        code="python/efficiencies/eff_table.py",
        data=eff_in_files
    output: out_list_eff_table
    shell: "python {input.code}"


##########################
### Calculate BF ratio ###
##########################

BF_in_files = []
#efficiency values
BF_in_files.append(an.loc.ROOT+"output/json/effs.json")
#yield values from m(D* Ds) fit
BF_in_files.append(an.loc.ROOT+"output/json/m_DstDs_for_fL.json")
#systematics on the yields
BF_in_files.append(an.loc.ROOT+"output/json/m_DstDs_for_fL_syst.json")

rule calc_BF_ratio:
    input:
        code="python/calc_BF_ratio/calc_BF_ratio.py",
        data=BF_in_files
    output: out_list_BF_ratio
    shell: "python {input.code}"

############################################
### Make latex results for ANA and PAPER ###
############################################

final_results_in_files = []
final_results_in_files.append(an.loc.ROOT+'output/json/m_DstDs_for_fL.json')
final_results_in_files.append(an.loc.ROOT+'output/json/m_DstDs_for_fL_syst.json')
final_results_in_files.append(an.loc.ROOT+'output/json/effs.json')
final_results_in_files.append(an.loc.ROOT+'output/json/BF_ratio.json')
final_results_in_files.append(an.loc.ROOT+'output/json/m_DstDsst_for_sWeights.json')
final_results_in_files.append(an.loc.ROOT+'output/json/unbinned_angular_fit_results.json')
final_results_in_files.append(an.loc.ROOT+'output/json/unbinned_angular_fit_syst.json')
final_results_in_files.append(an.loc.ROOT+'output/json/unbinned_angular_fit_pulls_boost_N.json')
final_results_in_files.append(an.loc.ROOT+'output/json/unbinned_angular_fit_MC_results.json')

rule final_results:
    input:
        code="python/tex_results/tex_results.py",
        data=final_results_in_files
    output: out_list_final_results
    shell: "python {input.code}"

#############################################
### Plot unbinned fit results as true PDF ###
#############################################

plot_model_in_files = []
plot_model_in_files.append(an.loc.ROOT+"output/json/unbinned_angular_fit_results.json")
plot_model_in_files.append(an.loc.ROOT+"output/json/unbinned_angular_fit_pulls_boost_N.json")
plot_model_in_files.append(an.loc.ROOT+"output/json/m_DstDs_for_fL.json")

rule plot_model:
    input:
        code="python/plot_model/plot_model.py",
        data=plot_model_in_files
    output: out_list_plot_model
    shell: "python {input.code}"

######################################
### Table of pulls for the results ###
######################################

pull_table_in_files = []
pull_table_in_files.append(an.loc.ROOT+"output/json/m_DstDs_for_fL_pulls.json")
pull_table_in_files.append(an.loc.ROOT+"output/json/unbinned_angular_fit_pulls_boost_N.json")

rule pull_table:
    input:
        code="python/toys/pulls_table.py",
        data=pull_table_in_files
    output: out_list_pull_table
    shell: "python {input.code}"

##################################
### Fit MC B0 -> D* Ds* B peak ###
##################################

MC_Bd2DstDsst_fullreco_fit_infiles = []
for y in config["Year"]:
    for mag in config["Mag"]:
        for mode in ["Bd2DstDsst","Bd2DstDsst_ReDecay"]:
            MC_Bd2DstDsst_fullreco_fit_infiles.append(an.loc.WGEOS+f"/tuples/MC/{mode}/{y}_{mag}/Total_Bd2DstDsst_Vars.root")

rule MC_Bd2DstDsst_fullreco_fit:
    input:
        code="python/fitting/MC_Bd2DstDsst_fullreco.py",
        data=MC_Bd2DstDsst_fullreco_fit_infiles
    output: out_list_MC_Bd2DstDsst_fullreco_fit
    shell: "python {input.code}"

#################################
### Fit MC B0 -> D* Ds B peak ###
#################################

MC_Bd2DstDs_fit_infiles = []
for y in config["Year"]:
    for mag in config["Mag"]:
        for mode in ["Bd2DstDs"]:
            MC_Bd2DstDs_fit_infiles.append(an.loc.WGEOS+f"/tuples/MC/{mode}/{y}_{mag}/Total_Bd2DstDs_Vars.root")

rule MC_Bd2DstDs_fit:
    input:
        code="python/fitting/MC_Bd2DstDs.py",
        data=MC_Bd2DstDs_fit_infiles
    output: out_list_MC_Bd2DstDs_fit
    shell: "python {input.code}"
