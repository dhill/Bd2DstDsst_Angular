import numpy as np
import sys, os
import json
import os.path
import analysis as an
from calc_model import calc_model
from os import path
from root_pandas import to_root, read_root
import pandas as pd
import matplotlib
from collections import OrderedDict
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import cmath
from uncertainties import *

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

	n_points = 100
	x = np.linspace(-1,1,n_points)
	y = np.linspace(-1,1,n_points)
	z = np.linspace(-np.pi,np.pi,n_points)

	#Helicity amplitudes from MC model (world average fL)
	H0_amp = 0.7204
	Hp_amp = 0.4904
	Hm_amp = 0.4904
	H0_phi = 0.0
	Hp_phi = 0.0
	Hm_phi = 0.0

	f_MC = calc_model(H0_amp,H0_phi,Hp_amp,Hp_phi,Hm_amp,Hm_phi,x,y,z)
	x_int_MC = np.sum(f_MC,axis=(1,2))
	x_int_MC = x_int_MC / np.sum(x_int_MC)
	y_int_MC = np.sum(f_MC,axis=(0,2))
	y_int_MC = y_int_MC / np.sum(y_int_MC)
	z_int_MC = np.sum(f_MC,axis=(0,1))
	z_int_MC = z_int_MC / np.sum(z_int_MC)

	#Values from data fit
	with open(an.loc.ROOT+"output/json/unbinned_angular_fit_results.json","r") as f:
		results = json.load(f)

	#Pull corrections
	with open(an.loc.ROOT+"output/json/unbinned_angular_fit_pulls_boost_N.json","r") as f:
		pulls = json.load(f)

	#fL result
	with open(an.loc.ROOT+"output/json/m_DstDs_for_fL.json","r") as f:
		fL_result = json.load(f)

	H0_amp = fL_result["fL"][0]**0.5
	Hm_amp = results["Hm_amp"][0] - pulls["Hm_amp"]["mu"][0]*results["Hm_amp"][1]
	Hp_amp = np.sqrt(1. - H0_amp**2 - Hm_amp**2)
	H0_phi = 0.
	Hm_phi = results["Hm_phi"][0]
	Hp_phi = results["Hp_phi"][0]

	#Data result
	f_data = calc_model(H0_amp,H0_phi,Hp_amp,Hp_phi,Hm_amp,Hm_phi,x,y,z)
	x_int_data = np.sum(f_data,axis=(1,2))
	x_int_data = x_int_data / np.sum(x_int_data)
	y_int_data = np.sum(f_data,axis=(0,2))
	y_int_data = y_int_data / np.sum(y_int_data)
	z_int_data = np.sum(f_data,axis=(0,1))
	z_int_data = z_int_data / np.sum(z_int_data)

	#costheta_X
	fig, ax = plt.subplots(figsize=(8,8))
	#MC model
	plt.plot(x,x_int_MC,label="Model with W.A. $f_L$",color='crimson')
	#Default data fit result
	plt.plot(x,x_int_data,label="Results of this analysis",color='navy')
	plt.xlabel("$\cos(\\theta_X)$",fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(np.min(x),np.max(x))
	ymin, ymax = plt.ylim()
	plt.ylim(0,1.1*ymax)
	plt.legend(fontsize=20,loc='lower left')
	#plt.show()
	plt.tight_layout()
	fig.savefig(an.loc.ROOT+"output/plots/costheta_X_results_vs_WA.pdf")


	#costheta_D
	fig, ax = plt.subplots(figsize=(8,8))
	#MC model
	plt.plot(y,y_int_MC,label="Model with W.A. $f_L$",color='crimson')
	#Default data fit result
	plt.plot(y,y_int_data,label="Results of this analysis",color='navy')

	plt.xlabel("$\cos(\\theta_D)$",fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(np.min(y),np.max(y))
	ymin, ymax = plt.ylim()
	plt.ylim(0,1.1*ymax)
	#plt.show()
	plt.tight_layout()
	fig.savefig(an.loc.ROOT+"output/plots/costheta_D_results_vs_WA.pdf")

	#chi
	fig, ax = plt.subplots(figsize=(8,8))
	#MC model
	plt.plot(z,z_int_MC,label="Model with W.A. $f_L$",color='crimson')
	#Default data fit result
	plt.plot(z,z_int_data,label="Results of this analysis",color='navy')

	plt.xlabel("$\\chi$ [rad]",fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(np.min(z),np.max(z))
	ymin, ymax = plt.ylim()
	plt.ylim(0,1.1*ymax)
	#plt.show()
	plt.tight_layout()
	fig.savefig(an.loc.ROOT+"output/plots/chi_results_vs_WA.pdf")


if __name__ == '__main__':
    main()
