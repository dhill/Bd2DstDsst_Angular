import os, sys

os.environ["ROOT_INCLUDE_PATH"] = "$ANAROOT/Meerkat/inc/"

from ROOT import gSystem, gStyle, RooRealVar, gApplication, TChain, AddressOf

gSystem.Load("$ANAROOT/Meerkat/lib/libMeerkat.so")

from ROOT import OneDimPhaseSpace, FormulaDensity, CombinedPhaseSpace
from ROOT import Logger
from ROOT import BinnedKernelDensity, BinnedDensity, AdaptiveKernelDensity
from ROOT import TFile, TNtuple, TCanvas, TH1F, TH2F, TH3F, TText, vector
from ROOT import RooFit, RooDataSet, RooDataHist, RooHistPdf, RooRealVar, RooFormulaVar, RooArgSet, RooArgList, RooExponential, RooAddPdf, RooProdPdf, RooAbsReal
from array import array
import math
import analysis as an
import numpy as np

def main():

    mode = "Bd2DstDsst"
    angle_type = "VV"
    geom = "All"

    vars = ['costheta_D_true',
            f'costheta_X_{angle_type}_true',
            f'chi_{angle_type}_true'
            ]

    #Read in 4pi RapidSim sample to create density histogram of generated model
    #MC generated with fL from m(D* Ds) analysis, no acceptance cuts or cuts on decay products
    path = an.loc.WGEOS + f"/RapidSim_tuples/{mode}_With_Part_Reco_fL/Total_{geom}_Vars.root"

    tree = TChain("DecayTree")

    tree.Add(path)

    tree.SetBranchStatus("*",0)

    for v in vars:
	       tree.SetBranchStatus(v,1)

    angles = ['costheta_D', f'costheta_X_{angle_type}', f'chi_{angle_type}']

    angle_min = {'costheta_D': -1.,
                 f'costheta_X_{angle_type}': -1.,
                 f'chi_{angle_type}': -math.pi
                 }

    angle_max = {'costheta_D': 1.,
                 f'costheta_X_{angle_type}': 1.,
                 f'chi_{angle_type}': math.pi
                 }

    title = {'costheta_D': "$cos(\\theta_{D})$",
             f'costheta_X_{angle_type}': "$cos(\\theta_{X})$",
             f'chi_{angle_type}': "$\\chi$ [rad]"
             }

    #Define phase space for q2
    phsp_x = OneDimPhaseSpace("Phsp_x", angle_min['costheta_D'], angle_max['costheta_D'])
    #Define phase space for slow pion energy
    phsp_y = OneDimPhaseSpace("Phsp_y", angle_min[f'costheta_X_{angle_type}'], angle_max[f'costheta_X_{angle_type}'])
    #Define phase space for double charm BDT
    phsp_z = OneDimPhaseSpace("Phsp_z", angle_min[f'chi_{angle_type}'], angle_max[f'chi_{angle_type}'])

    #Combined 3D angular phase space
    phsp = CombinedPhaseSpace("Phsp3D",phsp_x, phsp_y, phsp_z)

    #Kernel width (in terms of number of bins)
    k = 2.
    n_bins = 30

    if(mode=="Bd2DstDsst"):
	       truepdf = 0

    kde = BinnedKernelDensity("KernelPDF",
                              phsp,   # Phase space
                              tree, # Input ntuple
                              angles[0]+"_true", angles[1]+"_true", angles[2]+"_true",     # Variable to use
                              #weight_var=None, #Weight variable
                              int(n_bins), int(n_bins), int(n_bins),
                              float(k/n_bins)*float(angle_max['costheta_D']-angle_min['costheta_D']), float(k/n_bins)*float(angle_max[f'costheta_X_{angle_type}']-angle_min[f'costheta_X_{angle_type}']), float(k/n_bins)*float(angle_max[f'chi_{angle_type}']-angle_min[f'chi_{angle_type}']),
                              truepdf,       # Approximation PDF (0 sfor flat approximation)
                              10000000   # Sample size for MC convolution (0 for binned convolution)
                              )

    #Fill ROOT histogram with the values of the full physics model from akde
    hist_full = TH3F("hist_full","",n_bins,angle_min['costheta_D'],angle_max['costheta_D']+1e-10,
                     n_bins,angle_min[f'costheta_X_{angle_type}'],angle_max[f'costheta_X_{angle_type}']+1e-10,
                     n_bins,angle_min[f'chi_{angle_type}'],angle_max[f'chi_{angle_type}']+1e-10)

    # Fill histogram with the result of kernel density estimation
    kde.project(hist_full)
    hist_full.Sumw2()

    #3D grid over physical angular range
    xx_phys, yy_phys, zz_phys = np.mgrid[angle_min['costheta_D']:angle_max['costheta_D']:complex(0,n_bins),
                                         angle_min[f'costheta_X_{angle_type}']:angle_max[f'costheta_X_{angle_type}']:complex(0,n_bins),
                                         angle_min[f'chi_{angle_type}']:angle_max[f'chi_{angle_type}']:complex(0,n_bins)]

    #Dictionary of functions for each angular term

    #Helicity amplitude version
    if(mode=="Bd2DstDsst"):
        funcs = {"1": xx_phys**2 * np.sin(np.arccos(yy_phys))**2 + 0.*zz_phys,
                 "2": np.sin(np.arccos(xx_phys))**2 * (1.0 + yy_phys**2) + 0.*zz_phys,
                 "3": np.sin(np.arccos(xx_phys))**2 * np.sin(np.arccos(yy_phys))**2 * np.cos(2*zz_phys),
                 "4": np.sin(np.arccos(xx_phys))**2 * np.sin(np.arccos(yy_phys))**2 * np.sin(2*zz_phys),
                 "5": np.sin(2*np.arccos(xx_phys)) * np.sin(2*np.arccos(yy_phys)) * np.cos(zz_phys),
                 "6": np.sin(2*np.arccos(xx_phys)) * np.sin(2*np.arccos(yy_phys)) * np.sin(zz_phys)
                 }

    #Histogram containing the true angular distribution for each term
    hist_true = {}

    #Histogram containing the ratio of single term / full model
    hist_ratio = {}

    coeffs = list(funcs.keys())

    #Loop over angular terms
    for c in coeffs:
        print("Running over coefficient %s" % c)

        #Tiny bit extra on upper bin edge to catch everything
        hist_true[c] = TH3F("hist_true_%s" % c,"",n_bins,angle_min['costheta_D'],angle_max['costheta_D']+1e-10,
                            n_bins,angle_min[f'costheta_X_{angle_type}'],angle_max[f'costheta_X_{angle_type}']+1e-10,
                            n_bins,angle_min[f'chi_{angle_type}'],angle_max[f'chi_{angle_type}']+1e-10)

        #Loop over theta_X
        for i in range(0,n_bins):
            #Loop over theta_L
            for j in range(0,n_bins):
                #Loop over chi
                for k in range(0,n_bins):

                    #theta_X location
                    x_val = xx_phys[i,0,0]
                    #theta_L location
                    y_val = yy_phys[0,j,0]
                    #chi location
                    z_val = zz_phys[0,0,k]

                    hist_x_true = hist_true[c].GetXaxis()
                    bin_x_true = hist_x_true.FindBin(x_val)

                    hist_y_true = hist_true[c].GetYaxis()
                    bin_y_true = hist_y_true.FindBin(y_val)

                    hist_z_true = hist_true[c].GetZaxis()
                    bin_z_true = hist_z_true.FindBin(z_val)

                    #Fill angular histogram with the single angular function value
                    hist_true[c].SetBinContent(bin_x_true,bin_y_true,bin_z_true,funcs[c][i,j,k])


    #Make ratio histograms from hist_true / hist_full ratio
    for c in coeffs:
        hist_true[c].Sumw2()
        hist_ratio[c] = hist_true[c].Clone("hist_ratio_%s" % c)
        hist_ratio[c].Divide(hist_full)

    #Write the histograms to file for use in fit
    out_file = TFile(f"$ANAROOT/output/hists/weight_hists_{mode}.root", "RECREATE")
    out_file.cd()
    hist_full.Write()
    for c in coeffs:
        hist_true[c].Write()
        hist_ratio[c].Write()
    out_file.Write()
    out_file.Close()

if __name__ == '__main__':
    main()
