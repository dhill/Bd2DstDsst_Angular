#!/bin/bash

shopt -s expand_aliases

## Define root variable
export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

set +u

source ~/miniconda/etc/profile.d/conda.sh
conda activate ana_env

cd $ANAROOT

export PYTHONPATH=$ANAROOT:$ANAROOT/python:$PYTHONPATH

# Locations

export SCRIPTS=$ANAROOT/python/scripts
export PLOTS=$ANAROOT/output/plots
export TABLES=$ANAROOT/output/tables
export LOGS=$ANAROOT/output/logs

## Aliases

alias goAna='cd $ANAROOT'

## Variables and aliases for snakemake

alias snakeclean='rm $(snakemake --summary | tail -n+2 | cut -f1)'
alias stripeff='lb-run LHCbDirac/prod dirac-bookkeeping-rejection-stats -BK '
