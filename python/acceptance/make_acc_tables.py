import sys,os
import json
import analysis as an

def main():

    f = open(an.loc.ROOT+'output/tex/acc_pars.tex','w')
    f.write("\\begin{table}[!h] \n")
    f.write("\\begin{center} \n")
    f.write("\\begin{tabular}{c|c} \n")
    f.write("Acceptance parameter & Value \\\\ \\hline \n")

    angles = ["costheta_X_D","chi"]

    for a in angles:

        with open(an.loc.ROOT+'output/json/acc_pars_%s.json' % a,'r') as file:
            acc_dict = json.load(file)

        for p in acc_dict:

            p_name = str(p)
            for i in range(0,7):
                p_name = p_name.replace("a%s" % i,"$a_%s$" % i)
            p_name = p_name.replace("costheta_D","$\\cos(\\theta_D)$")
            p_name = p_name.replace("costheta_X_VV","$\\cos(\\theta_X)$")
            p_name = p_name.replace("chi_VV","$\\chi$")
            p_name = p_name.replace("_$a"," $a")
            phan = ""
            val = acc_dict[p][0]
            if(val>0):
                phan = "\\phantom{-}"
            f.write("%s & $%s%.3f \\pm %.3f$ \\\\ \n" % (p_name, phan, acc_dict[p][0], acc_dict[p][1]))

    f.write("\\end{tabular} \n")
    f.write("\\caption{Acceptance function parameters for each decay angle.} \n")
    f.write("\\label{tab:acc_pars} \n")
    f.write("\\end{center} \n")
    f.write("\\end{table}")





if __name__ == '__main__':
    main()
