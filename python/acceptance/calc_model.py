import numpy as np

def calc_model(H0_amp,H0_phi,Hp_amp,Hp_phi,Hm_amp,Hm_phi,costheta_D,costheta_X,chi):
	H0_re = H0_amp * np.cos(H0_phi)
	H0_im = H0_amp * np.sin(H0_phi)
	Hp_re = Hp_amp * np.cos(Hp_phi)
	Hp_im = Hp_amp * np.sin(Hp_phi)
	Hm_re = Hm_amp * np.cos(Hm_phi)
	Hm_im = Hm_amp * np.sin(Hm_phi)

	#complex amplitudes
	h0 = complex(H0_re,H0_im)
	hp = complex(Hp_re,Hp_im)
	hm = complex(Hm_re,Hm_im)
	h0st = complex(H0_re,-H0_im)
	hpst = complex(Hp_re,-Hp_im)
	hmst = complex(Hm_re,-Hm_im)
	hphmst = hp*hmst
	hph0st = hp*h0st
	hmh0st = hm*h0st
	A = hph0st + hmh0st
	B = hph0st - hmh0st

	#3D decay rate
	#costheta_D, costheta_X, chi = np.meshgrid(x, y, z)

	f = 0.25*np.sin(np.arccos(costheta_D))**2 * (1 + costheta_X**2)*(Hp_amp**2 + Hm_amp**2) \
		+ costheta_D**2*np.sin(np.arccos(costheta_X))**2*H0_amp**2 \
		- 0.5*np.sin(np.arccos(costheta_D))**2*np.sin(np.arccos(costheta_X))**2*np.cos(2*chi)*hphmst.real \
		+ 0.5*np.sin(np.arccos(costheta_D))**2*np.sin(np.arccos(costheta_X))**2*np.sin(2*chi)*hphmst.imag \
		- 0.25*np.sin(2*np.arccos(costheta_D))*np.sin(2*np.arccos(costheta_X))*np.cos(chi)*A.real \
		+ 0.25*np.sin(2*np.arccos(costheta_D))*np.sin(2*np.arccos(costheta_X))*np.sin(chi)*B.imag
	f = f * (9./8.)

	return f
