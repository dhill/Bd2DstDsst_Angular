import glob, os, sys
import numpy as np
import os.path
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib import rc
from scipy import stats
from scipy.stats import norm
from scipy.optimize import curve_fit
import json
from collections import OrderedDict
from root_pandas import read_root
import pandas as pd
from scipy import interpolate
import pickle
import random
from calc_model import calc_model
import analysis as an
from iminuit import Minuit
from iminuit.cost import LeastSquares

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

    #RapidSim 4pi sample with no cuts or acceptance, generated with same fL as in data according to m(D* Ds) fit
    path_MC = an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_All_Vars_Weights.root"
    df_MC = read_root(path_MC,"DecayTree")

    #sWeighted data sample
    path_data = an.loc.ROOT+"output/rootfiles/Bd2DstDsst_sWeights.root"
    df_data = read_root(path_data,"RooTreeDataStore_data_data")

    angles = {"costheta_D": ["$\\cos(\\theta_D)$",-1,1],
              "costheta_X_VV": ["$\\cos(\\theta_X)$",-1,1]
              }

    #Dict containing the fitted acceptance pars
    acc_pars = {}

    bins = 15

    for a in angles:

        #Histogram of data
        counts_reco, bin_edges = np.histogram(df_data[a+"_reco"], bins, range=(angles[a][1],angles[a][2]),weights=df_data["n_sig_sw"])
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
        errs_reco = np.sqrt(counts_reco)
        #Normalise
        errs_reco = errs_reco.astype(float)/np.sum(counts_reco)
        counts_reco = counts_reco.astype(float)/np.sum(counts_reco)

        #Histogram of RapidSim MC in reco angles
        counts_true, bin_edges = np.histogram(df_MC[a+"_true"], bins, range=(angles[a][1],angles[a][2]))
        errs_true = np.sqrt(counts_true)
        #Normalise
        errs_true = errs_true.astype(float)/np.sum(counts_true)
        counts_true = counts_true.astype(float)/np.sum(counts_true)

        fig,ax = plt.subplots(figsize=(8,8))
        plt.errorbar(bin_centres,counts_true,yerr=errs_true,ls=None,color='purple',fmt='o',markersize=5,label="Truth-level RapidSim",alpha=0.6)
        plt.errorbar(bin_centres,counts_reco,yerr=errs_reco,ls=None,color='hotpink',fmt='o',markersize=5,label="Data",alpha=0.6)

        plt.legend(fontsize=20,loc='upper right')
        plt.xlabel(angles[a][0],horizontalalignment='right',x=1.0,labelpad=5,fontsize=30)
        ax.tick_params(axis='both', which='major', labelsize=25)
        plt.xlim(angles[a][1],angles[a][2])
        ymin, ymax = plt.ylim()
        plt.ylim(0.0,1.5*ymax)
        plt.tight_layout()
        #plt.show()
        fig.savefig(an.loc.ROOT+"output/plots/Data_RapidSim_True_MC_compare_%s.pdf" % a)

        #Make ratio and use it to correct the costheta_X and costheta_D distributions in MC
        ratio = counts_reco / counts_true
        errs_ratio = ratio * np.sqrt( (errs_reco/counts_reco)**2 + (errs_true/counts_true)**2)

        def pdf(x, a0, a1, a2, a3, a4, a5, a6):
            return a0 + a1*x + a2*x**2 + a3*x**3 + a4*x**4 + a5*x**5 + a6*x**6
        x_vals = np.linspace(angles[a][1],angles[a][2],1000)
        least_squares = LeastSquares(bin_centres, ratio, errs_ratio, pdf)

        m = Minuit(least_squares, a0=1., a1=0., a2=0., a3=0., a4=0., a5=0., a6=0.)

        m.migrad() # finds minimum of least_squares function
        m.hesse()  # computes errors
        cov_m = m.np_matrix()
        print(cov_m)

        corr_m = m.latex_matrix()
        print(corr_m)

        corr_file = an.loc.ROOT+"output/tex/acc_pars_corr_m_%s.tex" % a
        with open(corr_file, 'w') as f:
            print(corr_m, file=f)

        #Save covariance matrix for use in varying the chi corr parameters in a multivariate gaussian
        np.save(an.loc.ROOT+"/output/json/acc_pars_cov_%s.npy" % a,cov_m)

        for p in m.parameters:
            print("{} = {:.5f} +/- {:.5f}".format(p, m.values[p], m.errors[p]))
            acc_pars["%s_%s" % (a,p)] = [m.values[p], m.errors[p]]

        fig, ax = plt.subplots(figsize=(8,8))
        plt.errorbar(bin_centres,ratio,yerr=errs_ratio,ls=None,color='k',fmt='o',markersize=5,alpha=0.6)
        plt.plot(x_vals, pdf(x_vals, *m.values.values()), color='crimson',label='Polynomial fit')
        plt.xlabel(angles[a][0],horizontalalignment='right',x=1.0,labelpad=5,fontsize=30)
        plt.ylabel("Data / Truth-level RapidSim",horizontalalignment='right',y=1.0,labelpad=5,fontsize=30)
        plt.axhline(y=1.0,color='gray',linestyle='--',alpha=0.4)
        ax.tick_params(axis='both', which='major', labelsize=25)
        plt.legend(fontsize=20)
        plt.xlim(angles[a][1],angles[a][2])
        ylow, yhigh = plt.ylim()
        plt.tight_layout()
        #plt.show()

        fig.savefig(an.loc.ROOT+"output/plots/Data_RapidSim_True_MC_ratio_%s.pdf" % a)

    #Save acceptance parameters
    with open(an.loc.ROOT+'output/json/acc_pars_costheta_X_D.json', 'w') as fp:
        json.dump(acc_pars, fp, sort_keys=True, indent=4)

    acc_pars_syst = {}

    #Model from m(D* Ds) fit, same one used in RapidSim sample generation
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL.json','r') as f:
        fL_dict = json.load(f)
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL_syst.json','r') as f:
        fL_syst_dict = json.load(f)
    f_L = fL_dict["fL"][0]
    f_L_sigma = np.sqrt(fL_dict["fL"][1]**2 + fL_syst_dict["fL_tot"]**2)
    H0_amp = np.sqrt(f_L)
    H0_phi = 0.0
    Hp_amp = np.sqrt((1 - f_L)/2)
    Hp_phi = 0.0
    Hm_amp = np.sqrt((1 - f_L)/2)
    Hm_phi = 0.0

    df_MC["gen_w"] = calc_model(H0_amp,H0_phi,Hp_amp,Hp_phi,Hm_amp,Hm_phi,df_MC["costheta_D_true"],df_MC["costheta_X_VV_true"],df_MC["chi_VV_true"])

    #Run alternative acceptance fits with fL varied systematically
    for i in range(0,1000):
        print(i)

        #Model with systematically varied fL
        np.random.seed(i)
        f_L += np.random.normal(0.,f_L_sigma,1)
        H0_amp = np.sqrt(f_L)
        Hp_amp = np.sqrt((1 - f_L)/2)
        Hm_amp = np.sqrt((1 - f_L)/2)

        df_MC["syst_w"] = calc_model(H0_amp,H0_phi,Hp_amp,Hp_phi,Hm_amp,Hm_phi,df_MC["costheta_D_true"],df_MC["costheta_X_VV_true"],df_MC["chi_VV_true"])

        df_MC["model_w"] = df_MC["syst_w"] / df_MC["gen_w"]

        for a in angles:

            #Histogram of data
            counts_reco, bin_edges = np.histogram(df_data[a+"_reco"], bins, range=(angles[a][1],angles[a][2]),weights=df_data["n_sig_sw"])
            bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
            errs_reco = np.sqrt(counts_reco)
            #Normalise
            errs_reco = errs_reco.astype(float)/np.sum(counts_reco)
            counts_reco = counts_reco.astype(float)/np.sum(counts_reco)

            #Histogram of RapidSim MC in reco angles
            counts_true, bin_edges = np.histogram(df_MC[a+"_true"], bins, range=(angles[a][1],angles[a][2]),weights=df_MC["model_w"])
            errs_true = np.sqrt(counts_true)
            #Normalise
            errs_true = errs_true.astype(float)/np.sum(counts_true)
            counts_true = counts_true.astype(float)/np.sum(counts_true)

            #Make ratio and use it to correct the costheta_X and costheta_D distributions in MC
            ratio = counts_reco / counts_true
            errs_ratio = ratio * np.sqrt( (errs_reco/counts_reco)**2 + (errs_true/counts_true)**2)

            def pdf(x, a0, a1, a2, a3, a4, a5, a6):
                return a0 + a1*x + a2*x**2 + a3*x**3 + a4*x**4 + a5*x**5 + a6*x**6
            x_vals = np.linspace(angles[a][1],angles[a][2],1000)
            least_squares = LeastSquares(bin_centres, ratio, errs_ratio, pdf)

            m = Minuit(least_squares, a0=1., a1=0., a2=0., a3=0., a4=0., a5=0., a6=0.)

            m.migrad() # finds minimum of least_squares function
            m.hesse()  # computes errors

            for p in m.parameters:
                #print("{} = {:.5f} +/- {:.5f}".format(p, m.values[p], m.errors[p]))
                acc_pars_syst["%s_%s_%s" % (a,p,i)] = [m.values[p], m.errors[p]]

    #Save systematic acceptance parameters
    with open(an.loc.ROOT+'output/json/acc_pars_costheta_X_D_syst.json', 'w') as fp:
        json.dump(acc_pars_syst, fp, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()
