import os, sys, argparse
import pandas as pd
import numpy as np
from root_pandas import to_root, read_root
import hepvector
from hepvector import LorentzVector, Vector3D
from tqdm import tqdm
import random
import analysis as an
from ROOT import TChain

#B momentum
def calc_B_P(df,c,X):

    #D/D* momentum vector
    p_C = Vector3D(df["%s_PX" % c],
                   df["%s_PY" % c],
                   df["%s_PZ" % c])

    #Other charm meson flight vector
    u_X = Vector3D(df["%s_ENDVERTEX_X" % X] - df["B_ENDVERTEX_X"],
                   df["%s_ENDVERTEX_Y" % X] - df["B_ENDVERTEX_Y"],
                   df["%s_ENDVERTEX_Z" % X] - df["B_ENDVERTEX_Z"])

    u_X_unit = u_X.unit

    #B flight vector
    u_B = Vector3D(df["B_ENDVERTEX_X"] - df["B_OWNPV_X"],
                   df["B_ENDVERTEX_Y"] - df["B_OWNPV_Y"],
                   df["B_ENDVERTEX_Z"] - df["B_OWNPV_Z"])

    u_B_unit = u_B.unit

    #Numerator vector
    num_vec = p_C.cross(u_X_unit)
    num_vec_mag = num_vec.mag

    #Denominator vector
    den_vec = u_B_unit.cross(u_X_unit)
    den_vec_mag = den_vec.mag

    p_B = num_vec_mag / den_vec_mag

    return p_B



#Invariant mass of a particle pair
def calc_mXY(df,x, y):

    v_X = LorentzVector(df["B_D0constDsconstPVconst_%s_PX" % x].values,
                        df["B_D0constDsconstPVconst_%s_PY" % x].values,
                        df["B_D0constDsconstPVconst_%s_PZ" % x].values,
                        df["B_D0constDsconstPVconst_%s_PE" % x].values)

    v_Y = LorentzVector(df["B_D0constDsconstPVconst_%s_PX" % y].values,
                        df["B_D0constDsconstPVconst_%s_PY" % y].values,
                        df["B_D0constDsconstPVconst_%s_PZ" % y].values,
                        df["B_D0constDsconstPVconst_%s_PE" % y].values)

    v_comb = v_X + v_Y

    m = v_comb.mag

    return m

#Calculate decay q2
def calc_q2(df,c,type,isFullReco):
    b_vars = []
    c_vars = []

    if(type=="reco"):
        #Was the B momentum estimated (for a part reco decay)
        if(isFullReco==False):
            suf = "_reco"
            b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_PE%s" % suf]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]
        else:
            b_vars = ["B_PX","B_PY","B_PZ","B_PE"]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]

    elif(type=="true"):
        b_vars = ["B_TRUEP_X","B_TRUEP_Y","B_TRUEP_Z","B_TRUEP_E"]
        c_vars = ["%s_TRUEP_X" % c,"%s_TRUEP_Y" % c,"%s_TRUEP_Z" % c,"%s_TRUEP_E" % c]

    v_B = LorentzVector(df[b_vars[0]].values,
                        df[b_vars[1]].values,
                        df[b_vars[2]].values,
                        df[b_vars[3]].values
                        )
    v_Dst = LorentzVector(df[c_vars[0]].values,
                          df[c_vars[1]].values,
                          df[c_vars[2]].values,
                          df[c_vars[3]].values
                          )

    v_q = v_B - v_Dst

    q2 = v_q.mag2

    #Returns array of q2 values
    return q2

#D*(2010) system angle
def calc_theta_D(df,c,type,isFullReco):
    suf = ""
    b_vars = []
    c_vars = []
    c_dau_vars = []

    if(type=="reco"):
        if(isFullReco==False):
            suf = "_reco"
            b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_PE%s" % suf]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]
        else:
            b_vars = ["B_PX","B_PY","B_PZ","B_PE"]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]
        if(c=="Dst"):
            c_dau_vars = ["D0_PX","D0_PY","D0_PZ","D0_PE"]
        elif(c=="D"):
            c_dau_vars = ["D_K_PX","D_K_PY","D_K_PZ","D_K_PE"]

    elif(type=="true"):
        b_vars = ["B_TRUEP_X","B_TRUEP_Y","B_TRUEP_Z","B_TRUEP_E"]
        c_vars = ["%s_TRUEP_X" % c,"%s_TRUEP_Y" % c,"%s_TRUEP_Z" % c,"%s_TRUEP_E" % c]
        if(c=="Dst"):
            c_dau_vars = ["D0_TRUEP_X","D0_TRUEP_Y","D0_TRUEP_Z","D0_TRUEP_E"]
        elif(c=="D"):
            c_dau_vars = ["D_K_TRUEP_X","D_K_TRUEP_Y","D_K_TRUEP_Z","D_K_TRUEP_E"]

    v_c_dau = LorentzVector(df[c_dau_vars[0]].values,
                            df[c_dau_vars[1]].values,
                            df[c_dau_vars[2]].values,
                            df[c_dau_vars[3]].values)

    v_c = LorentzVector(df[c_vars[0]].values,
                        df[c_vars[1]].values,
                        df[c_vars[2]].values,
                        df[c_vars[3]].values)

    v_B = LorentzVector(df[b_vars[0]].values,
                        df[b_vars[1]].values,
                        df[b_vars[2]].values,
                        df[b_vars[3]].values)

    #Boost to B rest frame first
    B_boost = -(v_B.boostp3)

    v_c_dau.boost(B_boost,inplace=True)
    v_c.boost(B_boost,inplace=True)
    v_B.boost(B_boost,inplace=True)

    #Now boost to X frame
    c_boost = -(v_c.boostp3)
    v_c_dau.boost(c_boost,inplace=True)
    v_B.boost(c_boost,inplace=True)

    vec_c_dau = v_c_dau.p3.unit
    vec_B = v_B.p3.unit

    theta_D = vec_c_dau.angle(vec_B)

    return theta_D

#Angle in the "other" charm system
def calc_theta_X(df,c,type,X,isFullReco):
    b_vars = []
    x_vars = []
    c_vars = []

    if(type=="reco"):
        if(isFullReco==False):
            suf = "_reco"
            b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_PE%s" % suf]
            x_vars = ["%s_PX" % X,"%s_PY" % X,"%s_PZ" % X,"%s_PE" % X]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]
        else:
            b_vars = ["B_PX","B_PY","B_PZ","B_PE"]
            x_vars = ["%s_PX" % X,"%s_PY" % X,"%s_PZ" % X,"%s_PE" % X]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]

    elif(type=="true"):
        b_vars = ["B_TRUEP_X","B_TRUEP_Y","B_TRUEP_Z","B_TRUEP_E"]
        c_vars = ["%s_TRUEP_X" % c,"%s_TRUEP_Y" % c,"%s_TRUEP_Z" % c,"%s_TRUEP_E" % c]
        x_vars = ["%s_TRUEP_X" % X,"%s_TRUEP_Y" % X,"%s_TRUEP_Z" % X,"%s_TRUEP_E" % X]

    v_X = LorentzVector(df[x_vars[0]].values,
                        df[x_vars[1]].values,
                        df[x_vars[2]].values,
                        df[x_vars[3]].values)

    v_c = LorentzVector(df[c_vars[0]].values,
                        df[c_vars[1]].values,
                        df[c_vars[2]].values,
                        df[c_vars[3]].values)

    v_B = LorentzVector(df[b_vars[0]].values,
                        df[b_vars[1]].values,
                        df[b_vars[2]].values,
                        df[b_vars[3]].values)

    v_W = v_B - v_c

    #Boost to B rest frame first
    B_boost = -(v_B.boostp3)

    v_X.boost(B_boost,inplace=True)
    v_B.boost(B_boost,inplace=True)
    v_W.boost(B_boost,inplace=True)

    #Now boost to the W rest frame
    W_boost = -(v_W.boostp3)

    #inplace is important!! Wrong results calculated otherwise
    v_X.boost(W_boost,inplace=True)
    v_B.boost(W_boost,inplace=True)

    vec_X = v_X.p3.unit
    vec_B = v_B.p3.unit

    theta_X = vec_X.angle(vec_B)

    return theta_X

#Angle between the decay planes
def calc_chi(df,c,type,X,isFullReco):
    b_vars = []
    x_vars = []
    c_vars = []
    c_dau_vars = []

    if(type=="reco"):
        if(isFullReco==False):
            suf = "_reco"
            b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_PE%s" % suf]
            x_vars = ["%s_PX" % X,"%s_PY" % X,"%s_PZ" % X,"%s_PE" % X]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]
        else:
            b_vars = ["B_PX","B_PY","B_PZ","B_PE"]
            x_vars = ["%s_PX" % X,"%s_PY" % X,"%s_PZ" % X,"%s_PE" % X]
            c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_PE" % c]
        if(c=="Dst"):
            c_dau_vars = ["D0_PX","D0_PY","D0_PZ","D0_PE"]
        elif(c=="D"):
            c_dau_vars = ["D_K_PX","D_K_PY","D_K_PZ","D_K_PE"]

    elif(type=="true"):
        b_vars = ["B_TRUEP_X","B_TRUEP_Y","B_TRUEP_Z","B_TRUEP_E"]
        x_vars = ["%s_TRUEP_X" % X,"%s_TRUEP_Y" % X,"%s_TRUEP_Z" % X,"%s_TRUEP_E" % X]
        c_vars = ["%s_TRUEP_X" % c,"%s_TRUEP_Y" % c,"%s_TRUEP_Z" % c,"%s_TRUEP_E" % c]
        if(c=="Dst"):
            c_dau_vars = ["D0_TRUEP_X","D0_TRUEP_Y","D0_TRUEP_Z","D0_TRUEP_E"]
        elif(c=="D"):
            c_dau_vars = ["D_K_TRUEP_X","D_K_TRUEP_Y","D_K_TRUEP_Z","D_K_TRUEP_E"]

    v_X = LorentzVector(df[x_vars[0]].values,
                        df[x_vars[1]].values,
                        df[x_vars[2]].values,
                        df[x_vars[3]].values)

    v_c_dau = LorentzVector(df[c_dau_vars[0]].values,
                            df[c_dau_vars[1]].values,
                            df[c_dau_vars[2]].values,
                            df[c_dau_vars[3]].values)

    v_c = LorentzVector(df[c_vars[0]].values,
                        df[c_vars[1]].values,
                        df[c_vars[2]].values,
                        df[c_vars[3]].values)

    v_B = LorentzVector(df[b_vars[0]].values,
                        df[b_vars[1]].values,
                        df[b_vars[2]].values,
                        df[b_vars[3]].values)

    #4-vector of W
    v_W = v_B - v_c

    #Boost everything to the B frame
    B_boost = -(v_B.boostp3)

    v_B.boost(B_boost,inplace=True)
    v_W.boost(B_boost,inplace=True)
    v_X.boost(B_boost,inplace=True)
    v_c.boost(B_boost,inplace=True)
    v_c_dau.boost(B_boost,inplace=True)

    vec_c = v_c.p3.unit
    vec_c_dau = v_c_dau.p3.unit
    vec_c_c_dau = vec_c.cross(vec_c_dau)

    vec_W = v_W.p3.unit
    vec_X = v_X.p3.unit
    vec_WX = vec_W.cross(vec_X)

    vec_x = vec_c_c_dau.unit
    vec_z = vec_c
    vec_y = (vec_z.cross(vec_x)).unit

    C = vec_WX.dot(vec_x)
    S = vec_WX.dot(vec_y)

    chi = np.arctan2(S,C)
    return chi

#Main function
def calc_vars(data_type, year, mag, mode, mode_reco):

    print(f"DataType: {data_type}")
    print(f"Year: {year}")
    print(f"Mag: {mag}")
    print(f"Mode: {mode}")
    print(f"ModeReco: {mode_reco}")

    path = f"{an.loc.WGEOS}/tuples/{data_type}/{mode}"

    in_file = f"{path}/{year}_{mag}/Total"

    out_file = in_file + f"_{mode_reco}_Vars"

    if os.path.exists(out_file+".root"):
        os.remove(out_file+".root")

    #Quickly read input file with ROOT to get list of branch names
    tree = TChain(f"{mode_reco}/DecayTree")
    tree.Add(in_file+".root")
    vars = tree.GetListOfBranches()
    all_vars = []
    for v in vars:
        all_vars.append(v.GetName())

    keep_vars = []
    keep_keys = ["PX",
                 "PY",
                 "PZ",
                 "PE",
                 "PT",
                 "TRUE",
                 "BKGCAT",
                 "ENDVERTEX",
                 "OWNPV",
                 "D0constDsconstPVconst",
                 "TIS",
                 "TOS",
                 "_M",
                 "PIDK",
                 "nTracks",
                 "ID"
                 ]
    for v in all_vars:
        for key in keep_keys:
            if(key in v):
                keep_vars.append(v)
    print("Number of vars to keep: %s" % len(keep_vars))

    for df in tqdm(read_root(in_file+".root", f"{mode_reco}/DecayTree", chunksize=100000, columns=keep_vars)):

        if("Dst" in mode_reco):
            df["Dst_Delta_M"] = df["Dst_M"] - df["D0_M"]

        if("Dsst" in mode_reco):
            df["Dsst_Delta_M"] = df["Dsst_M"] - df["Ds_M"]

        #Calculate D* Ds mass for the D* Ds* case, use to veto D* Ds events
        if(mode_reco=="Bd2DstDsst"):
            df["DstDs_M"] = calc_mXY(df,"Dst","Ds").tolist()

        #Calculate D Ds mass for the D Ds* case, use to veto D Ds events
        if(mode_reco=="Bd2DDsst"):
            df["DDs_M"] = calc_mXY(df,"D","Ds").tolist()

        #Apply cuts
        cuts = ""
        if(mode_reco=="Bd2DstDs"):
            if(data_type=="data"):
                cuts = an.cuts.Bd2DstDs_data_py
            else:
                cuts = an.cuts.Bd2DstDs_py #MC contributions without any PID
        elif(mode_reco=="Bd2DstDsst"):
            if(data_type=="data"):
                cuts = an.cuts.Bd2DstDsst_data_py
            else:
                cuts = an.cuts.Bd2DstDsst_py

        df = df.query(cuts)

        #B momentum estimation (useful for part reco D* Ds where photon is missed)
        #Y is the D* + Ds system
        M_B = 5279.58

        #Name of the charm meson other than the Ds
        c = ""
        if(mode_reco=="Bd2DstDs" or mode_reco=="Bd2DstDsst"):
            c = "Dst"
        elif(mode_reco=="Bd2DDs" or mode_reco=="Bd2DDsst"):
            c = "D"

        df["B_P_reco"] = calc_B_P(df,c,"Ds").tolist()

        #B flight to resolve momentum components
        df["B_FD_X"] = df["B_ENDVERTEX_X"] - df["B_OWNPV_X"]
        df["B_FD_Y"] = df["B_ENDVERTEX_Y"] - df["B_OWNPV_Y"]
        df["B_FD_Z"] = df["B_ENDVERTEX_Z"] - df["B_OWNPV_Z"]
        df["B_FD"] = np.sqrt( df["B_FD_X"]**2 + df["B_FD_Y"]**2 + df["B_FD_Z"]**2)

        p_list = ["X","Y","Z"]
        for p in p_list:
            df["B_P%s_reco" % p] = df["B_P_reco"]*(df["B_FD_%s" % p]/df["B_FD"])
        df["B_PE_reco"] = np.sqrt(df["B_P_reco"]**2 + M_B**2)

        var_types = ["reco"]
        if(data_type=="MC"):
            var_types.append("true")

        for v in var_types:

            df["q2_%s" % v] = calc_q2(df,c,v,True).tolist()

            #Split in B0 and B0-bar, and flip the 3-momentum of the B0-bar particles
            df_B0 = df.query("Dst_ID < 0")
            df_B0b = df.query("Dst_ID > 0")

            parts = ["B",
                    "Dst",
                    "D0",
                    "Dst_Pi",
                    "D0_Pi",
                    "D0_K",
                    "Ds",
                    "Ds_K1",
                    "Ds_K2",
                    "Ds_Pi"
                    ]
            if(mode_reco=="Bd2DstDsst"):
                parts.append("Dsst")
                parts.append("Dsst_Gamma")

            comps = ["PX","PY","PZ"]
            if(data_type=="MC"):
                comps.append("TRUEP_X")
                comps.append("TRUEP_Y")
                comps.append("TRUEP_Z")

            for part in parts:
                for comp in comps:
                    df_B0b["%s_%s" % (part,comp)] = -df_B0b["%s_%s" % (part,comp)]

            #Recombine sample
            df = df_B0.append(df_B0b)

            #theta_D using measured B momentum
            df["theta_D_%s" % v] = calc_theta_D(df,c,v,True).tolist()
            df["costheta_D_%s" % v] = np.cos(df["theta_D_%s" % v])

            #Add sin(theta_D) for use in weight calculation (doesn't go to zero)
            df["sintheta_D_%s" % v] = np.sin(df["theta_D_%s" % v])

            #P -> V P angles (D* Ds) using measured B momentum
            df["theta_X_VP_%s" % v] = calc_theta_X(df,c,v,"Ds_K1",True).tolist()
            df["costheta_X_VP_%s" % v] = np.cos(df["theta_X_VP_%s" % v])

            #P -> V V angles (D* Ds*) using measured B momentum
            df["theta_X_VV_%s" % v] = calc_theta_X(df,c,v,"Ds",True).tolist()
            df["costheta_X_VV_%s" % v] = np.cos(df["theta_X_VV_%s" % v])

            df["chi_VP_%s" % v] = calc_chi(df,c,v,"Ds_K1",True).tolist()
            df["coschi_VP_%s" % v] = np.cos(df["chi_VP_%s" % v])

            df["chi_VV_%s" % v] = calc_chi(df,c,v,"Ds",True).tolist()
            df["coschi_VV_%s" % v] = np.cos(df["chi_VV_%s" % v])

            if(v=="reco"):
                #theta_D using estimated B momentum
                df["theta_D_Breco_%s" % v] = calc_theta_D(df,c,v,False).tolist()
                df["costheta_D_Breco_%s" % v] = np.cos(df["theta_D_Breco_%s" % v])

                #P -> V P angles (D* Ds) using estimated B momentum
                df["theta_X_VP_Breco_%s" % v] = calc_theta_X(df,c,v,"Ds_K1",False).tolist()
                df["costheta_X_VP_Breco_%s" % v] = np.cos(df["theta_X_VP_Breco_%s" % v])

                df["chi_VP_Breco_%s" % v] = calc_chi(df,c,v,"Ds_K1",False).tolist()
                df["coschi_VP_Breco_%s" % v] = np.cos(df["chi_VP_Breco_%s" % v])

                #P -> V V angles (D* Ds*) using estimated B momentum
                df["theta_X_VV_Breco_%s" % v] = calc_theta_X(df,c,v,"Ds",False).tolist()
                df["costheta_X_VV_Breco_%s" % v] = np.cos(df["theta_X_VV_Breco_%s" % v])

                df["chi_VV_Breco_%s" % v] = calc_chi(df,c,v,"Ds",False).tolist()
                df["coschi_VV_Breco_%s" % v] = np.cos(df["chi_VV_Breco_%s" % v])

        #Append this dataframe chunk to the output file with the 'a' append mode
        df.to_root(out_file+".root", key='DecayTree',mode='a')

    print("Output file written")


def main():
    parser = argparse.ArgumentParser(description='Calculate additional variables such as decay angles')
    parser.add_argument("--DataType", choices=["data","MC"],
                    required=True, help="Data type")
    parser.add_argument("--Year", choices=["2015","2016","2017","2018"],
                        required=True, help="Data-taking year")
    parser.add_argument("--Mag", choices=["Up","Down"],
                        required=True, help="Magnet polarity")
    parser.add_argument("--Mode", choices=["Bd2DstDs","Bd2DstDsst","Bd2DstDsst_ReDecay"],
                        required=True, help="Decay mode")
    parser.add_argument("--ModeReco", choices=["Bd2DstDs","Bd2DstDsst"],
                        required=True, help="Decay mode reco. as")
    args = parser.parse_args()

    calc_vars(args.DataType, args.Year, args.Mag, args.Mode, args.ModeReco)

if __name__ == '__main__':
    main()
