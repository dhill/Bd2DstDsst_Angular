import analysis as an
import sys,os
import json

def main():

    #Load the pull values
    with open(an.loc.ROOT+"output/json/m_DstDs_for_fL_pulls.json", 'r') as f_p:
    	fL_pull = json.load(f_p)
    with open(an.loc.ROOT+"output/json/unbinned_angular_fit_pulls_boost_N.json", 'r') as f_p:
    	other_pull = json.load(f_p)

    par_titles = {"fL": "$f_L$",
                  "Hm_amp": "$|H_-|$",
                  "Hm_phi": "$\\phi_-$",
                  "Hp_phi": "$\\phi_+$"
                 }

    f = open(an.loc.ROOT+"output/tex/pulls.tex","w")
    f.write("\\begin{table}[!h] \n")
    f.write("\\begin{center} \n")
    f.write("\\begin{tabular}{c|c|c} \n")
    f.write("Parameter & Pull mean & Pull width \\\\ \\hline \n")
    for p in par_titles:
        if(p=="fL"):
            if(fL_pull[p]["mu"][0]<0):
                phan = ""
            else:
                phan = "\\phantom{-}"
            f.write("%s & $%s%.3f \\pm %.3f$ & $%.3f \\pm %.3f$ \\\\ \n" % (par_titles[p], phan, fL_pull[p]["mu"][0], fL_pull[p]["mu"][1], fL_pull[p]["sigma"][0], fL_pull[p]["sigma"][1]))
        else:
            if(other_pull[p]["mu"][0]<0):
                phan = ""
            else:
                phan = "\\phantom{-}"
            f.write("%s & $%s%.3f \\pm %.3f$ & $%.3f \\pm %.3f$ \\\\ \n" % (par_titles[p], phan, other_pull[p]["mu"][0], other_pull[p]["mu"][1], other_pull[p]["sigma"][0], other_pull[p]["sigma"][1]))
    f.write("\\end{tabular} \n")
    f.write("\\caption{Pull means and widths for the helicity parameters measured in the analysis. \\label{tab:pull_vals}} \n")
    f.write("\\end{center}")
    f.write("\\end{table}")


if __name__ == '__main__':
    main()
