
config = {"D0_M_window" : 20,
          "Ds_M_window": 20,
          "Dst_M_window": 40,
          "Gamma_PT_min": 500,
          "DstDs_M_max": 5240,
          "Dsst_Delta_M_min": 120,
          "Dsst_Delta_M_max": 180,
          "Dst_Delta_M_min": 140,
          "Dst_Delta_M_max": 150,
          "Ds_FD_ZSIG_min": 1,
          "Ds_K_PIDK_min": 1,
          "Ds_Pi_PIDK_max": 10
          }

#Cuts for B0 -> D* Ds

#Mass windows
Bd2DstDs =  f"abs(D0_M - 1865) < {config['D0_M_window']} && abs(Ds_M - 1968) < {config['Ds_M_window']} && abs(Dst_M - 2010) < {config['Dst_M_window']} "
#Trigger requirements
Bd2DstDs += "&& (B_L0HadronDecision_TOS || B_L0Global_TIS) && (B_Hlt1TrackMVADecision_TOS || B_Hlt1TwoTrackMVADecision_TOS) && (B_Hlt2Topo2BodyDecision_TOS || B_Hlt2Topo3BodyDecision_TOS || B_Hlt2Topo4BodyDecision_TOS) "
#D* Delta M
Bd2DstDs += f"&& Dst_Delta_M > {config['Dst_Delta_M_min']} && Dst_Delta_M < {config['Dst_Delta_M_max']} "
#Ds flight cut
Bd2DstDs += f"&& ((Ds_ENDVERTEX_Z - B_ENDVERTEX_Z)/(Ds_ENDVERTEX_ZERR^2 + B_ENDVERTEX_ZERR^2)^(0.5)) > {config['Ds_FD_ZSIG_min']} "

#Data cut has PID in addtion
Bd2DstDs_data = Bd2DstDs + f"&& Ds_K1_PIDK > {config['Ds_K_PIDK_min']}  && Ds_K2_PIDK > {config['Ds_K_PIDK_min']} && Ds_Pi_PIDK < {config['Ds_Pi_PIDK_max']}"

#Python version for pandas
Bd2DstDs_py = str(Bd2DstDs)
Bd2DstDs_py = Bd2DstDs_py.replace("||","or")
Bd2DstDs_py = Bd2DstDs_py.replace("&&","and")
Bd2DstDs_py = Bd2DstDs_py.replace("^","**")

Bd2DstDs_data_py = str(Bd2DstDs_data)
Bd2DstDs_data_py = Bd2DstDs_data_py.replace("||","or")
Bd2DstDs_data_py = Bd2DstDs_data_py.replace("&&","and")
Bd2DstDs_data_py = Bd2DstDs_data_py.replace("^","**")

#Cuts for fully reco B0 -> D* Ds* (same as above with additional cuts)

Bd2DstDsst = Bd2DstDs[:]
#Photon pT cut
Bd2DstDsst += f" && Dsst_Gamma_PT > {config['Gamma_PT_min']} "
#Veto on the m(D* Ds) mass
Bd2DstDsst += f"&& DstDs_M < {config['DstDs_M_max']} "
#Ds* Delta M
Bd2DstDsst += f"&& Dsst_Delta_M > {config['Dsst_Delta_M_min']} && Dsst_Delta_M < {config['Dsst_Delta_M_max']} "

#Data cut has PID in addtion
Bd2DstDsst_data = Bd2DstDsst + f"&& Ds_K1_PIDK > {config['Ds_K_PIDK_min']}  && Ds_K2_PIDK > {config['Ds_K_PIDK_min']} && Ds_Pi_PIDK < {config['Ds_Pi_PIDK_max']}"

#Python version for pandas
Bd2DstDsst_py = str(Bd2DstDsst)
Bd2DstDsst_py = Bd2DstDsst_py.replace("||","or")
Bd2DstDsst_py = Bd2DstDsst_py.replace("&&","and")
Bd2DstDsst_py = Bd2DstDsst_py.replace("^","**")

Bd2DstDsst_data_py = str(Bd2DstDsst_data)
Bd2DstDsst_data_py = Bd2DstDsst_data_py.replace("||","or")
Bd2DstDsst_data_py = Bd2DstDsst_data_py.replace("&&","and")
Bd2DstDsst_data_py = Bd2DstDsst_data_py.replace("^","**")
