import glob, os, sys
import numpy as np
import os.path
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy import stats
from scipy.stats import norm
import json
from collections import OrderedDict
from root_pandas import read_root
import pandas as pd
from scipy.stats import pearsonr
import pickle
from uncertainties import ufloat
import analysis as an

def binomial_err(N,k):
	return (1.0/N)*np.sqrt(k*(1-float(k)/N))

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)


def main():
	years = ["2015","2016"]
	mags = ["Up","Down"]

	#Generator efficiencies
	gen_eff = {}
	gen_eff["Bd2DstDs"] = ufloat(0.25*(0.16656 + 0.16801 + 0.16778 + 0.16797), 0.001)
	gen_eff["Bd2DstDsst"] = ufloat(0.5*(0.1685 + 0.1636), 0.001)

	#B0 -> D* Ds* part reco
	path = an.loc.WGEOS+"/tuples/MC/Bd2DstDsst"

	file_list = []
	file_list_gen = []

	for y in years:
		for m in mags:
			file_list.append("%s/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))
			file_list.append("%s_ReDecay/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))
			#Add ReDecay samples too
			file_list_gen.append("%s/%s_%s/Total.root" % (path,y,m))
			file_list_gen.append("%s_ReDecay/%s_%s/Total.root" % (path,y,m))

	#Final selected events
	df_dstdsst = read_root(file_list,"DecayTree",columns=["B_BKGCAT","Ds_K1_PIDK","Ds_K2_PIDK","Ds_Pi_PIDK"])
	#Truth match and PID
	df_dstdsst = df_dstdsst.query("B_BKGCAT==50 or B_BKGCAT==60")
	df_dstdsst = df_dstdsst.query("Ds_K1_PIDK>1 and Ds_K2_PIDK>1 and Ds_Pi_PIDK<10")

	#All events in the nTuple after stripping
	df_dstdsst_all = read_root(file_list_gen,"Bd2DstDs/DecayTree",columns=["B_BKGCAT"])
	#Truth-match
	df_dstdsst_all = df_dstdsst_all.query("B_BKGCAT==50 or B_BKGCAT==60")

	#Generated events in BKK
	df_dstdsst_gen = read_root(file_list_gen,"EventTuple/EventTuple")

	#Stripping eff including truth matching
	strip_eff = {}
	eff = float(len(df_dstdsst_all)) / len(df_dstdsst_gen)
	err = binomial_err(len(df_dstdsst_gen), len(df_dstdsst_all))
	strip_eff["Bd2DstDsst"] = ufloat(eff, err)
	print("B0 -> D* Ds* strip eff")
	print(strip_eff["Bd2DstDsst"])

	#Offline efficiency
	off_eff = {}
	eff = float(len(df_dstdsst)) / len(df_dstdsst_all)
	err = binomial_err(len(df_dstdsst_all),len(df_dstdsst))
	off_eff["Bd2DstDsst"] = ufloat(eff, err)
	print("B0 -> D* Ds* offline eff")
	print(off_eff["Bd2DstDsst"])

	#B0 -> D* Ds full reco
	path = an.loc.WGEOS+"/tuples/MC/Bd2DstDs"

	file_list = []
	file_list_gen = []

	for y in years:
		for m in mags:
			file_list.append("%s/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))
			file_list_gen.append("%s/%s_%s/Total.root" % (path,y,m))

	#Final selected events
	df_dstds = read_root(file_list,"DecayTree",columns=["B_BKGCAT","Ds_K1_PIDK","Ds_K2_PIDK","Ds_Pi_PIDK"])

	#Truth match and PID
	df_dstds = df_dstds.query("B_BKGCAT==10 or B_BKGCAT==50 or B_BKGCAT==60")
	df_dstds = df_dstds.query("Ds_K1_PIDK>1 and Ds_K2_PIDK>1 and Ds_Pi_PIDK<10")

	#All events in the nTuple after stripping
	df_dstds_all = read_root(file_list_gen,"Bd2DstDs/DecayTree",columns=["B_BKGCAT"])
	#Truth-match
	df_dstds_all = df_dstds_all.query("B_BKGCAT==10 or B_BKGCAT==50 or B_BKGCAT==60")

	#Generated events in BKK
	df_dstds_gen = read_root(file_list_gen,"EventTuple/EventTuple")

	#Stripping eff including truth matching and additional filtering efficiency
	eff_strip_dstds = 0.0162
	eff = eff_strip_dstds * (float(len(df_dstds_all)) / len(df_dstds_gen))
	err = binomial_err(len(df_dstds_gen), len(df_dstds_all)) * eff_strip_dstds
	strip_eff["Bd2DstDs"] = ufloat(eff,err)
	print("B0 -> D* Ds strip eff")
	print(strip_eff["Bd2DstDs"])

	#Offline efficiency
	eff = float(len(df_dstds)) / len(df_dstds_all)
	err = binomial_err(len(df_dstds_all), len(df_dstds))
	off_eff["Bd2DstDs"] = ufloat(eff,err)
	print("B0 -> D* Ds offline eff")
	print(off_eff["Bd2DstDs"])

	#Total effs
	tot_eff = {}
	tot_eff["Bd2DstDsst"] = gen_eff["Bd2DstDsst"] * strip_eff["Bd2DstDsst"] * off_eff["Bd2DstDsst"]
	tot_eff["Bd2DstDs"] = gen_eff["Bd2DstDs"] * strip_eff["Bd2DstDs"] * off_eff["Bd2DstDs"]

	#Eff ratio
	r = tot_eff["Bd2DstDs"] / tot_eff["Bd2DstDsst"]
	print(r)

	#Make table of effs

	modes = {"Bd2DstDs": "$B^0 \\to D^{*-} D_s^+$",
			 "Bd2DstDsst": "$B^0 \\to D^{*-} (D_s^{*+} \\to D_s^+ \\gamma)$"
			}

	f = open(an.loc.ROOT+"output/tex/effs.tex","w")
	f.write("\\begin{table}[!h] \n")
	f.write("\\small \n")
	f.write("\\begin{center} \n")
	f.write("\\begin{tabular}{c|c|c|c|c} \n")
	f.write("Mode & $\\epsilon_{gen}$ & $\\epsilon_{strip}$ & $\\epsilon_{off}$ & $\\epsilon_{tot}$ \\\\ \\hline \n")
	for mode in modes:
		f.write("%s & $%.4f \\pm %.4f$ & $%.4f \\pm %.4f$ & $%.4f \\pm %.4f$ & $%.4f \\pm %.4f$ \\\\ \n" % (modes[mode],
																										 100*gen_eff[mode].n,
																										 100*gen_eff[mode].s,
																										 100*strip_eff[mode].n,
																										 100*strip_eff[mode].s,
																										 100*off_eff[mode].n,
																										 100*off_eff[mode].s,
																										 100*tot_eff[mode].n,
																										 100*tot_eff[mode].s))
	f.write("\\end{tabular} \n")
	f.write("\\caption{Efficiencies (\\%) for $B^0 \\to D^{*-} D_s^+$ and partially reconstructed $B^0 \\to D^{*-} D_s^{*+}$ decays which are used in the branching fraction ratio calculation. \\label{tab:effs_breakdown}} \n")
	f.write("\\end{center} \n")
	f.write("\\end{table} \n")



if __name__ == '__main__':
    main()
