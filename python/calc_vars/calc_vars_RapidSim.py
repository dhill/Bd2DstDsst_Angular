import os, sys, argparse
import pandas as pd
import numpy as np
from root_pandas import to_root, read_root
import hepvector
from hepvector import LorentzVector
from tqdm import tqdm
import random
import analysis as an

#Invariant mass of a particle pair
def calc_mXY(df,x,y):

    v_X = LorentzVector(df["%s_PX" % x].values,
                        df["%s_PY" % x].values,
                        df["%s_PZ" % x].values,
                        df["%s_E" % x].values)

    v_Y = LorentzVector(df["%s_PX" % y].values,
                        df["%s_PY" % y].values,
                        df["%s_PZ" % y].values,
                        df["%s_E" % y].values)

    v_comb = v_X + v_Y

    m = v_comb.mag

    return m

#Calculate decay q2
def calc_q2(df,c,type,isFullReco):

	if(type=="reco"):
		#Was the B momentum estimated (for a part reco decay)
		if(isFullReco==False):
			suf = "_reco"
		else:
			suf = ""
		b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_E%s" % suf]
		c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_E" % c]

	elif(type=="true"):
		b_vars = ["B_PX_TRUE","B_PY_TRUE","B_PZ_TRUE","B_E_TRUE"]
		c_vars = ["%s_PX_TRUE" % c,"%s_PY_TRUE" % c,"%s_PZ_TRUE" % c,"%s_E_TRUE" % c]

	v_B = LorentzVector(df[b_vars[0]].values,
					    df[b_vars[1]].values,
					    df[b_vars[2]].values,
					    df[b_vars[3]].values
						)
	v_Dst = LorentzVector(df[c_vars[0]].values,
					      df[c_vars[1]].values,
					      df[c_vars[2]].values,
					      df[c_vars[3]].values
						 )

	v_q = v_B - v_Dst

	q2 = v_q.mag2

	#Returns array of q2 values
	return q2

#D*(2010) system angle
def calc_theta_D(df,c,type,isFullReco):

	if(type=="reco"):
		if(isFullReco==False):
			suf = "_reco"
		else:
			suf = ""
		b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_E%s" % suf]
		c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_E" % c]
		if(c=="Dst"):
			c_dau_vars = ["D0_PX","D0_PY","D0_PZ","D0_E"]
		elif(c=="D"):
			c_dau_vars = ["D_K_PX","D_K_PY","D_K_PZ","D_K_E"]

	elif(type=="true"):
		b_vars = ["B_PX_TRUE","B_PY_TRUE","B_PZ_TRUE","B_E_TRUE"]
		c_vars = ["%s_PX_TRUE" % c,"%s_PY_TRUE" % c,"%s_PZ_TRUE" % c,"%s_E_TRUE" % c]
		if(c=="Dst"):
			c_dau_vars = ["D0_PX_TRUE","D0_PY_TRUE","D0_PZ_TRUE","D0_E_TRUE"]
		elif(c=="D"):
			c_dau_vars = ["D_K_PX_TRUE","D_K_PY_TRUE","D_K_PZ_TRUE","D_K_E_TRUE"]

	v_c_dau = LorentzVector(df[c_dau_vars[0]].values,
					    df[c_dau_vars[1]].values,
					    df[c_dau_vars[2]].values,
					    df[c_dau_vars[3]].values)

	v_c = LorentzVector(df[c_vars[0]].values,
						df[c_vars[1]].values,
						df[c_vars[2]].values,
						df[c_vars[3]].values)

	v_B = LorentzVector(df[b_vars[0]].values,
						df[b_vars[1]].values,
						df[b_vars[2]].values,
						df[b_vars[3]].values)

	#Boost to B rest frame first
	B_boost = -(v_B.boostp3)

	v_c_dau.boost(B_boost,inplace=True)
	v_c.boost(B_boost,inplace=True)
	v_B.boost(B_boost,inplace=True)

	#Now boost to X frame
	c_boost = -(v_c.boostp3)
	v_c_dau.boost(c_boost,inplace=True)
	v_B.boost(c_boost,inplace=True)

	vec_c_dau = v_c_dau.p3.unit
	vec_B = v_B.p3.unit

	theta_D = vec_c_dau.angle(vec_B)

	return theta_D

#Angle in the "other" charm system
def calc_theta_X(df,c,type,X,isFullReco):

	if(type=="reco"):
		if(isFullReco==False):
			suf = "_reco"
		else:
			suf = ""
		b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_E%s" % suf]
		x_vars = ["%s_PX" % X,"%s_PY" % X,"%s_PZ" % X,"%s_E" % X]
		c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_E" % c]

	elif(type=="true"):
		b_vars = ["B_PX_TRUE","B_PY_TRUE","B_PZ_TRUE","B_E_TRUE"]
		c_vars = ["%s_PX_TRUE" % c,"%s_PY_TRUE" % c,"%s_PZ_TRUE" % c,"%s_E_TRUE" % c]
		x_vars = ["%s_PX_TRUE" % X,"%s_PY_TRUE" % X,"%s_PZ_TRUE" % X,"%s_E_TRUE" % X]

	v_X = LorentzVector(df[x_vars[0]].values,
						df[x_vars[1]].values,
						df[x_vars[2]].values,
						df[x_vars[3]].values)

	v_c = LorentzVector(df[c_vars[0]].values,
						  df[c_vars[1]].values,
						  df[c_vars[2]].values,
						  df[c_vars[3]].values)

	v_B = LorentzVector(df[b_vars[0]].values,
						df[b_vars[1]].values,
						df[b_vars[2]].values,
						df[b_vars[3]].values)

	v_W = v_B - v_c

	#Boost to B rest frame first
	B_boost = -(v_B.boostp3)

	v_X.boost(B_boost,inplace=True)
	v_B.boost(B_boost,inplace=True)
	v_W.boost(B_boost,inplace=True)

	#Now boost to the W rest frame
	W_boost = -(v_W.boostp3)

	#inplace is important!! Wrong results calculated otherwise
	v_X.boost(W_boost,inplace=True)
	v_B.boost(W_boost,inplace=True)

	vec_X = v_X.p3.unit
	vec_B = v_B.p3.unit

	theta_X = vec_X.angle(vec_B)

	return theta_X

#Angle between the decay planes
def calc_chi(df,c,type,X,isFullReco):

	if(type=="reco"):
		if(isFullReco==False):
			suf = "_reco"
		else:
			suf = ""
		b_vars = ["B_PX%s" % suf,"B_PY%s" % suf,"B_PZ%s" % suf,"B_E%s" % suf]
		x_vars = ["%s_PX" % X,"%s_PY" % X,"%s_PZ" % X,"%s_E" % X]
		c_vars = ["%s_PX" % c,"%s_PY" % c,"%s_PZ" % c,"%s_E" % c]
		if(c=="Dst"):
			c_dau_vars = ["D0_PX","D0_PY","D0_PZ","D0_E"]
		elif(c=="D"):
			c_dau_vars = ["D_K_PX","D_K_PY","D_K_PZ","D_K_E"]

	elif(type=="true"):
		b_vars = ["B_PX_TRUE","B_PY_TRUE","B_PZ_TRUE","B_E_TRUE"]
		x_vars = ["%s_PX_TRUE" % X,"%s_PY_TRUE" % X,"%s_PZ_TRUE" % X,"%s_E_TRUE" % X]
		c_vars = ["%s_PX_TRUE" % c,"%s_PY_TRUE" % c,"%s_PZ_TRUE" % c,"%s_E_TRUE" % c]
		if(c=="Dst"):
			c_dau_vars = ["D0_PX_TRUE","D0_PY_TRUE","D0_PZ_TRUE","D0_E_TRUE"]
		elif(c=="D"):
			c_dau_vars = ["D_K_PX_TRUE","D_K_PY_TRUE","D_K_PZ_TRUE","D_K_E_TRUE"]


	v_X = LorentzVector(df[x_vars[0]].values,
						df[x_vars[1]].values,
						df[x_vars[2]].values,
						df[x_vars[3]].values)

	v_c_dau = LorentzVector(df[c_dau_vars[0]].values,
						 df[c_dau_vars[1]].values,
						 df[c_dau_vars[2]].values,
						 df[c_dau_vars[3]].values)

	v_c = LorentzVector(df[c_vars[0]].values,
						df[c_vars[1]].values,
						df[c_vars[2]].values,
						df[c_vars[3]].values)

	v_B = LorentzVector(df[b_vars[0]].values,
						df[b_vars[1]].values,
						df[b_vars[2]].values,
						df[b_vars[3]].values)

	#4-vector of W
	v_W = v_B - v_c

	#Boost everything to the B frame
	B_boost = -(v_B.boostp3)

	v_B.boost(B_boost,inplace=True)
	v_W.boost(B_boost,inplace=True)
	v_X.boost(B_boost,inplace=True)
	v_c.boost(B_boost,inplace=True)
	v_c_dau.boost(B_boost,inplace=True)

	vec_c = v_c.p3.unit
	vec_c_dau = v_c_dau.p3.unit
	vec_c_c_dau = vec_c.cross(vec_c_dau)

	vec_W = v_W.p3.unit
	vec_X = v_X.p3.unit
	vec_WX = vec_W.cross(vec_X)

	vec_x = vec_c_c_dau.unit
	vec_z = vec_c
	vec_y = (vec_z.cross(vec_x)).unit

	C = vec_WX.dot(vec_x)
	S = vec_WX.dot(vec_y)

	chi = np.arctan2(S,C)

	return chi

def calc_vars(geom):

    #RapidSim generated with fL from the m(D* Ds) fit
    mode = "Bd2DstDsst_With_Part_Reco_fL"

    path = an.loc.WGEOS+"/RapidSim_tuples/%s" % mode

    #RapidSim output file
    in_file = "%s/model_KKPi_%s_tree" % (path,geom)

    out_file = "%s/Total_%s_Vars" % (path,geom)

    if os.path.exists(out_file+".root"):
        os.remove(out_file+".root")

    for df in tqdm(read_root(in_file+".root","DecayTree",chunksize=100000)):

        #B momentum estimation (useful for part reco D* Ds where photon is missed)
        #Y is the D* + Ds system
        M_B = 5.27958

        #Charm meson other than the Ds*
        c = "Dst"
        df["DstDs_M"] = calc_mXY(df,"Dst","Ds").tolist()
        df["Y_PX"] = df["%s_PX" % c] + df["Ds_PX"]
        df["Y_PY"] = df["%s_PY" % c] + df["Ds_PY"]
        df["Y_PZ"] = df["%s_PZ" % c] + df["Ds_PZ"]
        df["Y_PE"] = df["%s_E" % c] + df["Ds_E"]
        df["Y_P"] = np.sqrt(df["Y_PX"]**2 + df["Y_PY"]**2 + df["Y_PZ"]**2)
        df["Y_M"] = np.sqrt(df["Y_PE"]**2 - df["Y_P"]**2)
        df["Y_thetamax"] = np.arcsin((M_B**2 - df["Y_M"]**2)/(2*M_B*df["Y_P"]))

        df["B_P_reco"] = ((df["Y_M"]**2 + M_B**2)*df["Y_P"]*np.cos(df["Y_thetamax"]))/(2*(df["Y_PE"]**2 - df["Y_P"]**2*(np.cos(df["Y_thetamax"]))**2))

        #B flight to resolve momentum components
        df["B_FD_X"] = df["B_End_x_TRUE"] - df["B_Ori_x_TRUE"]
        df["B_FD_Y"] = df["B_End_y_TRUE"] - df["B_Ori_y_TRUE"]
        df["B_FD_Z"] = df["B_End_z_TRUE"] - df["B_Ori_z_TRUE"]
        df["B_FD"] = np.sqrt( df["B_FD_X"]**2 + df["B_FD_Y"]**2 + df["B_FD_Z"]**2)

        p_list = ["X","Y","Z"]
        for p in p_list:
            df["B_P%s_reco" % p] = df["B_P_reco"]*(df["B_FD_%s" % p]/df["B_FD"])
        df["B_E_reco"] = np.sqrt(df["B_P_reco"]**2 + M_B**2)

        var_types = ["reco","true"]

        for v in var_types:

            df["q2_%s" % v] = calc_q2(df,c,v,True).tolist()

            #theta_D using measured B momentum
            df["theta_D_%s" % v] = calc_theta_D(df,c,v,True).tolist()
            df["costheta_D_%s" % v] = np.cos(df["theta_D_%s" % v])

            #Add sin(theta_D) for use in weight calculation (doesn't go to zero)
            df["sintheta_D_%s" % v] = np.sin(df["theta_D_%s" % v])

            #P -> V P angles (D* Ds) using measured B momentum
            df["theta_X_VP_%s" % v] = calc_theta_X(df,c,v,"Ds_K1",True).tolist()
            df["costheta_X_VP_%s" % v] = np.cos(df["theta_X_VP_%s" % v])

            df["chi_VP_%s" % v] = calc_chi(df,c,v,"Ds_K1",True).tolist()
            df["coschi_VP_%s" % v] = np.cos(df["chi_VP_%s" % v])

            #P -> V V angles (D* Ds*) using measured B momentum
            df["theta_X_VV_%s" % v] = calc_theta_X(df,c,v,"Ds",True).tolist()
            df["costheta_X_VV_%s" % v] = np.cos(df["theta_X_VV_%s" % v])

            df["chi_VV_%s" % v] = calc_chi(df,c,v,"Ds",True).tolist()
            df["coschi_VV_%s" % v] = np.cos(df["chi_VV_%s" % v])

            if(v=="reco"):
                #theta_D using estimated B momentum
                df["theta_D_Breco_%s" % v] = calc_theta_D(df,c,v,False).tolist()
                df["costheta_D_Breco_%s" % v] = np.cos(df["theta_D_Breco_%s" % v])

                #P -> V P angles (D* Ds) using estimated B momentum
                df["theta_X_VP_Breco_%s" % v] = calc_theta_X(df,c,v,"Ds_K1",False).tolist()
                df["costheta_X_VP_Breco_%s" % v] = np.cos(df["theta_X_VP_Breco_%s" % v])
                df["chi_VP_Breco_%s" % v] = calc_chi(df,c,v,"Ds_K1",False).tolist()
                df["coschi_VP_Breco_%s" % v] = np.cos(df["chi_VP_Breco_%s" % v])

                #P -> V V angles (D* Ds*) using estimated B momentum
                df["theta_X_VV_Breco_%s" % v] = calc_theta_X(df,c,v,"Ds",False).tolist()
                df["costheta_X_VV_Breco_%s" % v] = np.cos(df["theta_X_VV_Breco_%s" % v])

                df["chi_VV_Breco_%s" % v] = calc_chi(df,c,v,"Ds",False).tolist()
                df["coschi_VV_Breco_%s" % v] = np.cos(df["chi_VV_Breco_%s" % v])
        #Append this dataframe chunk to the output file with the 'a' append mode
        df.to_root(out_file+".root", key='DecayTree',mode='a')

    print("Output file written")
    sys.exit()

def main():
    parser = argparse.ArgumentParser(description='Calculate additional variables such as decay angles for RapidSim MC')
    parser.add_argument("--Geom", choices=["LHCb","All"],
                        required=True, help="Geometry")
    args = parser.parse_args()

    calc_vars(args.Geom)

if __name__ == '__main__':
    main()
