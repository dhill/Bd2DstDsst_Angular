import sys, os
import subprocess
import analysis as an

n_fits = 200

boost = ["N","Y"] #stats boost for toys

for b in boost:
    cfile = open(an.loc.ROOT+f'condor/submit_scripts/toys_unbinned_angular_fit_boost_{b}.submit','w')
    command = \
    f"executable = run_toys_unbinned_angular_fit.sh \n\
    universe = vanilla \n\
    arguments = {b} \n\
    output = {an.loc.ROOT}output/logs/results.output.$(ClusterId) \n\
    error = {an.loc.ROOT}output/logs/results.error.$(ClusterId) \n\
    log = {an.loc.ROOT}output/logs/results.log.$(ClusterId) \n\
    notification    = never \n\
    request_cpus = 1 \n\
    queue {n_fits}   \n\n"
    cfile.write(command)
