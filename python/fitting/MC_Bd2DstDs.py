import numpy as np
import sys, os
import json
import os.path
from os import path
from root_pandas import to_root, read_root
import pandas as pd
import matplotlib
from collections import OrderedDict
#matplotlib.use('Agg')  #fix python2 tkinter problem
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooAbsData, RooStats, gInterpreter, gPad, RooChebychev, RooSimultaneous, RooAddPdf, RooCategory, RooExponential, RooFit, TCanvas, TFile, TTree, TChain, RooArgList, RooArgSet, RooGaussian, RooCBShape, RooDataSet, RooDataHist, RooPlot, RooRealVar, RooFormulaVar
from listsfromhist import listsfromhist
import analysis as an

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def Plot(frame,low,high,bin_width):
	D = frame.getObject(0)
	data_arr = listsfromhist(D, overunder = True)

	fig, ax = plt.subplots(figsize=(8,6))

	#Plot data
	plt.errorbar(data_arr[0],data_arr[1],yerr=data_arr[2],ls=None,color='k',fmt='o',markersize=2,label="2015-2018 MC")

	plt.ylabel("Candidates / (%.2f MeV/$c^2$)" % bin_width,fontsize=25)
	plt.xlabel("$m(D^{*-}D_s^+)$ [MeV/$c^2$]",fontsize=25)
	ax.tick_params(axis='both', which='major', labelsize=20)
	plt.xlim(low,high)

	x_vals = np.linspace(low,high,1000)

	#Total
	P = frame.getObject(1)
	pdf_arr = []
	for v in x_vals:
		pdf_arr.append(P.interpolate(v))
	plt.plot(x_vals,pdf_arr,color="crimson",linewidth=1.0,label="Fit")

	ymin, ymax = ax.get_ylim()
	plt.ylim(0.1,ymax*1.5)
	#plt.yscale("log")
	plt.legend(fontsize=20,loc='upper left')
	plt.tight_layout()
	plt.show()
	fig.savefig(an.loc.ROOT+"output/plots/MC_Bd2DstDs_M_B.pdf")

def main():

	#Load data
	years = ["2015","2016","2017","2018"]
	mags = ["Up","Down"]

	path = an.loc.WGEOS+"/tuples/MC/Bd2DstDs"

	tree = TChain("DecayTree")

	angle_type = "VV"

	vars = ["B_D0constDsconstPVconst_B_M",
			"B_BKGCAT",
			"Ds_K1_PIDK",
			"Ds_K2_PIDK",
			"Ds_Pi_PIDK"
		   ]

	for y in years:
		for m in mags:
			tree.Add("%s/%s_%s/Total_Bd2DstDs_Vars.root" % (path,y,m))

	tree.SetBranchStatus("*",0)

	for v in vars:
		tree.SetBranchStatus(v,1)

	cuts = "(B_BKGCAT==10 || B_BKGCAT==50 || B_BKGCAT==60)"
	cuts += "&& Ds_K1_PIDK > 1 && Ds_K2_PIDK > 1 && Ds_Pi_PIDK < 10"

	tree = tree.CopyTree(cuts)
	print("Events in tree: %s" % tree.GetEntries())

	#Make RooDataSet containing fit variable (B mass) and the angles we want to look at sWeighted
	b_low = 5225.
	b_high = 5325.
	B_M = RooRealVar("B_D0constDsconstPVconst_B_M","",b_low,b_high)
	bin_width = 1.
	num_bins = int((b_high-b_low)/bin_width)
	B_M.setBins(num_bins)

	args = RooArgSet(B_M)

	data = RooDataSet("data","data",args,RooFit.Import(tree))
	data.Print()

	data_h = data.binnedClone()

	#Model
	mu = RooRealVar("mu","",5279,5200,5400)
	sigma = RooRealVar("sigma","",8,0,100)
	alpha_R = RooRealVar("alpha_R","",-7.8827e-01,-10.0,-1e-10)
	n_R = RooRealVar("n_R","",10,0,1000)
	alpha_L = RooRealVar("alpha_L","",7.8827e-01,0.0,10.0)
	n_L = RooRealVar("n_L","",10,0,1000)
	frac = RooRealVar("frac","",0.5,0.,1.)

	sigL_pdf = RooCBShape("sigL_pdf","",B_M,mu,sigma,alpha_L,n_L)
	sigR_pdf = RooCBShape("sigR_pdf","",B_M,mu,sigma,alpha_R,n_R)
	sig_pdf = RooAddPdf("sig_pdf","",RooArgList(sigL_pdf,sigR_pdf),RooArgList(frac))

	result = sig_pdf.fitTo(data,RooFit.Save(),RooFit.NumCPU(8,0),RooFit.Optimize(False),RooFit.Offset(True),RooFit.Minimizer("Minuit2","migrad"),RooFit.Strategy(2))
	result.Print("V")

	#Save results
	result_file = TFile.Open(an.loc.ROOT+"output/roofitresults/MC_Bd2DstDs.root","RECREATE")
	result_file.cd()
	result.Write()
	result_file.Write()
	result_file.Close()

	#Write results to JSON
	results_dict = {}
	results_dict["mu"] = [mu.getVal(),mu.getError()]
	results_dict["sigma"] = [sigma.getVal(),sigma.getError()]
	results_dict["alpha_R"] = [alpha_R.getVal(),alpha_R.getError()]
	results_dict["n_R"] = [n_R.getVal(),n_R.getError()]
	results_dict["alpha_L"] = [alpha_L.getVal(),alpha_L.getError()]
	results_dict["n_L"] = [n_L.getVal(),n_L.getError()]
	results_dict["frac"] = [frac.getVal(),frac.getError()]

	with open(an.loc.ROOT+'output/json/MC_Bd2DstDs.json', 'w') as f:
		json.dump(results_dict, f, sort_keys=True, indent=4)

	frame = B_M.frame()
	data.plotOn(frame)
	sig_pdf.plotOn(frame)

	Plot(frame,b_low,b_high,bin_width)

if __name__ == '__main__':
    main()
