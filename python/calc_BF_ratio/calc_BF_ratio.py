import sys, os
from uncertainties import ufloat, correlated_values
import numpy as np
import analysis as an
import json

def main():

    #Yield ratio (stat error)
    corr_matrix = np.array([[1.0, 0.207], [0.207, 1.0]]) #correlation between yields in fit is 0.207

    #Read yields from m(D* Ds) fit result
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL.json','r') as f:
        yield_dict = json.load(f)

    vals = np.array([yield_dict["n_Bd2DstDs"][0], yield_dict["n_Bd2DstDsst_Dsst2Dsgamma"][0]]) #n_dstst, n_dstdsst
    errs = np.array([yield_dict["n_Bd2DstDs"][1], yield_dict["n_Bd2DstDsst_Dsst2Dsgamma"][1]]) #fit errors

    cov_matrix = errs*corr_matrix*errs

    (n_dstds, n_dstdsst) = correlated_values(vals, cov_matrix)

    N_stat = n_dstdsst / n_dstds
    print("Yield ratio stat = %s" % N_stat)

    #Yield ratio (syst error)
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL_syst.json','r') as f:
        syst_dict = json.load(f)
    errs = np.array([syst_dict["n_Bd2DstDs_tot"], syst_dict["n_Bd2DstDsst_Dsst2Dsgamma_tot"]]) #syst errors on yields from the fits

    corr_matrix = np.array([[1.0, 0.], [0., 1.0]]) #assume uncorrelated systematics

    cov_matrix = errs*corr_matrix*errs

    (n_dstds, n_dstdsst) = correlated_values(vals, cov_matrix)

    N_syst = n_dstdsst / n_dstds
    print("Yield ratio syst = %s" % N_syst)

    #MC effs
    with open(an.loc.ROOT+'/output/json/effs.json','r') as f:
        effs_dict = json.load(f)
    eff_dstds = ufloat(effs_dict["Bd2DstDs"][0],0.)
    eff_dstdsst = ufloat(effs_dict["Bd2DstDsst"][0],0.)

    R_stat = N_stat * (eff_dstds / eff_dstdsst)

    print("R stat = %s" % R_stat)

    eff_dstds = ufloat(effs_dict["Bd2DstDs"][0],effs_dict["Bd2DstDs"][1]) #include eff errors in systematics
    eff_dstdsst = ufloat(effs_dict["Bd2DstDsst"][0],effs_dict["Bd2DstDsst"][1])

    R_syst = N_syst * (eff_dstds / eff_dstdsst)

    print("R syst = %s" % R_syst)

    BF_results = {}

    BF_results["yield_ratio"] = [N_stat.n, N_stat.s, N_syst.s]
    BF_results["BF_ratio"] = [R_stat.n, R_stat.s, R_syst.s]

    #Save results
    with open(an.loc.ROOT+'output/json/BF_ratio.json', 'w') as fp:
        json.dump(BF_results, fp, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()
