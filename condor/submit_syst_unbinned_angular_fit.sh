#!/bin/sh

export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

cd $ANAROOT/condor

for syst in fL data_sw acc_funcs
do
	condor_submit submit_scripts/syst_unbinned_angular_fit_${syst}.submit
done
