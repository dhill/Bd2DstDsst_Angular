## Cloning repository

When cloning, make sure to clone with `git clone --recursive` to clone submodules. 

## Before setup

The `pyutils` submodule is currently configured with Python 2 as a default. To switch to the Python 3 `pyutils`, do:

```
cd pyutils/
git checkout simone_py3
```

## Setting up

A setup script is provided to create the required `conda` Python environment. To run the setup, do:

```
source setup/setup_env.sh
```

This installs the conda env called `ana_env` which includes ROOT. Additional required packages are also installed.  

After this is done, add the following line to your .bashrc:

```
source $HOME/miniconda/etc/profile.d/conda.sh
```

For any subsequent env setup, do:

```
source setup/setup.sh
```

which will activate the `ana_env` environment. To leave this env, do:

```
conda deactivate
```

## Running the analysis

The analysis pipeline is configured in the `Snakefile` file, where the pipeline begins with merged DaVinci output nTuples for data and MC as well as RapidSim nTuples. All processing steps are performed on these files, and various steps of the analysis performed leading to production of the final results.

To run the analysis, do:

```
snakemake -j8
```

which will run all steps of the analysis on 8 cores. The `output` folder of this project will store the required outputs, in sub-directories for LaTeX (`tex`) and plots (`plots`) which can be used in the ANA and PAPER. Additional outputs of the analysis are persisted in JSON (`json`), RooFitResult (`roofitresults`), ROOT histogram (`hists`), and ROOT TTree (`rootfiles`) format. The `Snakefile` contains all details regarding input and output files in the analysis, and calls various python scripts from the `python` folder to perform each analyis step.


## Analysis nTuple location

The DaVinci output files for data and MC, and the RapidSim files used in the analysis, are stored in this B2OC EOS directory:

```
/eos/lhcb/wg/b2oc/AmAn_Bd2DstDsst_Run12
```

This location is accessed in the analysis code via the `an.loc.WGEOS` variable, which is defined in `python/analysis/locations.py`. The analysis pipeline runs on files in this location, writing most output to the `output` directory in the project folder. Output fit results from toys and systematics runs are stored in the EOS directory. 


## Running condor batch jobs

The batch jobs for toys and systematics are not run in the snakemake workflow. They can be submitted by doing:

```
sh condor/submit_toys_unbinned_angular_fit.sh
sh condor/submit_syst_unbinned_angular_fit.sh
sh condor/submit_toys_m_DstDs_for_fL.sh
sh condor/submit_syst_m_DstDs_for_fL.sh
```

These commands will submit condor batch jobs for the fit which measures $`f_L`$ and the unbinned angular fit which measures the other helicity parameters. The output fit results are stored in the WG EOS folder, which are then analysed as part of the snakemake workflow.
