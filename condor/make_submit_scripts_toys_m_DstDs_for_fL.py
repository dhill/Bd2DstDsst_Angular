import sys, os
import subprocess
import analysis as an

n_fits = 200

cfile = open(an.loc.ROOT+'condor/submit_scripts/toys_m_DstDs_for_fL.submit','w')
command = \
f"executable = run_toys_m_DstDs_for_fL.sh \n\
universe = vanilla \n\
output = {an.loc.ROOT}output/logs/results.output.$(ClusterId) \n\
error = {an.loc.ROOT}output/logs/results.error.$(ClusterId) \n\
log = {an.loc.ROOT}output/logs/results.log.$(ClusterId) \n\
notification    = never \n\
request_cpus = 1 \n\
queue {n_fits}   \n\n"
cfile.write(command)
