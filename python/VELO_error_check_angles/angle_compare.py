import glob, os, sys
import numpy as np
import os.path
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib import rc
from scipy import stats
from scipy.stats import norm
from scipy.optimize import curve_fit
import json
from collections import OrderedDict
from root_pandas import read_root
import pandas as pd
from scipy import interpolate
import pickle
import random
import analysis as an
from iminuit import Minuit
from iminuit.cost import LeastSquares

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

    #lumi for each year to weight MC with
    lumi = {"2015": 2*(0.1222 + 0.1624),
            "2016": 2*(0.7778 + 0.8413),
            "2017": 2*(0.8204 + 0.8615),
            "2018": 2*(1.1069 + 1.0239)
            }

    mags = ["Up","Down"]

    tot_lumi = 0.
    for l in lumi:
        tot_lumi += lumi[l]

    df_MC_y = {}
    for y in lumi:
        file_list = []
        for m in mags:
            file_list.append(an.loc.WGEOS+f"/tuples/MC/Bd2DstDsst/{y}_{m}/Total_Bd2DstDsst_Vars_Weights.root")
            #Add ReDecay MC too
            file_list.append(an.loc.WGEOS+f"/tuples/MC/Bd2DstDsst_ReDecay/{y}_{m}/Total_Bd2DstDsst_Vars_Weights.root")
        df_MC_y[y] = read_root(file_list,"DecayTree")
        df_MC_y[y] = df_MC_y[y].query("B_BKGCAT==10")
        df_MC_y[y] = df_MC_y[y].query("costheta_D_reco > -1 and costheta_D_reco < 1 and costheta_X_VV_reco > -1 and costheta_X_VV_reco < 1 and chi_VV_reco > -%s and chi_VV_reco < %s" % (np.pi,np.pi))
        #Add lumi weight as a branch
        df_MC_y[y]["lumi_w"] = lumi[y]/tot_lumi

    df_15_16 = df_MC_y["2015"].append(df_MC_y["2016"])
    df_17_18 = df_MC_y["2017"].append(df_MC_y["2018"])

    bins = 8

    angles = {"costheta_D_reco": ["$\\cos(\\theta_D)$",-1,1],
              "costheta_X_VV_reco": ["$\\cos(\\theta_X)$",-1,1],
              "chi_VV_reco": ["$\\chi$ [rad]",-np.pi,np.pi]
             }

    for a in angles:

        #Histogram of 15-16 MC
        counts_15_16, bin_edges = np.histogram(df_15_16[a], bins, range=(angles[a][1],angles[a][2]),weights=df_15_16["lumi_w"])
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
        errs_15_16 = np.sqrt(counts_15_16)
        #Normalise
        errs_15_16 = errs_15_16.astype(float)/np.sum(counts_15_16)
        counts_15_16 = counts_15_16.astype(float)/np.sum(counts_15_16)

        #Histogram of 17-18 MC
        counts_17_18, bin_edges = np.histogram(df_17_18[a], bins, range=(angles[a][1],angles[a][2]),weights=df_17_18["lumi_w"])
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
        errs_17_18 = np.sqrt(counts_17_18)
        #Normalise
        errs_17_18 = errs_17_18.astype(float)/np.sum(counts_17_18)
        counts_17_18 = counts_17_18.astype(float)/np.sum(counts_17_18)

        fig,ax = plt.subplots(figsize=(8,8))
        plt.errorbar(bin_centres,counts_15_16,yerr=errs_15_16,ls=None,color='red',fmt='o',markersize=5,label="2015 + 2016 full sim MC",alpha=0.6)
        plt.errorbar(bin_centres,counts_17_18,yerr=errs_17_18,ls=None,color='blue',fmt='o',markersize=5,label="2017 + 2018 full sim MC",alpha=0.6)

        plt.legend(fontsize=20,loc='upper right')
        plt.xlabel(angles[a][0],horizontalalignment='right',x=1.0,labelpad=5,fontsize=30)
        ax.tick_params(axis='both', which='major', labelsize=25)
        plt.xlim(-np.pi,np.pi)
        ymin, ymax = plt.ylim()
        plt.ylim(0.0,1.5*ymax)
        plt.tight_layout()
        plt.show()
        fig.savefig(an.loc.ROOT+"output/plots/%s_15_16_vs_17_18_MC.pdf" % a)

        #Ratio
        ratio = counts_15_16 / counts_17_18
        errs_ratio = ratio * np.sqrt( (errs_15_16/counts_15_16)**2 + (errs_17_18/counts_17_18)**2)

        #calculate chi2
        chi2 = 0.
        for i in range(0,len(ratio)):
            chi2 += (ratio[i] - 1)**2 / errs_ratio[i]**2
        chi2 = chi2/len(ratio)

        fig, ax = plt.subplots(figsize=(8,8))
        plt.errorbar(bin_centres,ratio,yerr=errs_ratio,ls=None,color='k',fmt='o',markersize=5,alpha=0.6)
        plt.axhline(y=1,color='gray',linestyle='--',alpha=0.5)
        plt.xlabel(angles[a][0],horizontalalignment='right',x=1.0,labelpad=5,fontsize=30)
        plt.ylabel("15+16 MC / 17+18 MC",horizontalalignment='right',y=1.0,labelpad=5,fontsize=30)
        ax.tick_params(axis='both', which='major', labelsize=25)
        plt.text(-2.5,1.75,"$\\chi^2$/dof = %.2f" % chi2,fontsize=20)
        plt.xlim(-np.pi,np.pi)
        plt.ylim(0,2)
        plt.tight_layout()
        plt.show()
        fig.savefig(an.loc.ROOT+"output/plots/%s_15_16_vs_17_18_MC_ratio.pdf" % a)


if __name__ == '__main__':
    main()
