#Create data array for plotting from the RooFit RooHist
def listsfromhist(hist, overunder = False, normpeak = False, **kwargs):
  from ROOT import TH1
  x, y, x_err, y_err = [ ], [ ], [ ], [ ]
  if isinstance(hist, TH1):
    bin_min = 0 if overunder else 1
    bin_max = hist.GetNbinsX()
    if overunder: bin_max += 1
    for n in range(bin_min, bin_max+1):
      x.append(hist.GetXaxis().GetBinCenter(n))
      y.append(hist.GetBinContent(n))
      x_err.append(hist.GetXaxis().GetBinWidth(n)*0.5)
      y_err.append(hist.GetBinError(n))
  else:
    npts = hist.GetN()
    data = { }
    xscale = kwargs.get('xscale', 1.0)
    for v in ['X','Y','EXlow','EXhigh','EYlow','EYhigh']: #, 'EX', 'EY']:
      try:
        arr = getattr(hist, 'Get'+v)()
        scale = xscale if 'X' in v else 1.0
        tmp = [ arr[n]*scale for n in range(npts) ]
        data[v] = tmp
      except IndexError:
        pass
    x, y = data['X'], data['Y']
    if 'EXlow' in data and 'EXhigh' in data:
      x_err = [ data['EXlow'], data['EXhigh'] ]
    elif 'EX' in data:
      x_err = data['EX']

    if 'EYlow' in data and 'EYhigh' in data:
      y_err = [ data['EYlow'], data['EYhigh'] ]
    elif 'EY' in data:
      y_err = data['EY']

  if normpeak:
    ymax = max(y)
    y = [ yy/ymax for yy in y ]
    try:
      y_err = [
          [ down/ymax for down in y_err[0] ],
          [ up/ymax for up in y_err[1] ]
          ]
    except:
      y_err = [ err/ymax for err in y_err ]

  #return x, y, x_err, y_err
  return x, y, y_err
