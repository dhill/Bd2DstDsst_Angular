import glob, os, sys
import numpy as np
import os.path
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib import rc
from scipy import stats
from scipy.stats import norm
from scipy.optimize import curve_fit
import json
from collections import OrderedDict
from root_pandas import read_root
import pandas as pd
from scipy import interpolate
import pickle
import random
from calc_model import calc_model
import analysis as an
from iminuit import Minuit
from iminuit.cost import LeastSquares

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

def main():

    #RapidSim 4pi sample with no cuts or acceptance, generated with same fL as in data according to m(D* Ds) fit
    path_MC = an.loc.WGEOS+"/RapidSim_tuples/Bd2DstDsst_With_Part_Reco_fL/Total_All_Vars_Weights.root"
    df_MC = read_root(path_MC,"DecayTree")

    #Full sim MC, to represent the data
    path_data = an.loc.WGEOS+"/tuples/MC/Bd2DstDsst"
    file_list = []
    for y in ["2015","2016","2017","2018"]:
        for m in ["Up","Down"]:
            #file_list.append("%s/%s_%s/Total_Bd2DstDsst_Vars.root" % (path_data,y,m))
            file_list.append("%s_ReDecay/%s_%s/Total_Bd2DstDsst_Vars.root" % (path_data,y,m))
    df_data = read_root(file_list,"DecayTree")
    df_data = df_data.query("B_BKGCAT==10")


    #Model re-weighting for true RapidSim to match the MC generated value

    #Generated model in full sim MC
    H0_amp_gen = 0.7204
    H0_phi_gen = 0.0
    Hp_amp_gen = 0.4904
    Hp_phi_gen = 0.0
    Hm_amp_gen = 0.4904
    Hm_phi_gen = 0.0

    df_MC["gen_w"] = calc_model(H0_amp_gen,H0_phi_gen,Hp_amp_gen,Hp_phi_gen,Hm_amp_gen,Hm_phi_gen,df_MC["costheta_D_true"],df_MC["costheta_X_VV_true"],df_MC["chi_VV_true"])

    #Model from m(D* Ds) fit, same one used in RapidSim sample
    f_L = 0.579
    H0_amp_fit = np.sqrt(f_L)
    H0_phi_fit = 0.0
    Hp_amp_fit = np.sqrt((1 - f_L)/2)
    Hp_phi_fit = 0.0
    Hm_amp_fit = np.sqrt((1 - f_L)/2)
    Hm_phi_fit = 0.0

    df_MC["fit_w"] = calc_model(H0_amp_fit,H0_phi_fit,Hp_amp_fit,Hp_phi_fit,Hm_amp_fit,Hm_phi_fit,df_MC["costheta_D_true"],df_MC["costheta_X_VV_true"],df_MC["chi_VV_true"])

    df_MC["model_w"] = df_MC["gen_w"] / df_MC["fit_w"]

    angles = {"costheta_D": ["$\\cos(\\theta_D)$",-1,1],
              "costheta_X_VV": ["$\\cos(\\theta_X)$",-1,1]
              }

    #Dict containing the fitted acceptance pars
    acc_pars = {}

    bins = 15

    for a in angles:

        #Histogram of full sim MC, representing data
        counts_reco, bin_edges = np.histogram(df_data[a+"_reco"], bins, range=(angles[a][1],angles[a][2]))
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
        errs_reco = np.sqrt(counts_reco)
        #Normalise
        errs_reco = errs_reco.astype(float)/np.sum(counts_reco)
        counts_reco = counts_reco.astype(float)/np.sum(counts_reco)

        #Histogram of RapidSim MC in reco angles with model reweighting to fL = 0.52
        counts_true, bin_edges = np.histogram(df_MC[a+"_true"], bins, range=(angles[a][1],angles[a][2]), weights=df_MC["model_w"])
        errs_true = np.sqrt(counts_true)
        #Normalise
        errs_true = errs_true.astype(float)/np.sum(counts_true)
        counts_true = counts_true.astype(float)/np.sum(counts_true)

        fig,ax = plt.subplots(figsize=(8,8))
        plt.errorbar(bin_centres,counts_true,yerr=errs_true,ls=None,color='purple',fmt='o',markersize=5,label="Truth-level RapidSim",alpha=0.6)
        plt.errorbar(bin_centres,counts_reco,yerr=errs_reco,ls=None,color='hotpink',fmt='o',markersize=5,label="Full sim MC",alpha=0.6)

        plt.legend(fontsize=20,loc='upper right')
        plt.xlabel(angles[a][0],horizontalalignment='right',x=1.0,labelpad=5,fontsize=30)
        ax.tick_params(axis='both', which='major', labelsize=25)
        plt.xlim(angles[a][1],angles[a][2])
        ymin, ymax = plt.ylim()
        plt.ylim(0.0,1.5*ymax)
        plt.tight_layout()
        #plt.show()
        fig.savefig(an.loc.ROOT+"output/plots/FullSim_Reco_MC_RapidSim_True_MC_compare_%s.pdf" % a)

        #Make ratio and use it to correct the costheta_X and costheta_D distributions in MC
        ratio = counts_reco / counts_true
        errs_ratio = ratio * np.sqrt( (errs_reco/counts_reco)**2 + (errs_true/counts_true)**2)

        def pdf(x, a0, a1, a2, a3, a4, a5, a6):
            return a0 + a1*x + a2*x**2 + a3*x**3 + a4*x**4 + a5*x**5 + a6*x**6
        x_vals = np.linspace(angles[a][1],angles[a][2],1000)
        least_squares = LeastSquares(bin_centres, ratio, errs_ratio, pdf)

        m = Minuit(least_squares, a0=1., a1=0., a2=0., a3=0., a4=0., a5=0., a6=0.)

        m.migrad() # finds minimum of least_squares function
        m.hesse()  # computes errors
        cov_m = m.np_matrix()
        print(cov_m)

        corr_m = m.latex_matrix()
        print(corr_m)

        corr_file = an.loc.ROOT+"output/tex/MC_acc_pars_corr_m_%s.tex" % a
        with open(corr_file, 'w') as f:
            print(corr_m, file=f)

        #Save covariance matrix for use in varying the chi corr parameters in a multivariate gaussian
        np.save(an.loc.ROOT+"/output/json/MC_acc_pars_cov_%s.npy" % a,cov_m)

        for p in m.parameters:
            print("{} = {:.5f} +/- {:.5f}".format(p, m.values[p], m.errors[p]))
            acc_pars["%s_%s" % (a,p)] = [m.values[p], m.errors[p]]

        fig, ax = plt.subplots(figsize=(8,8))
        plt.errorbar(bin_centres,ratio,yerr=errs_ratio,ls=None,color='k',fmt='o',markersize=5,alpha=0.6)
        plt.plot(x_vals, pdf(x_vals, *m.values.values()), color='crimson',label='Polynomial fit')
        plt.xlabel(angles[a][0],horizontalalignment='right',x=1.0,labelpad=5,fontsize=30)
        plt.ylabel("Data / Truth-level RapidSim",horizontalalignment='right',y=1.0,labelpad=5,fontsize=30)
        plt.axhline(y=1.0,color='gray',linestyle='--',alpha=0.4)
        ax.tick_params(axis='both', which='major', labelsize=25)
        plt.legend(fontsize=20)
        plt.xlim(angles[a][1],angles[a][2])
        ylow, yhigh = plt.ylim()
        plt.tight_layout()
        #plt.show()

        fig.savefig(an.loc.ROOT+"output/plots/FullSim_Reco_MC_RapidSim_True_MC_ratio_%s.pdf" % a)

    #Save acceptance parameters
    with open(an.loc.ROOT+'output/json/MC_acc_pars_costheta_X_D.json', 'w') as fp:
        json.dump(acc_pars, fp, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()
