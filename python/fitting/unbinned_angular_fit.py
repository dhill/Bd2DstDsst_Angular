import tensorflow as tf
import zfit
import numpy as np
from root_pandas import read_root, to_root
import json
import matplotlib.pyplot as plt
import analysis as an
import argparse
import random
from zfit.minimize import DefaultToyStrategy
import progressbar
from init_vals_unbinned_angular_fit import init_vals

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Roman']})
rc('text', usetex=True)

#3D decay rate PDF with acceptance corrections
class CustomPDF(zfit.pdf.ZPDF):
    _PARAMS = ['H0_amp',
    		   'Hm_amp',
    		   'Hp_amp',
    		   'H0_phi',
    		   'Hm_phi',
    		   'Hp_phi',
    		   'costheta_D_p0',
			   'costheta_D_p1',
			   'costheta_D_p2',
			   'costheta_D_p3',
			   'costheta_D_p4',
			   'costheta_D_p5',
			   'costheta_D_p6',
			   'costheta_X_p0',
			   'costheta_X_p1',
			   'costheta_X_p2',
			   'costheta_X_p3',
			   'costheta_X_p4',
			   'costheta_X_p5',
			   'costheta_X_p6',
			   'chi_p0',
			   'chi_p1',
			   'chi_p2',
			   #'chi_p3',
			   #'chi_p4'
			   ]
    _N_OBS = 3

    def _unnormalized_pdf(self, x):
        costheta_D, costheta_X, chi = x.unstack_x()

        H0_amp = self.params['H0_amp']
        H0_phi = self.params['H0_phi']
        Hm_amp = self.params['Hm_amp']
        Hp_amp = self.params['Hp_amp']
        Hm_phi = self.params['Hm_phi']
        Hp_phi = self.params['Hp_phi']
        costheta_D_p0 = self.params['costheta_D_p0']
        costheta_D_p1 = self.params['costheta_D_p1']
        costheta_D_p2 = self.params['costheta_D_p2']
        costheta_D_p3 = self.params['costheta_D_p3']
        costheta_D_p4 = self.params['costheta_D_p4']
        costheta_D_p5 = self.params['costheta_D_p5']
        costheta_D_p6 = self.params['costheta_D_p6']
        costheta_X_p0 = self.params['costheta_X_p0']
        costheta_X_p1 = self.params['costheta_X_p1']
        costheta_X_p2 = self.params['costheta_X_p2']
        costheta_X_p3 = self.params['costheta_X_p3']
        costheta_X_p4 = self.params['costheta_X_p4']
        costheta_X_p5 = self.params['costheta_X_p5']
        costheta_X_p6 = self.params['costheta_X_p6']
        chi_p0 = self.params['chi_p0']
        chi_p1 = self.params['chi_p1']
        chi_p2 = self.params['chi_p2']
        #chi_p3 = self.params['chi_p3']
        #chi_p4 = self.params['chi_p4']

        sintheta_D = tf.math.sqrt( 1.0 - costheta_D * costheta_D )
        sintheta_X = tf.math.sqrt( 1.0 - costheta_X * costheta_X )
        sintheta_D2 = (2.0 * sintheta_D * costheta_D)
        sintheta_X2 = (2.0 * sintheta_X * costheta_X)
        coschi = tf.math.cos(chi)
        sinchi = tf.math.sin(chi)
        cos2chi = 2*coschi*coschi-1
        sin2chi = 2*sinchi*coschi

        h_0 = tf.complex(H0_amp*tf.cos(H0_phi),H0_amp*tf.sin(H0_phi))
        h_p = tf.complex(Hp_amp*tf.cos(Hp_phi),Hp_amp*tf.sin(Hp_phi))
        h_m = tf.complex(Hm_amp*tf.cos(Hm_phi),Hm_amp*tf.sin(Hm_phi))

        h_0st = tf.math.conj(h_0)
        h_pst = tf.math.conj(h_p)
        h_mst = tf.math.conj(h_m)

        HpHmst = h_p*h_mst
        HpH0st = h_p*h_0st
        HmH0st = h_m*h_0st

        #True decay rate
        pdf = H0_amp**2 * costheta_D**2 * sintheta_X**2
        pdf += 0.25 * (Hp_amp**2 + Hm_amp**2) * sintheta_D**2 * (1.0 + costheta_X**2)
        pdf += -0.5 * tf.math.real(HpHmst) * sintheta_D**2 * sintheta_X**2 * cos2chi
        pdf += 0.5 * tf.math.imag(HpHmst) * sintheta_D**2 * sintheta_X**2 * sin2chi
        pdf += -0.25 * tf.math.real(HpH0st + HmH0st) * sintheta_D2 * sintheta_X2 * coschi
        pdf += 0.25 * tf.math.imag(HpH0st - HmH0st) * sintheta_D2 * sintheta_X2 * sinchi
        pdf = pdf * 9./8.

        #Normalise to 1
        #pdf = pdf * (1./tf.reduce_sum(pdf))

        #Acceptance corrections
        acc_pdf_costheta_D = costheta_D_p0 + costheta_D_p1 * costheta_D + costheta_D_p2 * costheta_D**2 + costheta_D_p3 * costheta_D**3 + costheta_D_p4 * costheta_D**4 + costheta_D_p5 * costheta_D**5 + costheta_D_p6 * costheta_D**6
        acc_pdf_costheta_X = costheta_X_p0 + costheta_X_p1 * costheta_X + costheta_X_p2 * costheta_X**2 + costheta_X_p3 * costheta_X**3 + costheta_X_p4 * costheta_X**4 + costheta_X_p5 * costheta_X**5 + costheta_X_p6 * costheta_X**6
        acc_pdf_chi = chi_p0 + chi_p1 * chi + chi_p2 * chi**2 #chi_p0 + chi_p1 * chi + chi_p2 * chi**2 + chi_p3 * chi**3 + chi_p4 * chi**4
        acc_pdf_chi = tf.abs(acc_pdf_chi)

        acc_pdf = tf.multiply(acc_pdf_costheta_D,acc_pdf_costheta_X)
        acc_pdf = tf.multiply(acc_pdf,acc_pdf_chi)

        pdf = tf.multiply(pdf,acc_pdf)

        return pdf

def run_fit(syst, toy, boost):

    #Angular phase space
    xobs = zfit.Space('costheta_D_reco', (-1, 1))
    yobs = zfit.Space('costheta_X_VV_reco', (-0.9, 1))
    zobs = zfit.Space('chi_VV_reco', (-np.pi, np.pi))
    obs = xobs * yobs * zobs

    obs_dict = {"costheta_D_reco": [0,"$\\cos(\\theta_D)$",-1,1,xobs],
                "costheta_X_VV_reco": [1,"$\\cos(\\theta_X)$",-0.9,1,yobs],
                "chi_VV_reco": [2,"$\\chi$ [rad]",-np.pi,np.pi,zobs]
                }

    #Helicity amplitude parameters
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL.json','r') as f:
        fL_dict = json.load(f)
    with open(an.loc.ROOT+'/output/json/m_DstDs_for_fL_syst.json','r') as f:
        fL_syst_dict = json.load(f)
    fL_val = fL_dict["fL"][0]
    fL_err = np.sqrt(fL_dict["fL"][1]**2 + fL_syst_dict["fL_tot"]**2)
    #Systematically vary fL according to measured error
    if(syst=="fL"):
        randint = random.randint(1,999)
        np.random.seed(randint)
        fL_val += np.random.normal(0.,fL_err,1)
    H0_amp = zfit.Parameter("H0_amp", np.sqrt(fL_val), floating=False)
    Hm_amp = zfit.Parameter("Hm_amp", init_vals["Hm_amp"], 0., 1.)
    def Hp_amp_func(H0_amp, Hm_amp):
	       return tf.sqrt(1. - H0_amp**2 - Hm_amp**2)
    Hp_amp = zfit.ComposedParameter("Hp_amp", Hp_amp_func, dependents=[H0_amp, Hm_amp])
    H0_phi =  zfit.Parameter("H0_phi", 0., floating=False)
    Hp_phi =  zfit.Parameter("Hp_phi", init_vals["Hp_phi"], -np.pi, np.pi)#floating=False)
    Hm_phi =  zfit.Parameter("Hm_phi", init_vals["Hm_phi"],-np.pi, np.pi)

    #Acceptance corrections

    #Default costheta_D and costheta_X corrections
    if(syst!="fL"):
        with open(an.loc.ROOT+'/output/json/acc_pars_costheta_X_D.json','r') as f:
            acc_dict = json.load(f)
        suff = ""
    #Acceptance corrections with systematically varied fL
    else:
        with open(an.loc.ROOT+'/output/json/acc_pars_costheta_X_D_syst.json','r') as f:
            acc_dict = json.load(f)
        suff = "_%s" % randint #random integer matching the random seed used to vary fL above

    costheta_D_p0 = zfit.Parameter("costheta_D_p0" , acc_dict[f"costheta_D_a0{suff}"][0], floating=False)
    costheta_D_p1 = zfit.Parameter("costheta_D_p1" , acc_dict[f"costheta_D_a1{suff}"][0], floating=False)
    costheta_D_p2 = zfit.Parameter("costheta_D_p2" , acc_dict[f"costheta_D_a2{suff}"][0], floating=False)
    costheta_D_p3 = zfit.Parameter("costheta_D_p3" , acc_dict[f"costheta_D_a3{suff}"][0], floating=False)
    costheta_D_p4 = zfit.Parameter("costheta_D_p4" , acc_dict[f"costheta_D_a4{suff}"][0], floating=False)
    costheta_D_p5 = zfit.Parameter("costheta_D_p5" , acc_dict[f"costheta_D_a5{suff}"][0], floating=False)
    costheta_D_p6 = zfit.Parameter("costheta_D_p6" , acc_dict[f"costheta_D_a6{suff}"][0], floating=False)

    costheta_X_p0 = zfit.Parameter("costheta_X_p0" , acc_dict[f"costheta_X_VV_a0{suff}"][0], floating=False)
    costheta_X_p1 = zfit.Parameter("costheta_X_p1" , acc_dict[f"costheta_X_VV_a1{suff}"][0], floating=False)
    costheta_X_p2 = zfit.Parameter("costheta_X_p2" , acc_dict[f"costheta_X_VV_a2{suff}"][0], floating=False)
    costheta_X_p3 = zfit.Parameter("costheta_X_p3" , acc_dict[f"costheta_X_VV_a3{suff}"][0], floating=False)
    costheta_X_p4 = zfit.Parameter("costheta_X_p4" , acc_dict[f"costheta_X_VV_a4{suff}"][0], floating=False)
    costheta_X_p5 = zfit.Parameter("costheta_X_p5" , acc_dict[f"costheta_X_VV_a5{suff}"][0], floating=False)
    costheta_X_p6 = zfit.Parameter("costheta_X_p6" , acc_dict[f"costheta_X_VV_a6{suff}"][0], floating=False)

    with open(an.loc.ROOT+'/output/json/acc_pars_chi.json','r') as f:
        acc_dict_chi = json.load(f)

    chi_p0 = zfit.Parameter("chi_p0" , acc_dict_chi["chi_VV_a0"][0], floating=False)
    chi_p1 = zfit.Parameter("chi_p1" , acc_dict_chi["chi_VV_a1"][0], floating=False)
    chi_p2 = zfit.Parameter("chi_p2" , acc_dict_chi["chi_VV_a2"][0], floating=False)
    #chi_p3 = zfit.Parameter("chi_p3" , acc_dict_chi["chi_VV_a3"][0], floating=False)
    #chi_p4 = zfit.Parameter("chi_p4" , acc_dict_chi["chi_VV_a4"][0], floating=False)

    #If varying the acceptance params according to stat errors, vary according to multivariate gaussian using their covariance matrix
    if(syst=="acc_funcs"):

        ###########
        ### chi ###
        ###########

        #Load covariance matrix
        cov_m = np.load(an.loc.ROOT+"output/json/acc_pars_cov_chi_VV.npy")

        #Correction parameter values
        vals = np.array([acc_dict_chi["chi_VV_a0"][0],
                         acc_dict_chi["chi_VV_a1"][0],
                         acc_dict_chi["chi_VV_a2"][0]])
                         #acc_dict_chi["chi_VV_a3"][0],
                         #acc_dict_chi["chi_VV_a4"][0]])

        #Randomly varied values
        rand_int = random.randint(1,1e6)
        np.random.seed(rand_int)
        rand_pars = np.random.multivariate_normal(vals, cov_m, size=1)

        #Update values
        chi_p0.assign(rand_pars[0][0])
        chi_p1.assign(rand_pars[0][1])
        chi_p2.assign(rand_pars[0][2])
        #chi_p3.assign(rand_pars[0][3])
        #chi_p4.assign(rand_pars[0][4])

        ##################
        ### costheta_D ###
        ##################

        #Load covariance matrix
        cov_m = np.load(an.loc.ROOT+"output/json/acc_pars_cov_costheta_D.npy")

        #Correction parameter values
        vals = np.array([acc_dict["costheta_D_a0"][0],
                         acc_dict["costheta_D_a1"][0],
                         acc_dict["costheta_D_a2"][0],
                         acc_dict["costheta_D_a3"][0],
                         acc_dict["costheta_D_a4"][0],
                         acc_dict["costheta_D_a5"][0],
                         acc_dict["costheta_D_a6"][0]])

        #Randomly varied values
        rand_int = random.randint(1,1e6)
        np.random.seed(rand_int)
        rand_pars = np.random.multivariate_normal(vals, cov_m, size=1)

        #Update values
        costheta_D_p0.assign(rand_pars[0][0])
        costheta_D_p1.assign(rand_pars[0][1])
        costheta_D_p2.assign(rand_pars[0][2])
        costheta_D_p3.assign(rand_pars[0][3])
        costheta_D_p4.assign(rand_pars[0][4])
        costheta_D_p5.assign(rand_pars[0][5])
        costheta_D_p6.assign(rand_pars[0][6])

        ##################
        ### costheta_X ###
        ##################

        #Load covariance matrix
        cov_m = np.load(an.loc.ROOT+"output/json/acc_pars_cov_costheta_X_VV.npy")

        #Correction parameter values
        vals = np.array([acc_dict["costheta_X_VV_a0"][0],
                         acc_dict["costheta_X_VV_a1"][0],
                         acc_dict["costheta_X_VV_a2"][0],
                         acc_dict["costheta_X_VV_a3"][0],
                         acc_dict["costheta_X_VV_a4"][0],
                         acc_dict["costheta_X_VV_a5"][0],
                         acc_dict["costheta_X_VV_a6"][0]])

        #Randomly varied values
        rand_int = random.randint(1,1e6)
        np.random.seed(rand_int)
        rand_pars = np.random.multivariate_normal(vals, cov_m, size=1)

        #Update values
        costheta_X_p0.assign(rand_pars[0][0])
        costheta_X_p1.assign(rand_pars[0][1])
        costheta_X_p2.assign(rand_pars[0][2])
        costheta_X_p3.assign(rand_pars[0][3])
        costheta_X_p4.assign(rand_pars[0][4])
        costheta_X_p5.assign(rand_pars[0][5])
        costheta_X_p6.assign(rand_pars[0][6])



    custom_pdf = CustomPDF(obs=obs,
                           H0_amp=H0_amp,
                           Hm_amp=Hm_amp,
                           Hp_amp=Hp_amp,
                           H0_phi=H0_phi,
                           Hm_phi=Hm_phi,
                           Hp_phi=Hp_phi,
                           costheta_D_p0 = costheta_D_p0,
                           costheta_D_p1 = costheta_D_p1,
                           costheta_D_p2 = costheta_D_p2,
                           costheta_D_p3 = costheta_D_p3,
                           costheta_D_p4 = costheta_D_p4,
                           costheta_D_p5 = costheta_D_p5,
                           costheta_D_p6 = costheta_D_p6,
                           costheta_X_p0 = costheta_X_p0,
                           costheta_X_p1 = costheta_X_p1,
                           costheta_X_p2 = costheta_X_p2,
                           costheta_X_p3 = costheta_X_p3,
                           costheta_X_p4 = costheta_X_p4,
                           costheta_X_p5 = costheta_X_p5,
                           costheta_X_p6 = costheta_X_p6,
                           chi_p0 = chi_p0,
                           chi_p1 = chi_p1,
                           chi_p2 = chi_p2)
                           #chi_p3 = chi_p3,
                           #chi_p4 = chi_p4)

    integral = custom_pdf.integrate(limits=obs)  # = 1 since normalized

    #Load data
    path = an.loc.ROOT+"output/rootfiles/Bd2DstDsst_sWeights.root"
    df = read_root(path, "RooTreeDataStore_data_data", columns = ["costheta_D_reco","costheta_X_VV_reco","chi_VV_reco","n_sig_sw","B_D0constDsconstPVconst_B_M"])
    #Keep events within 50 MeV of B peak
    #df = df.query("abs(B_D0constDsconstPVconst_B_M-5284)<50")
    df_data = df[["costheta_D_reco","costheta_X_VV_reco","chi_VV_reco"]]
    df_w = df["n_sig_sw"]

    #Systematically vary sWeights according to percentage error on the sWeight sum i.e. number of signal events
    if(syst=="data_sw"):
        randint = random.randint(1,1e6)
        np.random.seed(randint)
        sum_sw = np.sum(df_w)
        sw_per_err = 1./np.sqrt(sum_sw)
        df_w = df_w * np.random.normal(1.0,sw_per_err,size=len(df_w))
        sum_sw_syst = np.sum(df_w)
        #Rescale so sWeights still sum to correct value
        df_w = df_w * (float(sum_sw) / float(sum_sw_syst))

    if(toy=="N"):
        data = zfit.Data.from_pandas(df_data, obs=obs, weights=df_w)
    #Generate toy data with expected number of signal events
    else:
        fac = 1.
        if(boost=="Y"):
            fac = 10.
        data = custom_pdf.create_sampler(n=fac*np.sum(df_w), fixed_params=True)

    # Stage 1: create an unbinned likelihood with the given PDF and dataset
    nll = zfit.loss.UnbinnedNLL(model=custom_pdf, data=data)

    # Stage 2: instantiate a minimiser (in this case a basic minuit
    if(toy=="N"):
        minimizer = zfit.minimize.Minuit(tolerance=1e-5)

    else:
        minimizer = zfit.minimize.Minuit(strategy=DefaultToyStrategy(), tolerance=1e-5)

        # Generate toys
        data.resample()

    #Stage 3: minimise the given negative likelihood
    result = minimizer.minimize(nll)

    param_errors, _ = result.errors(method="minuit_minos")

    print("Function minimum:", result.fmin)
    print("Converged:", result.converged)
    print("Full minimizer information:", result.info)

    params = result.params
    print(params)

    #Write fit results to JSON file
    results_dict = {}
    for p in params:
        results_dict["%s" % p.name] = [params[p]['value'], param_errors[p]['lower'], param_errors[p]['upper']]

    if(toy=="N" and syst=="none"):
        with open(an.loc.ROOT+'output/json/unbinned_angular_fit_results.json', 'w') as f:
            json.dump(results_dict, f, sort_keys=True, indent=4)
    elif(syst!="none"):
        randint = random.randint(1,1e10)
        with open(an.loc.WGEOS+'/Systematics/Unbinned_Fit/unbinned_angular_fit_results_%s_%s.json' % (syst,randint), 'w') as f:
            json.dump(results_dict, f, sort_keys=True, indent=4)
    elif(toy=="Y"):
        randint = random.randint(1,1e10)
        with open(an.loc.WGEOS+'/Toys/Unbinned_Fit/unbinned_angular_fit_results_%s_Boost_%s.json' % (randint,boost), 'w') as f:
            json.dump(results_dict, f, sort_keys=True, indent=4)

    #Plot results if running real data fit
    if(syst=="none" and toy=="N"):
        #Plot projections
        bins = 30
        data_np = zfit.run(data)
        print(data_np)

        for v in obs_dict:

            fig, ax = plt.subplots(figsize=(7,7))
            lower = obs_dict[v][2]
            upper = obs_dict[v][3]
            counts, bin_edges = np.histogram(df_data[v], bins=bins, range=(lower, upper), weights=df["n_sig_sw"])
            bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
            err = np.sqrt(counts)
            #Normalise
            err = err / np.sum(counts)
            counts = counts / np.sum(counts)

            #Plot the data
            plt.errorbar(bin_centres, counts, yerr=err, fmt='o', color='xkcd:black')

            #Plot the PDF
            n_p = 1000
            x_plot = np.linspace(lower, upper, num=n_p)
            x_plot_data = zfit.Data.from_numpy(array=x_plot, obs=obs_dict[v][4])

            #Other dims
            other_obs = []
            for k in obs_dict:
                if(k!=v):
                    other_obs.append(obs_dict[k][4])

            #Observables to integrate over
            obs_int = other_obs[0] * other_obs[1]

            #Integrating over the other dimensions
            pdf_x = custom_pdf.partial_integrate(x=x_plot_data, limits=obs_int)
            y_plot = zfit.run(pdf_x)
            #Normalise
            y_plot = y_plot / np.sum(y_plot) * (n_p/bins)

            plt.plot(x_plot, y_plot, color='crimson')

            #Labels and axis ranges
            plt.xlabel(obs_dict[v][1],fontsize=25)
            bin_width = float(upper - lower)/bins
            y_min, y_max = ax.get_ylim()
            plt.ylim(0.0,y_max*1.4)
            plt.xlim(lower, upper)
            ax.tick_params(axis='both', which='major', labelsize=25)
            plt.tight_layout()
            fig.savefig(an.loc.ROOT+f"output/plots/{v}_unbinned_fit.pdf")

        #Plot 2D data, results, and residuals
        bins_2d = 15
        for v1 in obs_dict:
            for v2 in obs_dict:
                if(v1!=v2):

                    #2D data
                    fig, ax = plt.subplots(figsize=(8,7))
                    h_data, x_data, y_data, i_data = ax.hist2d(df_data[v1],df_data[v2],bins=bins_2d,range=[[obs_dict[v1][2],obs_dict[v1][3]], [obs_dict[v2][2],obs_dict[v2][3]]],weights=df["n_sig_sw"],cmap='magma')
                    plt.xlabel(obs_dict[v1][1],fontsize=25)
                    plt.ylabel(obs_dict[v2][1],fontsize=25)
                    plt.title("Data",fontsize=20)
                    cbar = plt.colorbar(i_data, ax=ax)
                    cbar.ax.tick_params(labelsize=20)
                    ax.tick_params(axis='both', which='major', labelsize=25)
                    plt.tight_layout()
                    fig.savefig(an.loc.ROOT+f'output/plots/unbinned_fit_data_{v1}_vs_{v2}.pdf')

                    #2D PDF
                    n_samp = 1e6
                    sample = custom_pdf.sample(n_samp) #np.sum(df["n_sig_sw"]))
                    sample_np = zfit.run(sample)

                    if(v1=="costheta_D_reco"):
                        i = 0
                    elif(v1=="costheta_X_VV_reco"):
                        i = 1
                    else:
                        i = 2
                    if(v2=="costheta_D_reco"):
                        j = 0
                    elif(v2=="costheta_X_VV_reco"):
                        j = 1
                    else:
                        j = 2
                    fig, ax = plt.subplots(figsize=(8,7))
                    h_pdf, x_pdf, y_pdf, i_pdf = ax.hist2d(sample_np[:,i],sample_np[:,j],bins=bins_2d,range=[[obs_dict[v1][2],obs_dict[v1][3]], [obs_dict[v2][2],obs_dict[v2][3]]],cmap='magma')
                    #Scale values to match data yield
                    h_pdf = h_pdf * float(np.sum(df["n_sig_sw"]))/n_samp
                    plt.xlabel(obs_dict[v1][1],fontsize=25)
                    plt.ylabel(obs_dict[v2][1],fontsize=25)
                    plt.title("Fit",fontsize=20)
                    cbar = plt.colorbar(i_data, ax=ax)
                    cbar.ax.tick_params(labelsize=20)
                    ax.tick_params(axis='both', which='major', labelsize=25)
                    plt.tight_layout()
                    fig.savefig(an.loc.ROOT+f'output/plots/unbinned_fit_pdf_{v1}_vs_{v2}.pdf')

                    #Residuals
                    resid = np.subtract(list(h_data),list(h_pdf))
                    err = np.sqrt(list(h_data))
                    resid = resid / err
                    #Some bins with zero content end up with NaN when dividing by error above - set them to zero
                    resid[np.isnan(resid)] = 0.
                    resid[np.isinf(np.abs(resid))] = 0.

                    fig, ax = plt.subplots(figsize=(8,7))

                    im = ax.imshow(resid,origin='lower',extent=[obs_dict[v1][2], obs_dict[v1][3], obs_dict[v2][2], obs_dict[v2][3]],aspect='auto',clim=(-5.,5.),cmap='RdBu')
                    v = np.linspace(-5., 5., 11, endpoint=True)
                    cbar = plt.colorbar(im, ax=ax, ticks=v)
                    cbar.ax.tick_params(labelsize=20)
                    plt.xlabel(obs_dict[v1][1],fontsize=25)
                    plt.ylabel(obs_dict[v2][1],fontsize=25)
                    plt.title("Residuals",fontsize=20)
                    ax.tick_params(axis='both', which='major', labelsize=25)
                    plt.tight_layout()
                    fig.savefig(an.loc.ROOT+f'output/plots/unbinned_fit_resid_{v1}_vs_{v2}.pdf')


def main():
    parser = argparse.ArgumentParser(description='Unbinned angular fit to B0 -> D* Ds* sWeighted data')
    parser.add_argument("--Syst", choices=["fL","data_sw","acc_funcs","none"],
                    required=True, help="Systematic category")
    parser.add_argument("--Toy", choices=["Y","N"],
                    required=True, help="Whether to run a toy fit or not")
    parser.add_argument("--Boost", choices=["Y","N"],
                    required=True, help="Whether to run toy with 10x stats boost")
    args = parser.parse_args()

    run_fit(args.Syst, args.Toy, args.Boost)

if __name__ == '__main__':
    main()
