import sys, os
import subprocess
import analysis as an

systs = ["fL",
         "data_sw",
         "acc_funcs"
         ]

#Number of fits per systematic to run in a loop inside job
n_fits = 100

for s in systs:
    cfile = open(an.loc.ROOT+f'condor/submit_scripts/syst_unbinned_angular_fit_{s}.submit','w')
    command = \
    f"executable = run_syst_unbinned_angular_fit.sh \n\
    universe = vanilla \n\
    arguments = {s} \n\
    output = {an.loc.ROOT}output/logs/results.output.$(ClusterId) \n\
    error = {an.loc.ROOT}output/logs/results.error.$(ClusterId) \n\
    log = {an.loc.ROOT}output/logs/results.log.$(ClusterId) \n\
    notification    = never \n\
    request_cpus = 1 \n\
    queue {n_fits}   \n\n"
    cfile.write(command)
