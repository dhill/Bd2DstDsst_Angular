#!/bin/sh

export ANAROOT=/home/dhill/Bd2DstDsst_Angular
source $ANAROOT/setup/setup.sh

#Run 10 fits
for i in {1..10}
do
	python $ANAROOT/python/fitting/unbinned_angular_fit.py --Syst $1 --Toy N --Boost N
done
