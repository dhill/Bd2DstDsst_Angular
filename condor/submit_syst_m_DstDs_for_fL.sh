#!/bin/sh

export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

cd $ANAROOT/condor

for syst in PDFs BFs
do
	condor_submit submit_scripts/syst_m_DstDs_for_fL_${syst}.submit
done
